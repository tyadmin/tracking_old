<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
use Collect\ProxyHttp;

/**
 * Tracker proxy
 */
if ($_SERVER['REQUEST_METHOD'] == 'POST'
    || !empty($_SERVER['QUERY_STRING'])
) {
    include '../collect.php';
    exit;
}

/**
 * collect.js proxy
 *
 * @see core/collect.php
 */
define('TJWXJC_DOCUMENT_ROOT', '..');

if (file_exists(TJWXJC_DOCUMENT_ROOT . '/bootstrap.php')) {
    require_once TJWXJC_DOCUMENT_ROOT . '/bootstrap.php';
}

if (!defined('TJWXJC_INCLUDE_PATH')) {
    define('TJWXJC_INCLUDE_PATH', TJWXJC_DOCUMENT_ROOT);
}

if (!defined('TJWXJC_USER_PATH')) {
    define('TJWXJC_USER_PATH', TJWXJC_DOCUMENT_ROOT);
}

require_once TJWXJC_INCLUDE_PATH . '/libs/upgradephp/upgrade.php';

if (is_dir(TJWXJC_INCLUDE_PATH . '/vendor')) {
    define('TJWXJC_VENDOR_PATH', TJWXJC_INCLUDE_PATH . '/vendor'); // Collect is the main project
} else {
    define('TJWXJC_VENDOR_PATH', TJWXJC_INCLUDE_PATH . '/../..'); // Collect is installed as a Composer dependency
}

// Composer autoloader
require TJWXJC_VENDOR_PATH . '/autoload.php';

$file = '../matomo.js';

$daysExpireFarFuture = 10;

$byteStart = $byteEnd = false;
if (!defined("TJWXJC_KEEP_JS_TRACKER_COMMENT")
    || !TJWXJC_KEEP_JS_TRACKER_COMMENT
) {
    $byteStart = 378; // length of comment header in bytes
}

class Validator {
    public function validate() {}
}
$validator = new Validator();
$environment = new \Collect\Application\Environment(null, array(
    'Collect\Application\Kernel\EnvironmentValidator' => $validator
));
$environment->init();

if (!\Collect\Tracker\IgnoreCookie::isIgnoreCookieFound()) {
    
    $request = new \Collect\Tracker\Request(array());
    
    if ($request->shouldUseThirdPartyCookie()) {
        $visitorId = $request->getVisitorIdForThirdPartyCookie();
        if (!$visitorId) {
            $visitorId = \Collect\Common::hex2bin(\Collect\Tracker\Visit::generateUniqueVisitorId());
        }
        $request->setThirdPartyCookie($visitorId);
    }
}

ProxyHttp::serverStaticFile($file, "application/javascript; charset=UTF-8", $daysExpireFarFuture, $byteStart, $byteEnd);

exit;
