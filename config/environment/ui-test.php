<?php

use Collect\Container\StaticContainer;

return array(

    // UI tests will remove the port from all URLs to the test server. if a test
    // requires the ports in UI tests (eg, Overlay), add the api/controller methods
    // to one of these blacklists
    'tests.ui.url_normalizer_blacklist.api' => array(),
    'tests.ui.url_normalizer_blacklist.controller' => array(),

    'Collect\Config' => \DI\decorate(function (\Collect\Config $config) {
        $config->General['cors_domains'][] = '*';
        $config->General['trusted_hosts'][] = '127.0.0.1';
        $config->General['trusted_hosts'][] = $config->tests['http_host'];
        $config->General['trusted_hosts'][] = $config->tests['http_host'] . ':' . $config->tests['port'];
        return $config;
    }),

    'observers.global' => \DI\add([

        // removes port from all URLs to the test Collect server so UI tests will pass no matter
        // what port is used
        array('Request.dispatch.end', DI\value(function (&$result) {
            $request = $_GET + $_POST;

            $apiblacklist = StaticContainer::get('tests.ui.url_normalizer_blacklist.api');
            if (!empty($request['method'])
                && in_array($request['method'], $apiblacklist)
            ) {
                return;
            }

            $controllerActionblacklist = StaticContainer::get('tests.ui.url_normalizer_blacklist.controller');
            if (!empty($request['module'])) {
                $controllerAction = $request['module'] . '.' . (isset($request['action']) ? $request['action'] : 'index');
                if (in_array($controllerAction, $controllerActionblacklist)) {
                    return;
                }
            }

            $config = \Collect\Config::getInstance();
            $host = $config->tests['http_host'];
            $port = $config->tests['port'];

            if (!empty($port)) {
                // remove the port from URLs if any so UI tests won't fail if the port isn't 80
                $result = str_replace($host . ':' . $port, $host, $result);
            }

            // remove TJWXJC_INCLUDE_PATH from result so tests don't change based on the machine used
            $result = str_replace(realpath(TJWXJC_INCLUDE_PATH), '', $result ?? '');
        })),

        array('Controller.RssWidget.rssCollect.end', DI\value(function (&$result, $parameters) {
            $result = "";
        })),

        \Collect\Tests\Framework\XssTesting::getJavaScriptAddEvent(),
    ]),

);
