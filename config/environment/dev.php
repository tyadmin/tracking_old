<?php

return array(

    'Matomo\Cache\Backend' => DI\autowire('Matomo\Cache\Backend\ArrayCache'),

    'Collect\Translation\Loader\LoaderInterface' => DI\autowire('Collect\Translation\Loader\LoaderCache')
        ->constructorParameter('loader', DI\get('Collect\Translation\Loader\DevelopmentLoader')),
    'Collect\Translation\Loader\DevelopmentLoader' => DI\create()
        ->constructor(DI\get('Collect\Translation\Loader\JsonFileLoader')),

);
