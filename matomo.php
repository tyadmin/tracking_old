<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

if (!defined('TJWXJC_DOCUMENT_ROOT')) {
    define('TJWXJC_DOCUMENT_ROOT', dirname(__FILE__) == '/' ? '' : dirname(__FILE__));
}

include TJWXJC_DOCUMENT_ROOT . '/collect.php';