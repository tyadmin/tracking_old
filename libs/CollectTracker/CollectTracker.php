<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

// CollectTracker.php is now managed by composer. To prevent breaking existing
// code, this file has been left as a redirect to its new location in the
// vendor directory.
if (!class_exists('CollectTracker')) {
    require_once __DIR__ . '/../../vendor/matomo/matomo-php-tracker/CollectTracker.php';
}

if (CollectTracker::VERSION !== 1) {
    throw new Exception("Expected CollectTracker in libs/CollectTracker/CollectTracker.php to be version 1 for keeping backward compatibility.");
}
