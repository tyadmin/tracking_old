<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect;

use Collect\Application\Environment;
use Collect\Tests\Framework\TestingEnvironmentManipulator;
use Collect\Tests\Framework\TestingEnvironmentVariables;

if (!defined('TJWXJC_DOCUMENT_ROOT')) {
    define('TJWXJC_DOCUMENT_ROOT', realpath(dirname(__FILE__) . "/../.."));
}

define('TJWXJC_ENABLE_DISPATCH', false);
define('TJWXJC_ENABLE_ERROR_HANDLER', false);
define('TJWXJC_ENABLE_SESSION_START', false);

require_once TJWXJC_DOCUMENT_ROOT . "/index.php";

if (!Common::isPhpCliMode()) {
    return;
}

$testmode = in_array('--testmode', $_SERVER['argv']);
if ($testmode) {
    define('TJWXJC_TEST_MODE', true);

    Environment::setGlobalEnvironmentManipulator(new TestingEnvironmentManipulator(new TestingEnvironmentVariables()));
}

function getCollectDomain()
{
    foreach($_SERVER['argv'] as $param) {
        $pattern = '--matomo-domain=';
        if(false !== strpos($param, $pattern)) {
            return substr($param, strlen($pattern));
        }
    }
    return null;
}


$collectDomain = getCollectDomain();
if($collectDomain) {
    Url::setHost($collectDomain);
}

$environment = new Environment('cli');
$environment->init();

$token = Collect::requestTemporarySystemAuthToken('LogImporter', 48);

$filename = $environment->getContainer()->get('path.tmp') . '/cache/token.php';

$content  = "<?php exit; //\t" . $token;
file_put_contents($filename, $content);
echo $filename;