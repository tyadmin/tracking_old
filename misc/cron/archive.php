<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

if (!defined('TJWXJC_DOCUMENT_ROOT')) {
    define('TJWXJC_DOCUMENT_ROOT', realpath(dirname(__FILE__) . "/../.."));
}

if (!defined('TJWXJC_INCLUDE_PATH')) {
    define('TJWXJC_INCLUDE_PATH', TJWXJC_DOCUMENT_ROOT);
}

if (!defined('TJWXJC_USER_PATH')) {
    define('TJWXJC_USER_PATH', TJWXJC_INCLUDE_PATH);
}

define('TJWXJC_ENABLE_ERROR_HANDLER', false);
define('TJWXJC_ENABLE_SESSION_START', false);

require_once TJWXJC_INCLUDE_PATH . '/core/Common.php';

if (Collect\Common::isPhpCliMode()) {
    require_once TJWXJC_INCLUDE_PATH . "/core/bootstrap.php";

    $console = new Collect\Console();

    // manipulate command line arguments so CoreArchiver command will be executed
    $script = array_shift($_SERVER['argv']);
    $collectHome = TJWXJC_INCLUDE_PATH;

    echo "
-------------------------------------------------------
Using this 'archive.php' script is no longer recommended.
Please use '/path/to/php $collectHome/console core:archive " . implode(' ', $_SERVER['argv']) . "' instead.
To get help use '/path/to/php $collectHome/console core:archive --help'
See also: https://matomo.org/docs/setup-auto-archiving/

If you cannot use the console because it requires CLI
try 'php archive.php --url=http://your.collect/path'
-------------------------------------------------------
\n\n";

    array_unshift($_SERVER['argv'], 'core:archive');
    array_unshift($_SERVER['argv'], $script);

    $console->run();
} else { // if running via web request, use CoreAdminHome.runCronArchiving method
    Collect\Common::sendHeader('Content-type: text/plain');
    $_GET['module'] = 'API';
    $_GET['method'] = 'CoreAdminHome.runCronArchiving';
    $_GET['format'] = 'console'; // will use Content-type text/plain

    if('' === Collect\Common::getRequestVar('token_auth', '', 'string')) {
        echo "
<b>You must specify the Super User token_auth as a parameter to this script, eg. <code>?token_auth=XYZ</code> if you wish to run this script through the browser. </b>";
        exit;
    }

    require_once TJWXJC_INCLUDE_PATH . "/index.php";
}
