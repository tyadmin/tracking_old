<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

/**
 * This contains the bridge classes which were used prior to Collect 2.0
 * The serialized reports contains these classes below, which were not using namespaces yet
 */
namespace {

    use Collect\DataTable\Row;
    use Collect\DataTable\Row\DataTableSummaryRow;

    class Collect_DataTable_Row_DataTableSummary extends DataTableSummaryRow
    {
    }

    class Collect_DataTable_Row extends Row
    {
    }

    // only used for BC to unserialize old archived Row instances. We cannot unserialize Row directly as it implements
    // the Serializable interface and it would fail on PHP5.6+ when userializing the Row instance directly.
    class getCommandToChangeOwnerOfCollectFiles
    {
        public $c;
    }

}
