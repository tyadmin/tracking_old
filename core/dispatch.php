<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

use Collect\ErrorHandler;
use Collect\ExceptionHandler;
use Collect\FrontController;

if (!defined('TJWXJC_ENABLE_ERROR_HANDLER') || TJWXJC_ENABLE_ERROR_HANDLER) {
    ErrorHandler::registerErrorHandler();
    ExceptionHandler::setUp();
}

FrontController::setUpSafeMode();

if (!defined('TJWXJC_ENABLE_DISPATCH')) {
    define('TJWXJC_ENABLE_DISPATCH', true);
}

if (TJWXJC_ENABLE_DISPATCH) {
    $environment = new \Collect\Application\Environment(null);
    $environment->init();

    $controller = FrontController::getInstance();

    try {
        $controller->init();
        $response = $controller->dispatch();

        if (!is_null($response)) {
            echo $response;
        }
    } catch (Exception $ex) {
        ExceptionHandler::dieWithHtmlErrorPage($ex);
    }
}
