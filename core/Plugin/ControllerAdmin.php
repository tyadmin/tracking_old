<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugin;

use Collect\Config as CollectConfig;
use Collect\Config;
use Collect\Container\StaticContainer;
use Collect\Development;
use Collect\Menu\MenuAdmin;
use Collect\Menu\MenuTop;
use Collect\Notification;
use Collect\Notification\Manager as NotificationManager;
use Collect\Collect;
use Collect\Plugins\Marketplace\Marketplace;
use Collect\Tracker\TrackerConfig;
use Collect\Url;
use Collect\Version;
use Collect\View;
use Collect\ProxyHttp;
use Collect\SettingsCollect;

/**
 * Base class of plugin controllers that provide administrative functionality.
 *
 * See {@link Controller} to learn more about Collect controllers.
 *
 */
abstract class ControllerAdmin extends Controller
{
    private static function notifyWhenTrackingStatisticsDisabled()
    {
        $statsEnabled = CollectConfig::getInstance()->Tracker['record_statistics'];
        if ($statsEnabled == "0") {
            $notification = new Notification(Collect::translate('General_StatisticsAreNotRecorded'));
            $notification->context = Notification::CONTEXT_INFO;
            Notification\Manager::notify('ControllerAdmin_StatsAreNotRecorded', $notification);
        }
    }

    private static function notifyAnyInvalidLicense()
    {
        if (!Marketplace::isMarketplaceEnabled()) {
            return;
        }

        if (Collect::isUserIsAnonymous()) {
            return;
        }

        if (!Collect::isUserHasSomeAdminAccess()) {
            return;
        }

        if (Development::isEnabled()) {
            return;
        }

        $expired = StaticContainer::get('Collect\Plugins\Marketplace\Plugins\InvalidLicenses');

        $messageLicenseMissing = $expired->getMessageNoLicense();
        if (!empty($messageLicenseMissing)) {
            $notification = new Notification($messageLicenseMissing);
            $notification->raw = true;
            $notification->context = Notification::CONTEXT_ERROR;
            $notification->title = Collect::translate('Marketplace_LicenseMissing');
            Notification\Manager::notify('ControllerAdmin_LicenseMissingWarning', $notification);
        }

        $messageExceeded = $expired->getMessageExceededLicenses();
        if (!empty($messageExceeded)) {
            $notification = new Notification($messageExceeded);
            $notification->raw = true;
            $notification->context = Notification::CONTEXT_WARNING;
            $notification->title = Collect::translate('Marketplace_LicenseExceeded');
            Notification\Manager::notify('ControllerAdmin_LicenseExceededWarning', $notification);
        }

        $messageExpired = $expired->getMessageExpiredLicenses();
        if (!empty($messageExpired)) {
            $notification = new Notification($messageExpired);
            $notification->raw = true;
            $notification->context = Notification::CONTEXT_WARNING;
            $notification->title = Collect::translate('Marketplace_LicenseExpired');
            Notification\Manager::notify('ControllerAdmin_LicenseExpiredWarning', $notification);
        }
    }

    private static function notifyAnyInvalidPlugin()
    {
        if (!Collect::hasUserSuperUserAccess()) {
            return;
        }

        $missingPlugins = \Collect\Plugin\Manager::getInstance()->getMissingPlugins();

        if (empty($missingPlugins)) {
            return;
        }

        $pluginsLink = Url::getCurrentQueryStringWithParametersModified(array(
            'module' => 'CorePluginsAdmin', 'action' => 'plugins'
        ));

        $invalidPluginsWarning = Collect::translate('CoreAdminHome_InvalidPluginsWarning', array(
                self::getCollectVersion(),
                '<strong>' . implode('</strong>,&nbsp;<strong>', $missingPlugins) . '</strong>'))
            . "<br/>"
            . Collect::translate('CoreAdminHome_InvalidPluginsYouCanUninstall', array(
                '<a href="' . $pluginsLink . '"/>',
                '</a>'
        ));

        $notification = new Notification($invalidPluginsWarning);
        $notification->raw = true;
        $notification->context = Notification::CONTEXT_WARNING;
        $notification->title = Collect::translate('General_Warning');
        Notification\Manager::notify('ControllerAdmin_InvalidPluginsWarning', $notification);
    }

    /**
     * Calls {@link setBasicVariablesView()} and {@link setBasicVariablesAdminView()}
     * using the supplied view.
     *
     * @param View $view
     * @param string $viewType If 'admin', the admin variables are set as well as basic ones.
     */
    protected function setBasicVariablesViewAs($view, $viewType = 'admin')
    {
        $this->setBasicVariablesNoneAdminView($view);
        if ($viewType === 'admin') {
            self::setBasicVariablesAdminView($view);
        }
    }

    private static function notifyIfURLIsNotSecure()
    {
        $isURLSecure = ProxyHttp::isHttps();
        if ($isURLSecure) {
            return;
        }

        if (!Collect::hasUserSuperUserAccess()) {
            return;
        }

        if (Url::isLocalHost(Url::getCurrentHost())) {
            return;
        }

        if (Development::isEnabled()) {
            return;
        }

        $message = Collect::translate('General_CurrentlyUsingUnsecureHttp');

        $message .= " ";

        $message .= Collect::translate('General_ReadThisToLearnMore',
            array('<a rel="noreferrer noopener" target="_blank" href="https://matomo.org/faq/how-to/faq_91/">', '</a>')
          );

        $notification = new Notification($message);
        $notification->context = Notification::CONTEXT_WARNING;
        $notification->raw     = true;
        Notification\Manager::notify('ControllerAdmin_HttpIsUsed', $notification);
    }

    private static function notifyIfDevelopmentModeOnButNotInstalledThroughGit()
    {
        if (!Collect::hasUserSuperUserAccess()) {
            return;
        }

        if (!Development::isEnabled()) {
            return;
        }

        if (SettingsCollect::isGitDeployment()) {
            return;
        }

        $message = Collect::translate('General_WarningDevelopmentModeOnButNotGitInstalled');

        $notification = new Notification($message);
        $notification->context = Notification::CONTEXT_WARNING;
        $notification->raw = true;
        $notification->flags = Notification::FLAG_CLEAR;
        Notification\Manager::notify('ControllerAdmin_DevelopmentModeOn', $notification);
    }

    /**
     * @ignore
     */
    public static function displayWarningIfConfigFileNotWritable()
    {
        $isConfigFileWritable = CollectConfig::getInstance()->isFileWritable();

        if (!$isConfigFileWritable) {
            $exception = CollectConfig::getInstance()->getConfigNotWritableException();
            $message = $exception->getMessage();

            $notification = new Notification($message);
            $notification->raw     = true;
            $notification->context = Notification::CONTEXT_WARNING;
            Notification\Manager::notify('ControllerAdmin_ConfigNotWriteable', $notification);
        }
    }


    private static function notifyIfEAcceleratorIsUsed()
    {
        $isEacceleratorUsed = ini_get('eaccelerator.enable');
        if (empty($isEacceleratorUsed)) {
            return;
        }
        $message = sprintf("You are using the PHP accelerator & optimizer eAccelerator which is known to be not compatible with Matomo.
            We have disabled eAccelerator, which might affect the performance of Matomo.
            Read the %srelated ticket%s for more information and how to fix this problem.",
            '<a rel="noreferrer noopener" target="_blank" href="https://github.com/matomo-org/matomo/issues/4439">', '</a>');

        $notification = new Notification($message);
        $notification->context = Notification::CONTEXT_WARNING;
        $notification->raw     = true;
        Notification\Manager::notify('ControllerAdmin_EacceleratorIsUsed', $notification);
    }

    /**
     * PHP Version required by the next major Matomo version
     * @return string
     */
    private static function getNextRequiredMinimumPHP()
    {
        return '7.2';
    }

    private static function isUsingPhpVersionCompatibleWithNextCollect()
    {
        return version_compare( PHP_VERSION, self::getNextRequiredMinimumPHP(), '>=' );
    }

    private static function notifyWhenPhpVersionIsNotCompatibleWithNextMajorCollect()
    {
        if (self::isUsingPhpVersionCompatibleWithNextCollect()) {
            return;
        }

        $youMustUpgradePHP = Collect::translate('General_YouMustUpgradePhpVersionToReceiveLatestCollect');
        $message =  Collect::translate('General_CollectCannotBeUpgradedBecausePhpIsTooOld')
            .     ' '
            .  sprintf(Collect::translate('General_PleaseUpgradeYourPhpVersionSoYourCollectDataStaysSecure'), self::getNextRequiredMinimumPHP())
        ;

        $notification = new Notification($message);
        $notification->title = $youMustUpgradePHP;
        $notification->priority = Notification::PRIORITY_LOW;
        $notification->context = Notification::CONTEXT_WARNING;
        $notification->type = Notification::TYPE_TRANSIENT;
        $notification->flags = Notification::FLAG_NO_CLEAR;
        NotificationManager::notify('PHPVersionTooOldForNewestCollectCheck', $notification);
    }

    private static function notifyWhenPhpVersionIsEOL()
    {
        if (defined('TJWXJC_TEST_MODE')) { // to avoid changing every admin UI test
            return;
        }

        $notifyPhpIsEOL = Collect::hasUserSuperUserAccess() && ! self::isPhpVersionAtLeast71();
        if (!$notifyPhpIsEOL) {
            return;
        }

        $deprecatedMajorPhpVersion = PHP_MAJOR_VERSION . '.' . PHP_MINOR_VERSION;
        $message = Collect::translate('General_WarningCollectWillStopSupportingPHPVersion', array($deprecatedMajorPhpVersion, self::getNextRequiredMinimumPHP()))
            . "<br/> "
            . Collect::translate('General_WarningPhpVersionXIsTooOld', $deprecatedMajorPhpVersion);

        $notification = new Notification($message);
        $notification->raw = true;
        $notification->title = Collect::translate('General_Warning');
        $notification->priority = Notification::PRIORITY_LOW;
        $notification->context = Notification::CONTEXT_WARNING;
        $notification->type = Notification::TYPE_TRANSIENT;
        $notification->flags = Notification::FLAG_NO_CLEAR;
        NotificationManager::notify('PHP71VersionCheck', $notification);
    }

    private static function notifyWhenDebugOnDemandIsEnabled($trackerSetting)
    {
        if (!Development::isEnabled()
            && Collect::hasUserSuperUserAccess() &&
            TrackerConfig::getConfigValue($trackerSetting)) {

            $message = Collect::translate('General_WarningDebugOnDemandEnabled');
            $message = sprintf($message, '"' . $trackerSetting . '"', '"[Tracker] ' .  $trackerSetting . '"', '"0"',
                                               '"config/config.ini.php"');
            $notification = new Notification($message);
            $notification->title = Collect::translate('General_Warning');
            $notification->priority = Notification::PRIORITY_LOW;
            $notification->context = Notification::CONTEXT_WARNING;
            $notification->type = Notification::TYPE_TRANSIENT;
            $notification->flags = Notification::FLAG_NO_CLEAR;
            NotificationManager::notify('Tracker' . $trackerSetting, $notification);
        }
    }

    /**
     * Assigns view properties that would be useful to views that render admin pages.
     *
     * Assigns the following variables:
     *
     * - **statisticsNotRecorded** - Set to true if the `[Tracker] record_statistics` INI
     *                               config is `0`. If not `0`, this variable will not be defined.
     * - **topMenu** - The result of `MenuTop::getInstance()->getMenu()`.
     * - **enableFrames** - The value of the `[General] enable_framed_pages` INI config option. If
     *                    true, {@link Collect\View::setXFrameOptions()} is called on the view.
     * - **isSuperUser** - Whether the current user is a superuser or not.
     * - **usingOldGeoIPPlugin** - Whether this Collect install is currently using the old GeoIP
     *                             plugin or not.
     * - **invalidPluginsWarning** - Set if some of the plugins to load (determined by INI configuration)
     *                               are invalid or missing.
     * - **phpVersion** - The current PHP version.
     * - **phpIsNewEnough** - Whether the current PHP version is new enough to run Collect.
     * - **adminMenu** - The result of `MenuAdmin::getInstance()->getMenu()`.
     *
     * @param View $view
     * @api
     */
    public static function setBasicVariablesAdminView(View $view)
    {
        self::notifyWhenTrackingStatisticsDisabled();
        self::notifyIfEAcceleratorIsUsed();
        self::notifyIfURLIsNotSecure();
        self::notifyIfDevelopmentModeOnButNotInstalledThroughGit();

        $view->topMenu = MenuTop::getInstance()->getMenu();

        $view->isDataPurgeSettingsEnabled = self::isDataPurgeSettingsEnabled();
        $enableFrames = CollectConfig::getInstance()->General['enable_framed_settings'];
        $view->enableFrames = $enableFrames;

        if (!$enableFrames) {
            $view->setXFrameOptions('sameorigin');
        }

        $view->isSuperUser = Collect::hasUserSuperUserAccess();

        self::notifyAnyInvalidLicense();
        self::notifyAnyInvalidPlugin();
        self::notifyWhenPhpVersionIsEOL();
        self::notifyWhenPhpVersionIsNotCompatibleWithNextMajorCollect();
        self::notifyWhenDebugOnDemandIsEnabled('debug');
        self::notifyWhenDebugOnDemandIsEnabled('debug_on_demand');

        /**
         * Posted when rendering an admin page and notifications about any warnings or errors should be triggered.
         * You can use it for example when you have a plugin that needs to be configured in order to work and the
         * plugin has not been configured yet. It can be also used to cancel / remove other notifications by calling
         * eg `Notification\Manager::cancel($notificationId)`.
         *
         * **Example**
         *
         *     public function onTriggerAdminNotifications(Collect\Widget\WidgetsList $list)
         *     {
         *         if ($pluginFooIsNotConfigured) {
         *              $notification = new Notification('The plugin foo has not been configured yet');
         *              $notification->context = Notification::CONTEXT_WARNING;
         *              Notification\Manager::notify('fooNotConfigured', $notification);
         *         }
         *     }
         *
         */
        Collect::postEvent('Controller.triggerAdminNotifications');

        $view->adminMenu = MenuAdmin::getInstance()->getMenu();

        $notifications = $view->notifications;

        if (empty($notifications)) {
            $view->notifications = NotificationManager::getAllNotificationsToDisplay();
            NotificationManager::cancelAllNonPersistent();
        }
    }

    public static function isDataPurgeSettingsEnabled()
    {
        return (bool) Config::getInstance()->General['enable_delete_old_data_settings_admin'];
    }

    protected static function getCollectVersion()
    {
        return "Matomo " . Version::VERSION;
    }

    private static function isPhpVersionAtLeast71()
    {
        return version_compare(PHP_VERSION, '7.1', '>=');
    }
}
