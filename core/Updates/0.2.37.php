<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Updates;

use Collect\Common;
use Collect\Updater;
use Collect\Updates;
use Collect\Updater\Migration\Factory as MigrationFactory;

/**
 */
class Updates_0_2_37 extends Updates
{
    /**
     * @var MigrationFactory
     */
    private $migration;

    public function __construct(MigrationFactory $factory)
    {
        $this->migration = $factory;
    }

    public function getMigrations(Updater $updater)
    {
        return array(
            $this->migration->db->sql('DELETE FROM `' . Common::prefixTable('user_dashboard') . "`
                                       WHERE layout LIKE '%.getLastVisitsGraph%'
                                       OR layout LIKE '%.getLastVisitsReturningGraph%'"),
        );
    }

    public function doUpdate(Updater $updater)
    {
        $updater->executeMigrations(__FILE__, $this->getMigrations($updater));
    }
}
