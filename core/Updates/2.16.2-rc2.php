<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Updates;

use Collect\Updater;
use Collect\Updates as CollectUpdates;

/**
 * Update for version 2.16.2-rc2
 */
class Updates_2_16_2_rc2 extends CollectUpdates
{

    public function doUpdate(Updater $updater)
    {
        try {
            \Collect\Plugin\Manager::getInstance()->activatePlugin('ProfessionalServices');
        } catch (\Exception $e) {
        }

        try {
            \Collect\Plugin\Manager::getInstance()->deactivatePlugin('CollectPro');
            self::deletePluginFromConfigFile('CollectPro');
        } catch (\Exception $e) {
        }
    }
}
