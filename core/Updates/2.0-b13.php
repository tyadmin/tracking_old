<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Updates;

use Collect\Filesystem;
use Collect\Updates;
use Collect\Updater;

/**
 */
class Updates_2_0_b13 extends Updates
{
    public function doUpdate(Updater $updater)
    {
        $errors = array();

        // Deleting old libs
        $obsoleteDirectories = array(
            TJWXJC_INCLUDE_PATH . '/libs/Smarty',
            TJWXJC_INCLUDE_PATH . '/libs/Event',
        );

        foreach ($obsoleteDirectories as $dir) {
            if (file_exists($dir)) {
                Filesystem::unlinkRecursive($dir, true);
            }

            if (file_exists($dir)) {
                $errors[] = "Please delete this directory manually (eg. using your FTP software): $dir \n";
            }
        }
        if (!empty($errors)) {
            throw new \Exception("Warnings during the update: <br>" . implode("<br>", $errors));
        }
    }
}
