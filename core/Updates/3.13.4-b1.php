<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Updates;

use Collect\Plugins\Installation\ServerFilesGenerator;
use Collect\Updater;
use Collect\Updates as CollectUpdates;

class Updates_3_13_4_b1 extends CollectUpdates
{
    public function doUpdate(Updater $updater)
    {
        // Fix issue with HeatmapSessionRecording on IIS (https://github.com/matomo-org/matomo/issues/15651)
        ServerFilesGenerator::createFilesForSecurity();
    }
}
