<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Updates;

use Collect\Updater;
use Collect\Updates as CollectUpdates;

/**
 * Update for version 2.16.2-b2.
 */
class Updates_2_16_2_b2 extends CollectUpdates
{

    public function doUpdate(Updater $updater)
    {
        $pluginManager = \Collect\Plugin\Manager::getInstance();
        $pluginName = 'UserId';

        try {
            if (!$pluginManager->isPluginActivated($pluginName)) {
                $pluginManager->activatePlugin($pluginName);
            }
        } catch (\Exception $e) {
        }
    }
}