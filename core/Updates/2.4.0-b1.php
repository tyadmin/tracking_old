<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Updates;

use Collect\Updates;
use Collect\Updater;

class Updates_2_4_0_b1 extends Updates
{
    public function doUpdate(Updater $updater)
    {
        try {
            \Collect\Plugin\Manager::getInstance()->activatePlugin('Morpheus');
        } catch (\Exception $e) {
        }

        try {
            \Collect\Plugin\Manager::getInstance()->deactivatePlugin('Zeitgeist');
            self::deletePluginFromConfigFile('Zeitgeist');
        } catch (\Exception $e) {
        }
    }
}
