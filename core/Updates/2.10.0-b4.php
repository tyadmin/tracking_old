<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Updates;

use Collect\Updates;
use Collect\Updater;

/**
 * Update for version 2.10.0-b4.
 */
class Updates_2_10_0_b4 extends Updates
{

    public function doUpdate(Updater $updater)
    {
        $pluginManager = \Collect\Plugin\Manager::getInstance();

        try {
            $pluginManager->activatePlugin('BulkTracking');
        } catch (\Exception $e) {
        }
    }
}
