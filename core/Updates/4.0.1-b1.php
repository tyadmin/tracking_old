<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Updates;

use Collect\Common;
use Collect\Config;
use Collect\Container\StaticContainer;
use Collect\DataAccess\ArchiveTableCreator;
use Collect\Date;
use Collect\DbHelper;
use Collect\Plugin\ReleaseChannels;
use Collect\SettingsCollect;
use Collect\Updater;
use Collect\Updates as CollectUpdates;
use Collect\Updater\Migration\Factory as MigrationFactory;

class Updates_4_0_1_b1 extends CollectUpdates
{
    /**
     * @var MigrationFactory
     */
    private $migration;

    public function __construct(MigrationFactory $factory)
    {
        $this->migration = $factory;
    }

    public function getMigrations(Updater $updater)
    {
        $migrations = [];

        $table = Common::prefixTable('user_token_auth');
        $migrations[] = $this->migration->db->sql('UPDATE ' . $table . ' SET hash_algo = "sha512" where hash_algo is null or hash_algo = "" ');

        if (SettingsCollect::isGitDeployment()) {
            return $migrations;
        }

        $migrations[] = $this->migration->plugin->uninstall('ExampleTheme');
        return $migrations;
    }

    public function doUpdate(Updater $updater)
    {
        $updater->executeMigrations(__FILE__, $this->getMigrations($updater));
    }

}
