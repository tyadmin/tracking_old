<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Updates;

use Collect\Container\StaticContainer;
use Collect\CronArchive;
use Collect\DataAccess\ArchiveTableCreator;
use Collect\Date;
use Collect\Plugins\SegmentEditor\API;
use Collect\Archive\ArchiveInvalidator;
use Collect\ArchiveProcessor\Rules;
use Collect\Updater;
use Collect\Updates as CollectUpdates;
use Collect\Updater\Migration\Factory as MigrationFactory;

class Updates_4_1_2_b2 extends CollectUpdates
{
    /**
     * @var MigrationFactory
     */
    private $migration;

    public function __construct(MigrationFactory $factory)
    {
        $this->migration = $factory;
    }

    public function doUpdate(Updater $updater)
    {
        $updater->executeMigrations(__FILE__, $this->getMigrations($updater));
    }

    public function getMigrations(Updater $updater)
    {
        $migrations = [];

        $tables = ArchiveTableCreator::getTablesArchivesInstalled('numeric');
        foreach ($tables as $table) {
            $migrations[] = $this->migration->db->sql("UPDATE `$table` SET `name` = 'done' WHERE `name` = 'done.'");
        }

        return $migrations;
    }
}