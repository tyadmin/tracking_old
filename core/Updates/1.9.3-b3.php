<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Updates;

use Collect\Updates;
use Collect\Updater;

/**
 */
class Updates_1_9_3_b3 extends Updates
{
    public function doUpdate(Updater $updater)
    {
        // Insight was a temporary code name for Overlay
        $pluginToDelete = 'Insight';
        self::deletePluginFromConfigFile($pluginToDelete);
        \Collect\Plugin\Manager::getInstance()->deletePluginFromFilesystem($pluginToDelete);

        // We also clean up 1.9.1 and delete Feedburner plugin
        \Collect\Plugin\Manager::getInstance()->deletePluginFromFilesystem('Feedburner');
    }
}
