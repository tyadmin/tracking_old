<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Updates;

use Collect\Updater;
use Collect\Updates as CollectUpdates;
use Collect\Updater\Migration;
use Collect\Updater\Migration\Factory as MigrationFactory;
use Collect\Db;
use Collect\Common;

/**
 * Update for version 4.3.0-b3.
 */
class Updates_4_3_0_b3 extends CollectUpdates
{
    /**
     * @var MigrationFactory
     */
    private $migration;

    public function __construct(MigrationFactory $factory)
    {
        $this->migration = $factory;
    }

    public function getMigrations(Updater $updater)
    {
        $migrations = [];

        $migrations[] = $this->migration->db->addColumn('segment', 'hash', 'CHAR(32) NULL', 'definition');
        return $migrations;
    }

    public function doUpdate(Updater $updater)
    {
        $updater->executeMigrations(__FILE__, $this->getMigrations($updater));
    }
}
