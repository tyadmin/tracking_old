<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Updates;

use Collect\Filesystem;
use Collect\Updater;
use Collect\Updates as CollectUpdates;

/**
 * Update for version 4.6.2-rc2.
 */
class Updates_4_6_2_rc2 extends CollectUpdates
{
    /**
     * @param Updater $updater
     */
    public function doUpdate(Updater $updater)
    {
        Filesystem::unlinkRecursive(TJWXJC_INCLUDE_PATH . '/misc/composer', true);
        @unlink(TJWXJC_INCLUDE_PATH . '/node_modules/iframe-resizer/.eslintrc');
        @unlink(TJWXJC_INCLUDE_PATH . '/node_modules/jquery.dotdotdot/.npmignore');
        @unlink(TJWXJC_INCLUDE_PATH . '/node_modules/ng-dialog/.eslintrc');
    }
}
