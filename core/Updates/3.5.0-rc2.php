<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Updates;

use Collect\Plugins\PrivacyManager\PrivacyManager;
use Collect\Tracker\Cache;
use Collect\Updater;
use Collect\Updates as CollectUpdates;

class Updates_3_5_0_rc2 extends CollectUpdates
{
    public function doUpdate(Updater $updater)
    {
        // trigger salt for user if being created and stored in database
        PrivacyManager::getUserIdSalt();
        Cache::deleteTrackerCache();
    }
}
