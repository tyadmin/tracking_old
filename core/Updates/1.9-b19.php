<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Updates;

use Collect\Updater;
use Collect\Updates;
use Collect\Updater\Migration\Factory as MigrationFactory;

/**
 */
class Updates_1_9_b19 extends Updates
{
    /**
     * @var MigrationFactory
     */
    private $migration;

    public function __construct(MigrationFactory $factory)
    {
        $this->migration = $factory;
    }

    public function getMigrations(Updater $updater)
    {
        return array(
            $this->migration->db->changeColumnType('log_link_visit_action', 'idaction_url_ref', 'INT( 10 ) UNSIGNED NULL DEFAULT 0'),
            $this->migration->db->changeColumnType('log_visit', 'visit_exit_idaction_url', 'INT( 10 ) UNSIGNED NULL DEFAULT 0'),
            $this->migration->plugin->activate('Transitions'),
        );
    }

    public function doUpdate(Updater $updater)
    {
        $updater->executeMigrations(__FILE__, $this->getMigrations($updater));
    }
}
