<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Updates;

use Collect\Updates;
use Collect\Updater;

class Updates_2_10_0_b8 extends Updates
{
    public function doUpdate(Updater $updater)
    {
        $pluginManager = \Collect\Plugin\Manager::getInstance();

        try {
            $pluginManager->activatePlugin('Resolution');
        } catch (\Exception $e) {
        }
    }
}
