<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Updates;

use Collect\Plugins\Installation\ServerFilesGenerator;
use Collect\Updater;
use Collect\Updates as CollectUpdates;

class Updates_3_0_1_b1 extends CollectUpdates
{
    public function doUpdate(Updater $updater)
    {
        // Re-generate .htaccess without 'Options -Indexes' because it does not always work on some servers
        ServerFilesGenerator::createFilesForSecurity();
    }
}
