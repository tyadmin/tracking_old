<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Updates;

use Collect\Updates;
use Collect\Updater;

class Updates_2_4_0_b3 extends Updates
{
    public function doUpdate(Updater $updater)
    {
        $pluginManager = \Collect\Plugin\Manager::getInstance();

        try {
            $pluginManager->activatePlugin('LeftMenu');
        } catch (\Exception $e) {
        }

        try {
            $pluginManager->deactivatePlugin('Zeitgeist');
        } catch (\Exception $e) {
        }

        try {
            $pluginManager->uninstallPlugin('Zeitgeist');
        } catch (\Exception $e) {
        }
    }
}
