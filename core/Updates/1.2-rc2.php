<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Updates;

use Collect\Updates;
use Collect\Updater;

/**
 */
class Updates_1_2_rc2 extends Updates
{

    public function doUpdate(Updater $updater)
    {
        try {
            \Collect\Plugin\Manager::getInstance()->activatePlugin('CustomVariables');
        } catch (\Exception $e) {
        }
    }
}
