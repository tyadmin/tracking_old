<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Tracker\Visit;

use Collect\Collect;
use Collect\Tracker\Visit;
use Collect\Tracker\VisitInterface;
use Exception;

class Factory
{
    /**
     * Returns the Tracker_Visit object.
     * This method can be overwritten to use a different Tracker_Visit object
     *
     * @throws Exception
     * @return \Collect\Tracker\Visit
     */
    public static function make()
    {
        $visit = null;

        /**
         * Triggered before a new **visit tracking object** is created. Subscribers to this
         * event can force the use of a custom visit tracking object that extends from
         * {@link Collect\Tracker\VisitInterface}.
         *
         * @param \Collect\Tracker\VisitInterface &$visit Initialized to null, but can be set to
         *                                              a new visit object. If it isn't modified
         *                                              Collect uses the default class.
         */
        Collect::postEvent('Tracker.makeNewVisitObject', array(&$visit));

        if (!isset($visit)) {
            $visit = new Visit();
        } elseif (!($visit instanceof VisitInterface)) {
            throw new Exception("The Visit object set in the plugin must implement VisitInterface");
        }

        return $visit;
    }
}
