<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Validators;

use Collect\Site;

class IdSite extends BaseValidator
{
    public function validate($value)
    {
        new Site($value);
    }
}