<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Access\Role;

use Collect\Access\Role;
use Collect\Collect;

class Write extends Role
{
    public const ID = 'write';

    public function getName(): string
    {
        return Collect::translate('UsersManager_PrivWrite');
    }

    public function getId(): string
    {
        return self::ID;
    }

    public function getDescription(): string
    {
        return Collect::translate('UsersManager_PrivWriteDescription');
    }

    public function getHelpUrl(): string
    {
        return 'https://matomo.org/faq/general/faq_26910';
    }

}
