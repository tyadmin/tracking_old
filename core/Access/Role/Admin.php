<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Access\Role;

use Collect\Access\Role;
use Collect\Collect;

class Admin extends Role
{
    public const ID = 'admin';

    public function getName(): string
    {
        return Collect::translate('UsersManager_PrivAdmin');
    }

    public function getId(): string
    {
        return self::ID;
    }

    public function getDescription(): string
    {
        return Collect::translate('UsersManager_PrivAdminDescription', array(
            Collect::translate('UsersManager_PrivWrite')
        ));
    }

    public function getHelpUrl(): string
    {
        return 'https://matomo.org/faq/general/faq_69/';
    }

}
