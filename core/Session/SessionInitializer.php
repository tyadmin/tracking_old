<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Session;

use Exception;
use Collect\Auth as AuthInterface;
use Collect\AuthResult;
use Collect\Collect;
use Collect\Session;

/**
 * Initializes authenticated sessions using an Auth implementation.
 */
class SessionInitializer
{
    /**
     * Authenticates the user and, if successful, initializes an authenticated session.
     *
     * @param \Collect\Auth $auth The Auth implementation to use.
     * @throws Exception If authentication fails or the user is not allowed to login for some reason.
     */
    public function initSession(AuthInterface $auth)
    {
        $this->regenerateSessionId();

        $authResult = $this->doAuthenticateSession($auth);

        if (!$authResult->wasAuthenticationSuccessful()) {

            Collect::postEvent('Login.authenticate.failed', array($auth->getLogin()));

            $this->processFailedSession();
        } else {

            Collect::postEvent('Login.authenticate.successful', array($auth->getLogin()));

            $this->processSuccessfulSession($authResult);
        }
    }

    /**
     * Authenticates the user.
     *
     * Derived classes can override this method to customize authentication logic or impose
     * extra requirements on the user trying to login.
     *
     * @param AuthInterface $auth The Auth implementation to use when authenticating.
     * @return AuthResult
     */
    protected function doAuthenticateSession(AuthInterface $auth)
    {
        Collect::postEvent(
            'Login.authenticate',
            array(
                $auth->getLogin(),
            )
        );

        return $auth->authenticate();
    }

    /**
     * Executed when the session could not authenticate.
     *
     * @throws Exception always.
     */
    protected function processFailedSession()
    {
        throw new Exception(Collect::translate('Login_LoginPasswordNotCorrect'));
    }

    /**
     * Executed when the session was successfully authenticated.
     *
     * @param AuthResult $authResult The successful authentication result.
     */
    protected function processSuccessfulSession(AuthResult $authResult)
    {
        $sessionIdentifier = new SessionFingerprint();
        $sessionIdentifier->initialize($authResult->getIdentity(), $authResult->getTokenAuth(), $this->isRemembered());

        /**
         * @ignore
         */
        Collect::postEvent('Login.authenticate.processSuccessfulSession.end', array($authResult->getIdentity()));
    }

    protected function regenerateSessionId()
    {
        Session::regenerateId();
    }

    private function isRemembered()
    {
        $cookieParams = session_get_cookie_params();
        return $cookieParams['lifetime'] > 0;
    }
}
