<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Session;

use Collect\Common;
use Collect\Session;
use Zend_Session_Namespace;

/**
 * Session namespace.
 *
 */
class SessionNamespace extends Zend_Session_Namespace
{
    /**
     * @param string $namespace
     * @param bool $singleInstance
     */
    public function __construct($namespace = 'Default', $singleInstance = false)
    {
        if (Common::isPhpCliMode()) {
            self::$_readable = true;
            self::$_writable = true;
            return;
        }

        Session::start();

        parent::__construct($namespace, $singleInstance);
    }
}
