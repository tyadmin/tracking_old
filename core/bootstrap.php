<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 * Bootstraps the Collect application.
 *
 * This file cannot be a class because it needs to be compatible with PHP 4.
 */

if (!defined('TJWXJC_USER_PATH')) {
    define('TJWXJC_USER_PATH', TJWXJC_DOCUMENT_ROOT);
}

error_reporting(E_ALL | E_NOTICE);
@ini_set('display_errors', defined('TJWXJC_DISPLAY_ERRORS') ? TJWXJC_DISPLAY_ERRORS : @ini_get('display_errors'));
@ini_set('xdebug.show_exception_trace', 0);

if (!defined('TJWXJC_VENDOR_PATH')) {
	if (is_dir(TJWXJC_INCLUDE_PATH . '/vendor')) {
		define('TJWXJC_VENDOR_PATH', TJWXJC_INCLUDE_PATH . '/vendor'); // Collect is the main project
	} else {
		define('TJWXJC_VENDOR_PATH', TJWXJC_INCLUDE_PATH . '/../..'); // Collect is installed as a Composer dependency
	}
}

// NOTE: the code above must be PHP 4 compatible
require_once TJWXJC_INCLUDE_PATH . '/core/testMinimumPhpVersion.php';

if (session_status() !== PHP_SESSION_ACTIVE && !headers_sent()) {
    session_cache_limiter('nocache');
}

define('TJWXJC_DEFAULT_TIMEZONE', @date_default_timezone_get());
@date_default_timezone_set('UTC');

disableEaccelerator();

require_once TJWXJC_INCLUDE_PATH . '/libs/upgradephp/upgrade.php';

// Composer autoloader
require_once TJWXJC_VENDOR_PATH . '/autoload.php';

require_once TJWXJC_INCLUDE_PATH . '/DIObject.php';

\Collect\Plugin\Manager::initPluginDirectories();

/**
 * Eaccelerator does not support closures and is known to be not compatible with Collect. Therefore we are disabling
 * it automatically. At this point it looks like Eaccelerator is no longer under development and the bug has not
 * been fixed within a year.
 *
 */
function disableEaccelerator()
{
    $isEacceleratorUsed = ini_get('eaccelerator.enable');
    if (!empty($isEacceleratorUsed)) {
        @ini_set('eaccelerator.enable', 0);
    }
}
