<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\RssWidget;
use Collect\Plugins\RssWidget\Widgets\RssChangelog;
use Collect\Plugins\RssWidget\Widgets\RssCollect;
use Collect\SettingsCollect;
use Collect\Widget\WidgetsList;

/**
 *
 */
class RssWidget extends \Collect\Plugin
{
    /**
     * @see \Collect\Plugin::registerEvents
     */
    public function registerEvents()
    {
        return array(
            'AssetManager.getStylesheetFiles' => 'getStylesheetFiles',
            'Request.getRenamedModuleAndAction' => 'renameExampleRssWidgetModule',
            'Widget.filterWidgets' => 'filterWidgets'
        );
    }

    public function getStylesheetFiles(&$stylesheets)
    {
        $stylesheets[] = "plugins/RssWidget/stylesheets/rss.less";
    }

    public function renameExampleRssWidgetModule(&$module, &$action)
    {
        if ($module == 'ExampleRssWidget') {
            $module = 'RssWidget';
        }
    }

    /**
     * @param WidgetsList $list
     */
    public function filterWidgets($list)
    {
        if (!SettingsCollect::isInternetEnabled()) {
            $list->remove(RssChangelog::getCategory(), RssChangelog::getName());
            $list->remove(RssCollect::getCategory(), RssCollect::getName());
        }
    }
}
