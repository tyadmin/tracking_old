<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\Morpheus;

use Collect\Development;
use Collect\Collect;
use Collect\View;

class Controller extends \Collect\Plugin\Controller
{
    public function demo()
    {
        if (! Development::isEnabled() || !Collect::isUserHasSomeAdminAccess()) {
            return;
        }

        return $this->renderTemplate('demo');
    }
}
