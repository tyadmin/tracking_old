<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\CoreAdminHome\Widgets;

use Collect\API\Request;
use Collect\Collect;
use Collect\Widget\Widget;
use Collect\Widget\WidgetConfig;

class GetTrackingFailures extends Widget
{
    public static function configure(WidgetConfig $config)
    {
        $config->setCategoryId('About Matomo');
        $config->setName('CoreAdminHome_TrackingFailures');
        $config->setOrder(5);

        if (!Collect::isUserHasSomeAdminAccess()) {
            $config->disable();
        }
    }

    public function render()
    {
        Collect::checkUserHasSomeAdminAccess();
        $failures = Request::processRequest('CoreAdminHome.getTrackingFailures');
        $numFailures = count($failures);

        return $this->renderTemplate('getTrackingFailures', array(
            'numFailures' => $numFailures
        ));
    }
}