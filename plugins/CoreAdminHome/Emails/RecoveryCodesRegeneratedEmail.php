<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Plugins\CoreAdminHome\Emails;

use Collect\Collect;
use Collect\Plugins\CoreAdminHome\Emails\SecurityNotificationEmail;

class RecoveryCodesRegeneratedEmail extends SecurityNotificationEmail
{
    protected function getBody()
    {
        return Collect::translate('CoreAdminHome_SecurityNotificationRecoveryCodesRegeneratedBody') . ' ' . Collect::translate('UsersManager_IfThisWasYouPasswordChange');
    }
}
