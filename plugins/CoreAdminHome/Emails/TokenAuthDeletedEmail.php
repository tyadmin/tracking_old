<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Plugins\CoreAdminHome\Emails;

use Collect\Collect;
use Collect\Plugins\CoreAdminHome\Emails\SecurityNotificationEmail;

class TokenAuthDeletedEmail extends SecurityNotificationEmail
{
    /**
     * @var string
     */
    private $tokenDescription;

    /**
     * @var bool
     */
    private $all;

    public function __construct($login, $emailAddress, $tokenDescription, $all = false)
    {
        $this->tokenDescription = $tokenDescription;
        $this->all = $all;

        parent::__construct($login, $emailAddress);
    }

    protected function getBody()
    {
        if ($this->all) {
            return Collect::translate('CoreAdminHome_SecurityNotificationAllTokenAuthDeletedBody') . ' ' . Collect::translate('UsersManager_IfThisWasYouPasswordChange');
        }

        return Collect::translate('CoreAdminHome_SecurityNotificationTokenAuthDeletedBody', [$this->tokenDescription]) . ' ' . Collect::translate('UsersManager_IfThisWasYouPasswordChange');
    }
}
