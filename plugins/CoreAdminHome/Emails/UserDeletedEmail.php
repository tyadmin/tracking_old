<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Plugins\CoreAdminHome\Emails;

use Collect\Collect;
use Collect\Plugins\CoreAdminHome\Emails\SecurityNotificationEmail;

class UserDeletedEmail extends SecurityNotificationEmail
{
    /**
     * @var string
     */
    private $userLogin;

    public function __construct($login, $emailAddress, $userLogin)
    {
        $this->userLogin = $userLogin;

        parent::__construct($login, $emailAddress);
    }

    protected function getBody()
    {
        return Collect::translate('CoreAdminHome_SecurityNotificationUserDeletedBody', [$this->userLogin]) . ' ' . Collect::translate('UsersManager_IfThisWasYouPasswordChange');
    }
}
