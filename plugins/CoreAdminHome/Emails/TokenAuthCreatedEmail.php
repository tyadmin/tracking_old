<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Plugins\CoreAdminHome\Emails;

use Collect\Collect;
use Collect\Plugins\CoreAdminHome\Emails\SecurityNotificationEmail;

class TokenAuthCreatedEmail extends SecurityNotificationEmail
{
    /**
     * @var string
     */
    private $tokenDescription;

    public function __construct($login, $emailAddress, $tokenDescription)
    {
        $this->tokenDescription = $tokenDescription;

        parent::__construct($login, $emailAddress);
    }

    protected function getBody()
    {
        return Collect::translate('CoreAdminHome_SecurityNotificationTokenAuthCreatedBody', [$this->tokenDescription]) . ' ' . Collect::translate('UsersManager_IfThisWasYouPasswordChange');
    }
}
