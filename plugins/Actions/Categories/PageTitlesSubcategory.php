<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Actions\Categories;

use Collect\Category\Subcategory;
use Collect\Collect;

class PageTitlesSubcategory extends Subcategory
{
    protected $categoryId = 'General_Actions';
    protected $id = 'Actions_SubmenuPageTitles';
    protected $order = 20;

    public function getHelp()
    {
        return '<p>' . Collect::translate('Actions_PageTitlesSubcategoryHelp1') . '</p>'
            . '<p>' . Collect::translate('Actions_PageTitlesSubcategoryHelp2') . '</p>';
    }
}
