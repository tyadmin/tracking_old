<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Actions\Categories;

use Collect\Category\Subcategory;
use Collect\Collect;

class PagesSubcategory extends Subcategory
{
    protected $categoryId = 'General_Actions';
    protected $id = 'General_Pages';
    protected $order = 5;

    public function getHelp()
    {
        return '<p>' . Collect::translate('Actions_PagesSubcategoryHelp1') . '</p>'
            . '<p>' . Collect::translate('Actions_PagesSubcategoryHelp2') . '</p>'
            . '<p>' . Collect::translate('Actions_PagesSubcategoryHelp3') . '</p>';
    }
}
