<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Actions\Categories;

use Collect\Category\Subcategory;
use Collect\Collect;

class SiteSearchSubcategory extends Subcategory
{
    protected $categoryId = 'General_Actions';
    protected $id = 'Actions_SubmenuSitesearch';
    protected $order = 25;

    public function getHelp()
    {
        return '<p>' . Collect::translate('Actions_SiteSearchSubcategoryHelp1') . '</p>'
            . '<p>' . Collect::translate('Actions_SiteSearchSubcategoryHelp2') . '</p>'
            . '<p><a href="https://matomo.org/docs/site-search/" rel="noreferrer noopener" target="_blank">' . Collect::translate('Actions_SiteSearchSubcategoryHelp3') . '</a></p>';
    }
}
