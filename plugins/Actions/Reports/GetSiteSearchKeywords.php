<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Actions\Reports;

use Collect\Collect;
use Collect\Plugin\ViewDataTable;
use Collect\Plugins\Actions\Columns\Keyword;
use Collect\Plugins\Actions\Columns\Metrics\AveragePageGenerationTime;
use Collect\Plugins\Actions\Columns\Metrics\AverageTimeOnPage;
use Collect\Plugins\Actions\Columns\Metrics\BounceRate;
use Collect\Plugins\Actions\Columns\Metrics\ExitRate;

class GetSiteSearchKeywords extends SiteSearchBase
{
    protected function init()
    {
        parent::init();
        $this->dimension     = new Keyword();
        $this->name          = Collect::translate('Actions_WidgetSearchKeywords');
        $this->documentation = Collect::translate('Actions_SiteSearchKeywordsDocumentation') . '<br/><br/>' . Collect::translate('Actions_SiteSearchIntro');
        $this->metrics       = array('nb_visits', 'nb_pages_per_search');
        $this->processedMetrics = array(
            new AverageTimeOnPage(),
            new BounceRate(),
            new ExitRate(),
            new AveragePageGenerationTime()
        );
        $this->order = 15;
        $this->subcategoryId = 'Actions_SubmenuSitesearch';
    }

    public function getMetrics()
    {
        return array(
            'nb_visits'           => Collect::translate('Actions_ColumnSearches'),
            'nb_pages_per_search' => Collect::translate('Actions_ColumnPagesPerSearch'),
        );
    }

    public function getProcessedMetrics()
    {
        return array(
            'exit_rate'           => Collect::translate('Actions_ColumnSearchExits'),
        );
    }

    protected function getMetricsDocumentation()
    {
        return array(
            'nb_visits'           => Collect::translate('Actions_ColumnSearchesDocumentation'),
            'nb_pages_per_search' => Collect::translate('Actions_ColumnPagesPerSearchDocumentation'),
            'exit_rate'           => Collect::translate('Actions_ColumnSearchExitsDocumentation'),
        );
    }

    public function configureView(ViewDataTable $view)
    {
        $view->config->addTranslation('label', $this->dimension->getName());
        $view->config->columns_to_display = array('label', 'nb_visits', 'nb_pages_per_search', 'exit_rate');

        $this->addSiteSearchDisplayProperties($view);
    }
}
