<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Actions\Reports;

use Collect\DbHelper;
use Collect\Collect;
use Collect\Plugin\ReportsProvider;
use Collect\Plugin\ViewDataTable;
use Collect\Plugins\Actions\Columns\Metrics\AveragePageGenerationTime;
use Collect\Plugins\Actions\Columns\Metrics\BounceRate;
use Collect\Plugins\Actions\Columns\PageUrl;
use Collect\Plugins\Actions\Columns\Metrics\ExitRate;
use Collect\Plugins\Actions\Columns\Metrics\AverageTimeOnPage;
use Collect\Report\ReportWidgetFactory;
use Collect\Widget\WidgetsList;

class GetPageUrls extends Base
{
    protected function init()
    {
        parent::init();

        $this->dimension     = new PageUrl();
        $this->name          = Collect::translate('Actions_PageUrls');
        $this->documentation = Collect::translate('Actions_PagesReportDocumentation', '<br />')
                             . '<br />' . Collect::translate('General_UsePlusMinusIconsDocumentation');

        $this->actionToLoadSubTables = $this->action;
        $this->order   = 2;
        $this->metrics = array('nb_hits', 'nb_visits');
        $this->processedMetrics = array(
            new AverageTimeOnPage(),
            new BounceRate(),
            new ExitRate(),
            new AveragePageGenerationTime()
        );

        $this->subcategoryId = 'General_Pages';
    }

    public function configureWidgets(WidgetsList $widgetsList, ReportWidgetFactory $factory)
    {
        $widgetsList->addWidgetConfig($factory->createWidget()->setName($this->subcategoryId));
    }

    public function getMetrics()
    {
        $metrics = parent::getMetrics();
        $metrics['nb_visits'] = Collect::translate('General_ColumnUniquePageviews');

        return $metrics;
    }

    protected function getMetricsDocumentation()
    {
        $metrics = parent::getMetricsDocumentation();
        $metrics['nb_visits'] = Collect::translate('General_ColumnUniquePageviewsDocumentation');
        $metrics['bounce_rate'] = Collect::translate('General_ColumnPageBounceRateDocumentation');

        return $metrics;
    }

    public function configureView(ViewDataTable $view)
    {
        $view->config->addTranslation('label', $this->dimension->getName());
        $view->config->columns_to_display = array('label', 'nb_hits', 'nb_visits', 'bounce_rate',
                                                  'avg_time_on_page', 'exit_rate');

        if (version_compare(DbHelper::getInstallVersion(),'4.0.0-b1', '<')) {
            $view->config->columns_to_display[] = 'avg_time_generation';
        }

        $this->addPageDisplayProperties($view);
        $this->addBaseDisplayProperties($view);

        // related reports are only shown on performance page
        if ($view->requestConfig->getRequestParam('performance') !== '1') {
            $view->config->related_reports = [];
        }
    }

    public function getRelatedReports()
    {
        return [
            ReportsProvider::factory('Actions', 'getEntryPageUrls'),
            ReportsProvider::factory('Actions', 'getExitPageUrls'),
        ];
    }
}
