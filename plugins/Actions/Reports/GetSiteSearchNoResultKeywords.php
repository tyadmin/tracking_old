<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Actions\Reports;

use Collect\Collect;
use Collect\Plugin\ViewDataTable;
use Collect\Plugins\Actions\Columns\KeywordwithNoSearchResult;
use Collect\Plugins\Actions\Columns\Metrics\AveragePageGenerationTime;
use Collect\Plugins\Actions\Columns\Metrics\AverageTimeOnPage;
use Collect\Plugins\Actions\Columns\Metrics\BounceRate;
use Collect\Plugins\Actions\Columns\Metrics\ExitRate;

class GetSiteSearchNoResultKeywords extends SiteSearchBase
{
    protected function init()
    {
        parent::init();
        $this->dimension     = new KeywordwithNoSearchResult();
        $this->name          = Collect::translate('Actions_WidgetSearchNoResultKeywords');
        $this->documentation = Collect::translate('Actions_SiteSearchIntro') . '<br /><br />' . Collect::translate('Actions_SiteSearchKeywordsNoResultDocumentation');
        $this->metrics       = array('nb_visits');
        $this->processedMetrics = array(
            new AverageTimeOnPage(),
            new BounceRate(),
            new ExitRate(),
            new AveragePageGenerationTime()
        );
        $this->order = 18;

        $this->subcategoryId = 'Actions_SubmenuSitesearch';
    }

    public function getMetrics()
    {
        return array(
            'nb_visits' => Collect::translate('Actions_ColumnSearches')
        );
    }

    public function getProcessedMetrics()
    {
        return array(
            'exit_rate' => Collect::translate('Actions_ColumnSearchExits')
        );
    }

    protected function getMetricsDocumentation()
    {
        return array(
            'nb_visits' => Collect::translate('Actions_ColumnSearchesDocumentation'),
            'exit_rate' => Collect::translate('Actions_ColumnSearchExitsDocumentation'),
        );
    }

    public function configureView(ViewDataTable $view)
    {
        $view->config->addTranslation('label', $this->dimension->getName());
        $view->config->columns_to_display = array('label', 'nb_visits', 'exit_rate');

        $this->addSiteSearchDisplayProperties($view);
    }
}
