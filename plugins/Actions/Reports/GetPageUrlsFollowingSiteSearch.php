<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Actions\Reports;

use Collect\Collect;
use Collect\Plugin\ViewDataTable;
use Collect\Plugins\Actions\Columns\DestinationPage;
use Collect\Plugin\ReportsProvider;

class GetPageUrlsFollowingSiteSearch extends GetPageTitlesFollowingSiteSearch
{
    protected function init()
    {
        parent::init();
        $this->dimension     = new DestinationPage();
        $this->name          = Collect::translate('Actions_WidgetPageUrlsFollowingSearch');
        $this->documentation = Collect::translate('Actions_SiteSearchFollowingPagesDoc') . '<br/>' . Collect::translate('General_UsePlusMinusIconsDocumentation');
        $this->order = 16;

        $this->subcategoryId = 'Actions_SubmenuSitesearch';
    }

    public function configureView(ViewDataTable $view)
    {
        $title = Collect::translate('Actions_WidgetPageTitlesFollowingSearch');

        $this->configureViewForUrlAndTitle($view, $title);
    }

    public function getRelatedReports()
    {
        return array(
            ReportsProvider::factory('Actions', 'getPageTitlesFollowingSiteSearch'),
        );
    }
}
