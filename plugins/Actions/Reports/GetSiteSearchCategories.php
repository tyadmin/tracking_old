<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Actions\Reports;

use Collect\Collect;
use Collect\Plugin\ViewDataTable;
use Collect\Plugins\Actions\Columns\SearchCategory;
use Collect\Plugins\CoreVisualizations\Visualizations\HtmlTable;

class GetSiteSearchCategories extends SiteSearchBase
{
    protected function init()
    {
        parent::init();
        $this->dimension     = new SearchCategory();
        $this->name          = Collect::translate('Actions_WidgetSearchCategories');
        $this->documentation = Collect::translate('Actions_SiteSearchCategories1') . '<br/>' . Collect::translate('Actions_SiteSearchCategories2');
        $this->metrics       = array('nb_visits', 'nb_pages_per_search', 'exit_rate');
        $this->order = 20;

        $this->subcategoryId = 'Actions_SubmenuSitesearch';
    }

    protected function isEnabledForIdSites($idSites, $idSite)
    {
        return parent::isEnabledForIdSites($idSites, $idSite);
    }

    public function getMetrics()
    {
        return array(
            'nb_visits'           => Collect::translate('Actions_ColumnSearches'),
            'nb_pages_per_search' => Collect::translate('Actions_ColumnPagesPerSearch'),
            'exit_rate'           => Collect::translate('Actions_ColumnSearchExits'),
        );
    }

    protected function getMetricsDocumentation()
    {
        return array(
            'nb_visits'           => Collect::translate('Actions_ColumnSearchesDocumentation'),
            'nb_pages_per_search' => Collect::translate('Actions_ColumnPagesPerSearchDocumentation'),
            'exit_rate'           => Collect::translate('Actions_ColumnSearchExitsDocumentation'),
        );
    }

    public function configureView(ViewDataTable $view)
    {
        $view->config->addTranslations(array('label' => $this->dimension->getName()));

        $view->config->columns_to_display     = array('label', 'nb_visits', 'nb_pages_per_search');
        $view->config->show_table_all_columns = false;
        $view->config->show_bar_chart         = false;

        if ($view->isViewDataTableId(HtmlTable::ID)) {
            $view->config->disable_row_evolution = false;
        }
    }
}
