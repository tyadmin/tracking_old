<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Actions\Reports;

use Collect\Collect;
use Collect\Plugin\ViewDataTable;
use Collect\Plugins\Actions\Columns\DownloadUrl;

class GetDownloads extends Base
{
    protected function init()
    {
        parent::init();

        $this->dimension     = new DownloadUrl();
        $this->name          = Collect::translate('General_Downloads');
        $this->documentation = Collect::translate('Actions_DownloadsReportDocumentation', '<br />');
        $this->metrics       = array('nb_visits', 'nb_hits');

        $this->actionToLoadSubTables = $this->action;
        $this->order = 9;

        $this->subcategoryId = 'General_Downloads';
    }

    public function getMetrics()
    {
        return array(
            'nb_visits' => Collect::translate('Actions_ColumnUniqueDownloads'),
            'nb_hits'   => Collect::translate('General_Downloads')
        );
    }

    protected function getMetricsDocumentation()
    {
        return array(
            'nb_visits' => Collect::translate('Actions_ColumnUniqueClicksDocumentation'),
            'nb_hits'   => Collect::translate('Actions_ColumnClicksDocumentation')
        );
    }

    public function configureView(ViewDataTable $view)
    {
        $view->config->addTranslations(array('label' => $this->dimension->getName()));

        $view->config->columns_to_display = array('label', 'nb_visits', 'nb_hits');
        $view->config->show_exclude_low_population = false;

        $this->addBaseDisplayProperties($view);
    }
}
