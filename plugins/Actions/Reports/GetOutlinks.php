<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Actions\Reports;

use Collect\Collect;
use Collect\Plugin\ViewDataTable;
use Collect\Plugins\Actions\Columns\ClickedUrl;

class GetOutlinks extends Base
{
    protected function init()
    {
        parent::init();

        $this->dimension     = new ClickedUrl();
        $this->name          = Collect::translate('General_Outlinks');
        $this->documentation = Collect::translate('Actions_OutlinksReportDocumentation') . ' '
                             . Collect::translate('Actions_OutlinkDocumentation') . '<br />'
                             . Collect::translate('General_UsePlusMinusIconsDocumentation');

        $this->metrics = array('nb_visits', 'nb_hits');
        $this->order   = 8;

        $this->actionToLoadSubTables = $this->action;

        $this->subcategoryId = 'General_Outlinks';
    }

    public function getMetrics()
    {
        return array(
            'nb_visits' => Collect::translate('Actions_ColumnUniqueClicks'),
            'nb_hits'   => Collect::translate('Actions_ColumnClicks')
        );
    }

    protected function getMetricsDocumentation()
    {
        return array(
            'nb_visits' => Collect::translate('Actions_ColumnUniqueClicksDocumentation'),
            'nb_hits'   => Collect::translate('Actions_ColumnClicksDocumentation')
        );
    }

    public function configureView(ViewDataTable $view)
    {
        $view->config->addTranslations(array('label' => $this->dimension->getName()));

        $view->config->columns_to_display          = array('label', 'nb_visits', 'nb_hits');
        $view->config->show_exclude_low_population = false;

        $this->addBaseDisplayProperties($view);
    }
}
