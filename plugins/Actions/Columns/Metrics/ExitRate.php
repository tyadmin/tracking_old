<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
namespace Collect\Plugins\Actions\Columns\Metrics;

use Collect\DataTable\Row;
use Collect\Metrics\Formatter;
use Collect\Collect;
use Collect\Plugin\ProcessedMetric;

/**
 * Percent of visits that finished on this page. Calculated as:
 *
 *     exit_nb_visits / nb_visits
 *
 * exit_nb_visits & nb_visits are calculated by the Actions archiver.
 */
class ExitRate extends ProcessedMetric
{
    public function getName()
    {
        return 'exit_rate';
    }

    public function getTranslatedName()
    {
        return Collect::translate('General_ColumnExitRate');
    }

    public function compute(Row $row)
    {
        $exitVisits = $this->getMetric($row, 'exit_nb_visits');
        $visits = $this->getMetric($row, 'nb_visits');

        return Collect::getQuotientSafe($exitVisits, $visits, $precision = 2);
    }

    public function format($value, Formatter $formatter)
    {
        return $formatter->getPrettyPercentFromQuotient($value);
    }

    public function getDependentMetrics()
    {
        return array('exit_nb_visits', 'nb_visits');
    }
}