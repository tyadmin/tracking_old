<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Actions\Columns;

use Collect\Columns\DimensionSegmentFactory;
use Collect\Columns\Join\ActionNameJoin;
use Collect\Plugin\Dimension\ActionDimension;
use Collect\Plugins\Actions\Segment;
use Collect\Segment\SegmentsList;

class ActionUrl extends ActionDimension
{
    protected $nameSingular = 'Actions_ColumnActionURL';
    protected $columnName = 'idaction_url';
    protected $type = self::TYPE_URL;

    public function getDbColumnJoin()
    {
        return new ActionNameJoin();
    }

    public function configureSegments(SegmentsList $segmentsList, DimensionSegmentFactory $dimensionSegmentFactory)
    {
        $segment = new Segment();
        $segment->setSegment('actionUrl');
        $segment->setName('Actions_ColumnActionURL');
        $segment->setUnionOfSegments(array('pageUrl', 'downloadUrl', 'outlinkUrl', 'eventUrl'));

        $segmentsList->addSegment($dimensionSegmentFactory->createSegment($segment));
    }

}
