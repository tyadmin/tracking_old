<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Actions\Columns;

use Collect\Columns\Discriminator;
use Collect\Columns\Join\ActionNameJoin;
use Collect\Plugin\Dimension\ActionDimension;
use Collect\Tracker\Action;

class DownloadUrl extends ActionDimension
{
    protected $segmentName = 'downloadUrl';
    protected $nameSingular = 'Actions_ColumnDownloadURL';
    protected $namePlural = 'Actions_ColumnDownloadURLs';
    protected $columnName = 'idaction_url';
    protected $category = 'General_Actions';
    protected $suggestedValuesApi = 'Actions.getDownloads';
    protected $sqlFilter = '\\Collect\\Tracker\\TableLogAction::getIdActionFromSegment';
    protected $type = self::TYPE_URL;

    public function getDbColumnJoin()
    {
        return new ActionNameJoin();
    }

    public function getDbDiscriminator()
    {
        return new Discriminator('log_action', 'type', Action::TYPE_DOWNLOAD);
    }
}
