<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Actions\Columns;

use Collect\Columns\Dimension;
use Collect\Collect;

class Keyword extends Dimension
{
    protected $type = self::TYPE_TEXT;
    protected $nameSingular = 'General_ColumnKeyword';
}