<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Actions;

/**
 * Actions segment base class
 */
class Segment extends \Collect\Plugin\Segment
{
    protected  function init()
    {
        $this->setCategory('General_Actions');
        $this->setSqlFilter('\\Collect\\Tracker\\TableLogAction::getIdActionFromSegment');
    }
}

