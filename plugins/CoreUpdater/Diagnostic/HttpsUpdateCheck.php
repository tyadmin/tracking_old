<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
namespace Collect\Plugins\CoreUpdater\Diagnostic;

use Collect\Config;
use Collect\Plugins\CoreUpdater;
use Collect\Plugins\Diagnostics\Diagnostic\Diagnostic;
use Collect\Plugins\Diagnostics\Diagnostic\DiagnosticResult;
use Collect\Translation\Translator;

/**
 * Check the HTTPS update.
 */
class HttpsUpdateCheck implements Diagnostic
{
    /**
     * @var Translator
     */
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function execute()
    {
        $label = $this->translator->translate('Installation_SystemCheckUpdateHttps');

        if (CoreUpdater\Controller::isUpdatingOverHttps()) {
            return array(DiagnosticResult::singleResult($label, DiagnosticResult::STATUS_OK));
        }

        $comment = $this->translator->translate('Installation_SystemCheckUpdateHttpsNotSupported');

        return array(DiagnosticResult::singleResult($label, DiagnosticResult::STATUS_WARNING, $comment));
    }
}
