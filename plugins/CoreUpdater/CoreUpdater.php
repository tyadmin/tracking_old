<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\CoreUpdater;

use Exception;
use Collect\API\ResponseBuilder;
use Collect\Common;
use Collect\Filesystem;
use Collect\FrontController;
use Collect\Collect;
use Collect\SettingsCollect;
use Collect\UpdateCheck;
use Collect\Updater as CollectCoreUpdater;
use Collect\Version;

/**
 *
 */
class CoreUpdater extends \Collect\Plugin
{
    /**
     * @see \Collect\Plugin::registerEvents
     */
    public function registerEvents()
    {
        return array(
            'Request.dispatchCoreAndPluginUpdatesScreen' => 'dispatch',
            'Platform.initialized'                       => 'updateCheck',
        );
    }

    public function dispatch()
    {
        if (!SettingsCollect::isAutoUpdateEnabled()) {
            return;
        }

        $module = Common::getRequestVar('module', '', 'string');
        $action = Common::getRequestVar('action', '', 'string');

        if ($module == 'CoreUpdater'
            // Proxy module is used to redirect users to collect.org, should still work when Collect must be updated
            || $module == 'Proxy'
            // Do not show update page during installation.
            || $module == 'Installation'
            || ($module == 'CorePluginsAdmin' && $action == 'deactivate')
            || ($module == 'CorePluginsAdmin' && $action == 'uninstall')
            || ($module == 'LanguagesManager' && $action == 'saveLanguage')) {
            return;
        }

        $updater = new CollectCoreUpdater();
        $updates = $updater->getComponentsWithNewVersion(array('core' => Version::VERSION));

        if (!empty($updates)) {
            Filesystem::deleteAllCacheOnUpdate();
        }

        if ($updater->getComponentUpdates() !== null) {
            if (FrontController::shouldRethrowException()) {
                throw new Exception("Collect and/or some plugins have been upgraded to a new version. \n" .
                    "--> Please run the update process first. See documentation: https://matomo.org/docs/update/ \n");
            } elseif ($module === 'API' && ('' == $action || 'index' == $action))  {

                $outputFormat = strtolower(Common::getRequestVar('format', 'xml', 'string', $_GET + $_POST));
                $response = new ResponseBuilder($outputFormat);
                $e = new Exception('Database Upgrade Required. Your Matomo database is out-of-date, and must be upgraded before you can continue.');
                echo $response->getResponseException($e);
                Common::sendResponseCode(503);
                exit;

            } else {
                Collect::redirectToModule('CoreUpdater');
            }
        }
    }

    public function updateCheck()
    {
        UpdateCheck::check();
    }
}
