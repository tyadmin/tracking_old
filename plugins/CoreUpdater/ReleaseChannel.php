<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\CoreUpdater;

use Collect\Common;
use Collect\Config;
use Collect\Db;
use Collect\Http;
use Collect\Plugins\SitesManager\API;
use Collect\Url;
use Collect\Version;
use Collect\UpdateCheck\ReleaseChannel as BaseReleaseChannel;

abstract class ReleaseChannel extends BaseReleaseChannel
{
    public function getUrlToCheckForLatestAvailableVersion()
    {
        $parameters = array(
            'collect_version'   => Version::VERSION,
            'php_version'     => PHP_VERSION,
            'mysql_version'   => Db::get()->getServerVersion(),
            'release_channel' => $this->getId(),
            'url'             => Url::getCurrentUrlWithoutQueryString(),
            'trigger'         => Common::getRequestVar('module', '', 'string'),
            'timezone'        => API::getInstance()->getDefaultTimezone(),
        );

        $url = Config::getInstance()->General['api_service_url']
            . '/1.0/getLatestVersion/'
            . '?' . Http::buildQuery($parameters);

        return $url;
    }

    public function getDownloadUrlWithoutScheme($version)
    {
        return sprintf('://builds.matomo.org/collect-%s.zip', $version);
    }
}