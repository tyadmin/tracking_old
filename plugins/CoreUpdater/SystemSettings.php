<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\CoreUpdater;

use Collect\Db\Settings;
use Collect\DbHelper;
use Collect\Collect;
use Collect\Plugin\ReleaseChannels;
use Collect\Plugins\CoreAdminHome\Controller as CoreAdminController;
use Collect\Plugins\Marketplace\UpdateCommunication as PluginUpdateCommunication;
use Collect\Settings\Setting;
use Collect\Settings\FieldConfig;
use Collect\SettingsCollect;

/**
 * Defines Settings for CoreUpdater.
 *
 * Usage like this:
 * $settings = new SystemSettings();
 * $settings->metric->getValue();
 * $settings->description->getValue();
 */
class SystemSettings extends \Collect\Settings\Plugin\SystemSettings
{
    /** @var Setting */
    public $releaseChannel;

    /** @var Setting */
    public $sendPluginUpdateEmail;

    /** @var Setting */
    public $updateToUtf8mb4;

    /**
     * @var ReleaseChannels
     */
    private $releaseChannels;

    public function __construct(ReleaseChannels $releaseChannels)
    {
        $this->releaseChannels = $releaseChannels;

        parent::__construct();
    }

    protected function init()
    {
        $this->title = Collect::translate('CoreAdminHome_UpdateSettings');

        $isWritable = Collect::hasUserSuperUserAccess() && CoreAdminController::isGeneralSettingsAdminEnabled();
        $this->releaseChannel = $this->createReleaseChannel();
        $this->releaseChannel->setIsWritableByCurrentUser($isWritable
            && SettingsCollect::isMultiServerEnvironment() === false);

        $this->sendPluginUpdateEmail = $this->createSendPluginUpdateEmail();
        $this->sendPluginUpdateEmail->setIsWritableByCurrentUser($isWritable
            && PluginUpdateCommunication::canBeEnabled());

        $dbSettings = new Settings();
        if ($isWritable && $dbSettings->getUsedCharset() !== 'utf8mb4' && DbHelper::getDefaultCharset() === 'utf8mb4') {
            $this->updateToUtf8mb4 = $this->createUpdateToUtf8mb4();
        }
    }

    private function createReleaseChannel()
    {
        $releaseChannels = $this->releaseChannels;
        $default = 'latest_stable';

        return $this->makeSettingManagedInConfigOnly('General', 'release_channel', $default, FieldConfig::TYPE_STRING, function (FieldConfig $field) use ($releaseChannels) {

            $field->introduction = Collect::translate('CoreAdminHome_ReleaseChannel');
            $field->uiControl = FieldConfig::UI_CONTROL_RADIO;

            $field->availableValues = array();
            foreach ($releaseChannels->getAllReleaseChannels() as $channel) {
                $name = $channel->getName();
                $description = $channel->getDescription();
                if (!empty($description)) {
                    $name .= ' (' . $description . ')';
                }

                $field->availableValues[$channel->getId()] = $name;
            }

            $field->validate = function ($channel) use ($releaseChannels) {
                if (!$releaseChannels->isValidReleaseChannelId($channel)) {
                    throw new \Exception('Release channel is not valid');
                }
            };

            $field->inlineHelp = Collect::translate('CoreAdminHome_DevelopmentProcess')
                            . '<br/>'
                            . Collect::translate('CoreAdminHome_StableReleases',
                                               array("<a target='_blank' rel='noreferrer noopener' href='https://developer.matomo.org/guides/core-team-workflow#influencing-collect-development'>",
                                                     "</a>"))
                            . '<br/>'
                            . Collect::translate('CoreAdminHome_LtsReleases');
        });
    }

    private function createSendPluginUpdateEmail()
    {
        return $this->makeSetting('enable_plugin_update_communication', $default = true, FieldConfig::TYPE_BOOL, function (FieldConfig $field) {
            $field->introduction = Collect::translate('CoreAdminHome_SendPluginUpdateCommunication');
            $field->uiControl = FieldConfig::UI_CONTROL_RADIO;
            $field->availableValues = array('1' => sprintf('%s (%s)', Collect::translate('General_Yes'), Collect::translate('General_Default')),
                                            '0' => Collect::translate('General_No'));
            $field->inlineHelp = Collect::translate('CoreAdminHome_SendPluginUpdateCommunicationHelp');
        });
    }

    private function createUpdateToUtf8mb4()
    {
        return $this->makeSetting('update_to_utf8mb4', $default = false, FieldConfig::TYPE_BOOL, function (FieldConfig $field) {
            $field->introduction = Collect::translate('CoreUpdater_ConvertToUtf8mb4');
            $field->title = Collect::translate('CoreUpdater_TriggerDatabaseConversion');
            $field->uiControl = FieldConfig::UI_CONTROL_CHECKBOX;
            $field->inlineHelp = Collect::translate('CoreUpdater_Utf8mb4ConversionHelp', [
                '�',
                '<code>' . TJWXJC_INCLUDE_PATH . '/console core:convert-to-utf8mb4</code>',
                '<a href="https://matomo.org/faq/how-to-update/how-to-convert-the-database-to-utf8mb4-charset/" rel="noreferrer noopener" target="_blank">',
                '</a>'
            ]);
        });
    }

}
