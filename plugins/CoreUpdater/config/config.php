<?php

return array(
    'Collect\Plugins\CoreUpdater\Updater' => DI\autowire()
        ->constructorParameter('tmpPath', DI\get('path.tmp')),

    'diagnostics.optional' => DI\add(array(
        DI\get('Collect\Plugins\CoreUpdater\Diagnostic\HttpsUpdateCheck'),
    )),
);
