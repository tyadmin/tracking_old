<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\CoreUpdater\ReleaseChannel;

use Collect\Collect;
use Collect\Plugins\CoreUpdater\ReleaseChannel;

class LatestBeta extends ReleaseChannel
{
    public function getId()
    {
        return 'latest_beta';
    }

    public function getName()
    {
        return Collect::translate('CoreUpdater_LatestBetaRelease');
    }

    public function doesPreferStable()
    {
        return false;
    }

    public function getOrder()
    {
        return 11;
    }
}