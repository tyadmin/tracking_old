<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\CoreUpdater\ReleaseChannel;

use Collect\Collect;
use Collect\Plugins\CoreUpdater\ReleaseChannel;

class LatestStable extends ReleaseChannel
{
    public function getId()
    {
        return 'latest_stable';
    }

    public function getName()
    {
        return Collect::translate('CoreUpdater_LatestStableRelease');
    }

    public function getDescription()
    {
        return Collect::translate('General_Recommended');
    }

    public function getDownloadUrlWithoutScheme($version)
    {
        return '://builds.matomo.org/collect.zip';
    }

    public function getOrder()
    {
        return 10;
    }
}