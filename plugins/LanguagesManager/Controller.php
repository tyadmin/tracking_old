<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 *
 */
namespace Collect\Plugins\LanguagesManager;

use Collect\Common;
use Collect\DbHelper;
use Collect\Nonce;
use Collect\Collect;
use Collect\Url;

/**
 */
class Controller extends \Collect\Plugin\ControllerAdmin
{
    /**
     * anonymous = in the session
     * authenticated user = in the session
     */
    public function saveLanguage()
    {
        $language = Common::getRequestVar('language');
        $nonce = Common::getRequestVar('nonce', '');

        // Prevent CSRF only when collect is not installed yet (During install user can change language)
        if (DbHelper::isInstalled()) {
            $this->checkTokenInUrl();
        }

        Nonce::checkNonce(LanguagesManager::LANGUAGE_SELECTION_NONCE, $nonce);

        LanguagesManager::setLanguageForSession($language);
        Url::redirectToReferrer();
    }

    public function searchTranslation()
    {
        Collect::checkUserHasSomeAdminAccess();

        return $this->renderTemplate('searchTranslation');
    }
}
