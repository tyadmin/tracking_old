/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

/**
 * Usage:
 *
 * <div collect-translation-search></div>
 *
 * Will show a text box which allows the user to search for translation keys and actual translations. Currently,
 * only english is supported.
 */
(function () {
    angular.module('collectApp').directive('collectTranslationSearch', collectTranslationSearch);

    collectTranslationSearch.$inject = ['collect'];

    function collectTranslationSearch(collect){

        return {
            restrict: 'A',
            scope: {},
            templateUrl: 'plugins/LanguagesManager/angularjs/translationsearch/translationsearch.directive.html?cb=' + collect.cacheBuster,
            controller: 'TranslationSearchController',
            controllerAs: 'translationSearch'
        };
    }
})();