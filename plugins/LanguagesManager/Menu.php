<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\LanguagesManager;

use Collect\Development;
use Collect\Menu\MenuAdmin;
use Collect\Menu\MenuTop;
use Collect\Collect;
use Collect\SettingsCollect;

class Menu extends \Collect\Plugin\Menu
{
    public function configureTopMenu(MenuTop $menu)
    {
        if (Collect::isUserIsAnonymous() || !SettingsCollect::isMatomoInstalled()) {
            $langManager = new LanguagesManager();
            $menu->addHtml('LanguageSelector', $langManager->getLanguagesSelector(), true, $order = 30, false);
        }
    }

    public function configureAdminMenu(MenuAdmin $menu)
    {
        if (Development::isEnabled() && Collect::isUserHasSomeAdminAccess()) {
            $menu->addDevelopmentItem('LanguagesManager_TranslationSearch',
                                      $this->urlForAction('searchTranslation'));
        }
    }
}
