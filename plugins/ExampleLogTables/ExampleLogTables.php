<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license https://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\ExampleLogTables;

use Collect\Common;
use Collect\Plugins\ExampleLogTables\Dao\CustomUserLog;
use Collect\Plugins\ExampleLogTables\Dao\CustomGroupLog;

class ExampleLogTables extends \Collect\Plugin
{
    public function registerEvents()
    {
        return [
            'Db.getTablesInstalled' => 'getTablesInstalled'
        ];
    }

    public function install()
    {
        // Install custom log table [disabled as example only]

        // $userLog = new CustomUserLog();
        // $userLog->install();

        // $userLog = new CustomGroupLog();
        // $userLog->install();
    }

    /**
     * Register the new tables, so Matomo knows about them.
     *
     * @param array $allTablesInstalled
     */
    public function getTablesInstalled(&$allTablesInstalled)
    {
        $allTablesInstalled[] = Common::prefixTable('log_group');
        $allTablesInstalled[] = Common::prefixTable('log_custom');
    }
}