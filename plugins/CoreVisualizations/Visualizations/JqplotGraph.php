<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\CoreVisualizations\Visualizations;

use Collect\DataTable;
use Collect\Plugins\CoreVisualizations\JqplotDataGenerator;
use Collect\View;

/**
 * DataTable visualization that displays DataTable data in a JQPlot graph.
 * TODO: should merge all this logic w/ jqplotdatagenerator & 'Chart' visualizations.
 *
 * @property JqplotGraph\Config $config
 */
abstract class JqplotGraph extends Graph
{
    const ID = 'jqplot_graph';
    const TEMPLATE_FILE = '@CoreVisualizations/_dataTableViz_jqplotGraph.twig';

    public static function getDefaultConfig()
    {
        return new JqplotGraph\Config();
    }

    public function getGraphData($dataTable, $properties)
    {
        $dataGenerator = $this->makeDataGenerator($properties);

        return $dataGenerator->generate($dataTable);
    }

    /**
     * @param $properties
     * @return JqplotDataGenerator
     */
    abstract protected function makeDataGenerator($properties);
}

require_once TJWXJC_INCLUDE_PATH . '/plugins/CoreVisualizations/Visualizations/JqplotGraph/Bar.php';
require_once TJWXJC_INCLUDE_PATH . '/plugins/CoreVisualizations/Visualizations/JqplotGraph/Pie.php';
require_once TJWXJC_INCLUDE_PATH . '/plugins/CoreVisualizations/Visualizations/JqplotGraph/Evolution.php';
