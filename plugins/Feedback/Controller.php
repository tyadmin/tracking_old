<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Feedback;

use Collect\Date;
use Collect\View;
use Collect\Collect;
use Collect\Common;
use Collect\Version;
use Collect\Container\StaticContainer;
use Collect\DataTable\Renderer\Json;

class Controller extends \Collect\Plugin\Controller
{
    function index()
    {
        $view = new View('@Feedback/index');
        $this->setGeneralVariablesView($view);
        $popularHelpTopics = StaticContainer::get('popularHelpTopics');
        $view->popularHelpTopics = $popularHelpTopics;
        $view->collectVersion = Version::VERSION;
        return $view->render();
    }

}
