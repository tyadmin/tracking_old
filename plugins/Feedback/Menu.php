<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Feedback;

use Collect\Menu\MenuTop;
use Collect\Collect;

class Menu extends \Collect\Plugin\Menu
{
    public function configureTopMenu(MenuTop $menu)
    {
        $menu->registerMenuIcon('General_Help', 'icon-info2');
        $menu->addItem('General_Help', null, array('module' => 'Feedback', 'action' => 'index'), $order = 990, Collect::translate('General_Help'));
    }
}
