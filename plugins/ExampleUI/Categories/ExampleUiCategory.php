<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\ExampleUI\Categories;

use Collect\Category\Category;

class ExampleUiCategory extends Category
{
    protected $id = 'ExampleUI_UiFramework';
    protected $order = 90;
}
