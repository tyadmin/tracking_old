<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\ExampleUI\Reports;

use Collect\Collect;
use Collect\Plugin\Report;
use Collect\Plugin\ViewDataTable;
use Collect\Plugins\CoreVisualizations\Visualizations\Cloud;

/**
 * This class defines a new report.
 *
 */
class GetPlanetRatiosWithLogos extends Base
{
    protected function init()
    {
        parent::init();

        $this->name = Collect::translate('Advanced tag cloud: with logos and links');
        $this->documentation = 'This report shows a sample tab cloud.';
        $this->subcategoryId = 'Tag clouds';
        $this->order = 113;
    }

    public function getDefaultTypeViewDataTable()
    {
        return Cloud::ID;
    }

    public function configureView(ViewDataTable $view)
    {
        $view->config->display_logo_instead_of_label = true;
        $view->config->columns_to_display = array('label', 'value');
        $view->config->addTranslation('value', 'times the diameter of Earth');
    }

}
