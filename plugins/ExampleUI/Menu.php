<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\ExampleUI;

use Collect\Menu\MenuAdmin;

class Menu extends \Collect\Plugin\Menu
{
    public function configureAdminMenu(MenuAdmin $menu)
    {
        $menu->addPlatformItem('ExampleUI_UiNotifications', $this->urlForAction('notifications'), $order = 10);
    }
}
