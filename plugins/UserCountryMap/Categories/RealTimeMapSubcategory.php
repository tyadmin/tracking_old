<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\UserCountryMap\Categories;

use Collect\Category\Subcategory;
use Collect\Collect;

class RealTimeMapSubcategory extends Subcategory
{
    protected $categoryId = 'General_Visitors';
    protected $id = 'UserCountryMap_RealTimeMap';
    protected $order = 9;

    public function getHelp()
    {
        return '<p>' . Collect::translate('UserCountryMap_RealTimeMapHelp') . '</p>';
    }
}
