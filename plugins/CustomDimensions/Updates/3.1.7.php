<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Plugins\CustomDimensions;

use Collect\Updater;
use Collect\Updates as CollectUpdates;
use Collect\Updater\Migration\Factory as MigrationFactory;

/**
 * Update for version 3.1.7.
 */
class Updates_3_1_7 extends CollectUpdates
{
    /**
     * @var MigrationFactory
     */
    private $migration;

    public function __construct(MigrationFactory $factory)
    {
        $this->migration = $factory;
    }

    public function getMigrations(Updater $updater)
    {
        $migration1 = $this->migration->db->dropIndex('custom_dimensions', 'idcustomdimension_idsite');
        $migration2 = $this->migration->db->addPrimaryKey('custom_dimensions', array('idcustomdimension', 'idsite'));

        return array(
            $migration1,
            $migration2
        );
    }

    public function doUpdate(Updater $updater)
    {
        $updater->executeMigrations(__FILE__, $this->getMigrations($updater));
    }
}
