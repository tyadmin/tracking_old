/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
(function () {
    angular.module('collectApp').controller('CustomDimensionsListController', CustomDimensionsListController);

    CustomDimensionsListController.$inject = ['customDimensionsModel', 'collect'];

    function CustomDimensionsListController(customDimensionsModel, collect) {
        customDimensionsModel.fetchCustomDimensionsConfiguration();

        this.siteName = collect.siteName;

        this.model = customDimensionsModel;
    }
})();