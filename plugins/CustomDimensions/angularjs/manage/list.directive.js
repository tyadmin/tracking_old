/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

/**
 * Usage:
 * <div collect-custom-dimensions-list>
 */
(function () {
    angular.module('collectApp').directive('collectCustomDimensionsList', collectCustomDimensionsList);

    collectCustomDimensionsList.$inject = ['collect'];

    function collectCustomDimensionsList(collect){

        return {
            restrict: 'A',
            scope: {},
            templateUrl: 'plugins/CustomDimensions/angularjs/manage/list.directive.html?cb=' + collect.cacheBuster,
            controller: 'CustomDimensionsListController',
            controllerAs: 'dimensionsList'
        };
    }
})();