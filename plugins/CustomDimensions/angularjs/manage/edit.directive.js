/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

/**
 * Usage:
 * <div collect-custom-dimensions-edit>
 */
(function () {
    angular.module('collectApp').directive('collectCustomDimensionsEdit', collectCustomDimensionsEdit);

    collectCustomDimensionsEdit.$inject = ['collect'];

    function collectCustomDimensionsEdit(collect){

        return {
            restrict: 'A',
            scope: {
                dimensionId: '=',
                dimensionScope: '=',
            },
            templateUrl: 'plugins/CustomDimensions/angularjs/manage/edit.directive.html?cb=' + collect.cacheBuster,
            controller: 'CustomDimensionsEditController',
            controllerAs: 'editDimension'
        };
    }
})();