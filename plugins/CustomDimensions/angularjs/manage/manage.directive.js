/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

/**
 * Usage:
 * <div collect-custom-dimensions-manage>
 */
(function () {
    angular.module('collectApp').directive('collectCustomDimensionsManage', collectManageCustomDimensions);

    collectManageCustomDimensions.$inject = ['collect'];

    function collectManageCustomDimensions(collect){

        return {
            restrict: 'A',
            scope: {},
            templateUrl: 'plugins/CustomDimensions/angularjs/manage/manage.directive.html?cb=' + collect.cacheBuster,
            controller: 'ManageCustomDimensionsController',
            controllerAs: 'manageDimensions'
        };
    }
})();