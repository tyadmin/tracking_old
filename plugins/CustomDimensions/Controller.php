<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\CustomDimensions;

use Collect\Common;
use Collect\DataTable;
use Collect\Collect;
use Collect\View;

class Controller extends \Collect\Plugin\ControllerAdmin
{
    public function manage()
    {
        $idSite = Common::getRequestVar('idSite');

        Collect::checkUserHasWriteAccess($idSite);

        return $this->renderTemplate('manage', array(
            'idSite' => $this->idSite,
            'title' => Collect::translate('CustomDimensions_CustomDimensions'
        )));
    }

}

