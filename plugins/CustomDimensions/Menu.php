<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\CustomDimensions;

use Collect\Common;
use Collect\Menu\MenuAdmin;
use Collect\Collect;
use Collect\Plugins\UsersManager\UserPreferences;

/**
 * This class allows you to add, remove or rename menu items.
 * To configure a menu (such as Admin Menu, Reporting Menu, User Menu...) simply call the corresponding methods as
 * described in the API-Reference http://developer.collect.org/api-reference/Collect/Menu/MenuAbstract
 */
class Menu extends \Collect\Plugin\Menu
{
    public function configureAdminMenu(MenuAdmin $menu)
    {
        $userPreferences = new UserPreferences();
        $default = $userPreferences->getDefaultWebsiteId();
        $idSite = Common::getRequestVar('idSite', $default, 'int');

        if (Collect::isUserHasWriteAccess($idSite)) {
            $menu->addMeasurableItem('CustomDimensions_CustomDimensions', $this->urlForAction('manage'), $orderId = 41);
        }
    }

}
