<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
namespace Collect\Plugins\CustomDimensions\Columns\Metrics;

use Collect\DataTable\Row;
use Collect\Metrics\Formatter;
use Collect\Collect;
use Collect\Plugins\Actions\Columns\Metrics\AverageTimeOnPage;

/**
 * The average amount of time spent on a dimension. Calculated as:
 *
 *     sum_time_spent / nb_visits
 *
 * sum_time_spent and nb_visits are calculated by Archiver classes.
 */
class AverageTimeOnDimension extends AverageTimeOnPage
{
    public function getName()
    {
        return 'avg_time_on_dimension';
    }

    public function getTranslatedName()
    {
        return Collect::translate('CustomDimensions_ColumnAvgTimeOnDimension');
    }

}