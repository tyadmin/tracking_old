<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Referrers\Widgets;

use Collect\Common;
use Collect\Collect;
use Collect\Plugin;
use Collect\Widget\WidgetConfig;
use Collect\SettingsCollect;

class GetCampaignUrlBuilder extends \Collect\Widget\Widget
{
    public static function configure(WidgetConfig $config)
    {
        $config->setCategoryId('Referrers_Referrers');
        $config->setSubcategoryId('Referrers_URLCampaignBuilder');
        $config->setName('Referrers_URLCampaignBuilder');

        $idSite = self::getIdSite();
        if (!Collect::isUserHasViewAccess($idSite)) {
            $config->disable();
        }
    }

    private static function getIdSite()
    {
        return Common::getRequestVar('idSite', 0, 'int');
    }

    public function render()
    {
        $idSite = self::getIdSite();
        Collect::checkUserHasViewAccess($idSite);

        $hasExtraPlugin = Plugin\Manager::getInstance()->isPluginActivated('MarketingCampaignsReporting');

        return $this->renderTemplate('campaignBuilder', array(
            'hasExtraPlugin' => (int)$hasExtraPlugin
        ));
    }

}
