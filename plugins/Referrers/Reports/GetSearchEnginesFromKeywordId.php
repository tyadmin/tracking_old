<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Referrers\Reports;

use Collect\Collect;
use Collect\Plugin\ViewDataTable;
use Collect\Plugins\Referrers\Columns\SearchEngine;

class GetSearchEnginesFromKeywordId extends Base
{
    protected function init()
    {
        parent::init();
        $this->dimension     = new SearchEngine();
        $this->name          = Collect::translate('Referrers_Keywords');
        $this->documentation = Collect::translate('Referrers_KeywordsReportDocumentation', '<br />');
        $this->isSubtableReport = true;
        $this->order = 4;
    }

    public function configureView(ViewDataTable $view)
    {
        $view->config->show_search = false;
        $view->config->show_exclude_low_population = false;
        $view->config->addTranslation('label', $this->dimension->getName());
    }

}
