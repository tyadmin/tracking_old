<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Referrers\Categories;

use Collect\Category\Subcategory;
use Collect\Collect;

class SearchEnginesSubcategory extends Subcategory
{
    protected $categoryId = 'Referrers_Referrers';
    protected $id = 'Referrers_SubmenuSearchEngines';
    protected $order = 10;

    public function getHelp()
    {
        return '<p>' . Collect::translate('Referrers_SearchEnginesSubcategoryHelp1') . '</p>'
            . '<p>' . Collect::translate('Referrers_SearchEnginesSubcategoryHelp2', ['<a href="https://matomo.org/matomo-cloud/" rel="noreferrer noopener" target="_blank">', '</a>', '<a href="https://plugins.matomo.org/SearchEngineKeywordsPerformance" rel="noreferrer noopener" target="_blank">', '</a>']) . '</p>';
    }
}
