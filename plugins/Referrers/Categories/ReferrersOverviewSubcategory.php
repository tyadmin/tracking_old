<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Referrers\Categories;

use Collect\Category\Subcategory;
use Collect\Collect;

class ReferrersOverviewSubcategory extends Subcategory
{
    protected $categoryId = 'Referrers_Referrers';
    protected $id = 'General_Overview';
    protected $order = 2;

    public function getHelp()
    {
        return '<p>' . Collect::translate('Referrers_ReferrersOverviewSubcategoryHelp1') . '</p>'
            . '<p>' . Collect::translate('Referrers_ReferrersOverviewSubcategoryHelp2') . '</p>';
    }
}
