<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Referrers\Categories;

use Collect\Category\Category;
use Collect\Collect;

class ReferrersCategory extends Category
{
    protected $id = 'Referrers_Referrers';
    protected $order = 15;
    protected $icon = 'icon-reporting-referer';

    public function getDisplayName()
    {
        return Collect::translate('Referrers_Acquisition');
    }
}
