<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Referrers\DataTable\Filter;

use Collect\DataTable\Row;
use Collect\DataTable;

class KeywordNotDefined extends DataTable\Filter\ColumnCallbackReplace
{
    /**
     * Constructor.
     *
     * @param DataTable $table The table to eventually filter.
     */
    public function __construct($table)
    {
        parent::__construct($table, 'label', 'Collect\Plugins\Referrers\API::getCleanKeyword');
    }
}