<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\WebsiteMeasurable;
use Collect\IP;
use Collect\Measurable\Type\TypeManager;
use Matomo\Network\IPUtils;
use Collect\Collect;
use Collect\Plugin;
use Collect\Plugins\WebsiteMeasurable\Settings\Urls;
use Collect\Settings\Setting;
use Collect\Settings\FieldConfig;
use Collect\Plugins\SitesManager;
use Exception;

/**
 * Defines Settings for ExampleSettingsPlugin.
 *
 * Usage like this:
 * $settings = new MeasurableSettings($idSite);
 * $settings->autoRefresh->getValue();
 * $settings->metric->getValue();
 */
class MeasurableSettings extends \Collect\Settings\Measurable\MeasurableSettings
{
    /** @var Setting */
    public $urls;

    /** @var Setting */
    public $onlyTrackWhitelstedUrls;

    /** @var Setting */
    public $keepPageUrlFragments;

    /** @var Setting */
    public $excludeKnownUrls;

    /** @var Setting */
    public $excludedUserAgents;

    /** @var Setting */
    public $excludedIps;

    /** @var Setting */
    public $siteSearch;

    /** @var Setting */
    public $useDefaultSiteSearchParams;

    /** @var Setting */
    public $siteSearchKeywords;

    /** @var Setting */
    public $siteSearchCategory;

    /** @var Setting */
    public $excludedParameters;

    /** @var Setting */
    public $ecommerce;

    /**
     * @var SitesManager\API
     */
    private $sitesManagerApi;

    /**
     * @var Plugin\Manager
     */
    private $pluginManager;

    /**
     * @var TypeManager
     */
    private $typeManager;

    /**
     * @var bool
     */
    private $unsetSiteSearchKeywords = false;

    public function __construct(SitesManager\API $api, Plugin\Manager $pluginManager, TypeManager $typeManager, $idSite, $idMeasurableType)
    {
        $this->sitesManagerApi = $api;
        $this->pluginManager = $pluginManager;
        $this->typeManager = $typeManager;

        parent::__construct($idSite, $idMeasurableType);
    }

    protected function shouldShowSettingsForType($type)
    {
        $isWebsite = $type === Type::ID;

        if ($isWebsite) {
            return true;
        }

        // if no such type exists, we default to website properties
        return !$this->typeManager->isExistingType($type);
    }

    protected function init()
    {
        if (!$this->shouldShowSettingsForType($this->idMeasurableType)) {
            return;
        }

        $this->urls = new Urls($this->idSite);
        $this->addSetting($this->urls);

        $this->excludeKnownUrls = $this->makeExcludeUnknownUrls();
        $this->keepPageUrlFragments = $this->makeKeepUrlFragments($this->sitesManagerApi);
        $this->excludedIps = $this->makeExcludeIps();
        $this->excludedParameters = $this->makeExcludedParameters();
        $this->excludedUserAgents = $this->makeExcludedUserAgents();

        /**
         * SiteSearch
         */
        $this->siteSearch = $this->makeSiteSearch();
        $this->useDefaultSiteSearchParams = $this->makeUseDefaultSiteSearchParams($this->sitesManagerApi);
        $this->siteSearchKeywords = $this->makeSiteSearchKeywords();

        $siteSearchKeywords = $this->siteSearchKeywords->getValue();
        $areSiteSearchKeywordsEmpty = empty($siteSearchKeywords) || (is_array($siteSearchKeywords) && implode("", $siteSearchKeywords) == "");
        $this->useDefaultSiteSearchParams->setDefaultValue($areSiteSearchKeywordsEmpty);

        $this->siteSearchCategory = $this->makeSiteSearchCategory($this->pluginManager);
        /**
         * SiteSearch End
         */

        $this->ecommerce = $this->makeEcommerce();
    }

    private function makeExcludeUnknownUrls()
    {
        return $this->makeProperty('exclude_unknown_urls', $default = false, FieldConfig::TYPE_BOOL, function (FieldConfig $field) {
            $field->title = Collect::translate('SitesManager_OnlyMatchedUrlsAllowed');
            $field->inlineHelp = Collect::translate('SitesManager_OnlyMatchedUrlsAllowedHelp')
                . '<br />'
                . Collect::translate('SitesManager_OnlyMatchedUrlsAllowedHelpExamples');
            $field->uiControl = FieldConfig::UI_CONTROL_CHECKBOX;
        });
    }

    private function makeKeepUrlFragments(SitesManager\API $sitesManagerApi)
    {
        return $this->makeProperty('keep_url_fragment', $default = '0', FieldConfig::TYPE_STRING, function (FieldConfig $field) use ($sitesManagerApi) {
            $field->title = Collect::translate('SitesManager_KeepURLFragmentsLong');
            $field->uiControl = FieldConfig::UI_CONTROL_SINGLE_SELECT;

            if ($sitesManagerApi->getKeepURLFragmentsGlobal()) {
                $default = Collect::translate('General_Yes');
            } else {
                $default = Collect::translate('General_No');
            }

            $field->availableValues = array(
                '0' => $default . ' (' . Collect::translate('General_Default') . ')',
                '1' => Collect::translate('General_Yes'),
                '2' => Collect::translate('General_No')
            );
        });
    }

    private function makeExcludeIps()
    {
        return $this->makeProperty('excluded_ips', $default = array(), FieldConfig::TYPE_ARRAY, function (FieldConfig $field) {
            $ip = IP::getIpFromHeader();

            $field->title = Collect::translate('SitesManager_ExcludedIps');
            $field->inlineHelp = Collect::translate('SitesManager_HelpExcludedIpAddresses', array('1.2.3.4/24', '1.2.3.*', '1.2.*.*'))
                . '<br /><br />'
                . Collect::translate('SitesManager_YourCurrentIpAddressIs', array('<i>' . $ip . '</i>'));
            $field->uiControl = FieldConfig::UI_CONTROL_TEXTAREA;
            $field->uiControlAttributes = array(
              'cols' => '20',
              'rows' => '4',
              'placeholder' => $ip,
            );

            $field->validate = function ($value) {
                if (!empty($value)) {
                    $ips = array_map('trim', $value);
                    $ips = array_filter($ips, 'strlen');

                    foreach ($ips as $ip) {
                        if (IPUtils::getIPRangeBounds($ip) === null) {
                            throw new Exception(Collect::translate('SitesManager_ExceptionInvalidIPFormat', array($ip, "1.2.3.4, 1.2.3.*, or 1.2.3.4/5")));
                        }
                    }
                }
            };
            $field->transform = function ($value) {
                if (empty($value)) {
                    return array();
                }

                $ips = array_map('trim', $value);
                $ips = array_filter($ips, 'strlen');
                return $ips;
            };
        });
    }

    private function makeExcludedParameters()
    {
        $self = $this;
        return $this->makeProperty('excluded_parameters', $default = array(), FieldConfig::TYPE_ARRAY, function (FieldConfig $field) use ($self) {
            $field->title = Collect::translate('SitesManager_ExcludedParameters');
            $field->inlineHelp = Collect::translate('SitesManager_ListOfQueryParametersToExclude', "/^sess.*|.*[dD]ate$/")
                . '<br /><br />'
                . Collect::translate('SitesManager_CollectWillAutomaticallyExcludeCommonSessionParameters', array('phpsessid, sessionid, ...'));
            $field->uiControl = FieldConfig::UI_CONTROL_TEXTAREA;
            $field->uiControlAttributes = array('cols' => '20', 'rows' => '4');
            $field->transform = function ($value) use ($self) {
                return $self->checkAndReturnCommaSeparatedStringList($value);
            };
        });
    }

    private function makeExcludedUserAgents()
    {
        $self = $this;
        return $this->makeProperty('excluded_user_agents', $default = array(), FieldConfig::TYPE_ARRAY, function (FieldConfig $field) use ($self) {
            $field->title = Collect::translate('SitesManager_ExcludedUserAgents');
            $field->inlineHelp = Collect::translate('SitesManager_GlobalExcludedUserAgentHelp1')
                . '<br /><br />'
                . Collect::translate('SitesManager_GlobalListExcludedUserAgents_Desc')
                . '<br />'
                . Collect::translate('SitesManager_GlobalExcludedUserAgentHelp2') . " "
                . Collect::translate('SitesManager_GlobalExcludedUserAgentHelp3', "/bot|spider|crawl|scanner/i")
            ;
            $field->uiControl = FieldConfig::UI_CONTROL_TEXTAREA;
            $field->uiControlAttributes = array('cols' => '20', 'rows' => '4');
            $field->transform = function ($value) use ($self) {
                return $self->checkAndReturnCommaSeparatedStringList($value);
            };
        });
    }

    private function makeSiteSearch()
    {
        return $this->makeProperty('sitesearch', $default = 1, FieldConfig::TYPE_INT, function (FieldConfig $field) {
            $field->title = Collect::translate('Actions_SubmenuSitesearch');
            $field->inlineHelp = Collect::translate('SitesManager_SiteSearchUse');
            $field->uiControl = FieldConfig::UI_CONTROL_SINGLE_SELECT;
            $field->availableValues = array(
                1 => Collect::translate('SitesManager_EnableSiteSearch'),
                0 => Collect::translate('SitesManager_DisableSiteSearch')
            );
        });
    }

    private function makeUseDefaultSiteSearchParams(SitesManager\API $sitesManagerApi)
    {
        $settings = $this;
        return $this->makeSetting('use_default_site_search_params', $default = true, FieldConfig::TYPE_BOOL, function (FieldConfig $field) use ($sitesManagerApi, $settings) {

            if (Collect::hasUserSuperUserAccess()) {
                $title = Collect::translate('SitesManager_SearchUseDefault', array("<a href='#globalSettings'>","</a>"));
            } else {
                $title = Collect::translate('SitesManager_SearchUseDefault', array('', ''));
            }

            $field->title = $title;
            $field->uiControl = FieldConfig::UI_CONTROL_CHECKBOX;

            $searchKeywordsGlobal = $sitesManagerApi->getSearchKeywordParametersGlobal();

            $hasParams = (int) !empty($searchKeywordsGlobal);
            $field->condition = $hasParams . ' && sitesearch';

            $searchKeywordsGlobal = $sitesManagerApi->getSearchKeywordParametersGlobal();
            $searchCategoryGlobal = $sitesManagerApi->getSearchCategoryParametersGlobal();

            $field->description  = Collect::translate('SitesManager_SearchKeywordLabel');
            $field->description .= ' (' . Collect::translate('General_Default') . ')';
            $field->description .= ': ';
            $field->description .= $searchKeywordsGlobal;
            $field->description .= ' & ';
            $field->description .= Collect::translate('SitesManager_SearchCategoryLabel');
            $field->description .= ': ';
            $field->description .= $searchCategoryGlobal;
            $field->transform = function ($value) use ($settings) {
                if ($value) {
                    $settings->unsetSiteSearchKeywords = true;
                }
                return null; // never actually save a value for this
            };
        });
    }

    private function makeSiteSearchKeywords()
    {
        $settings = $this;
        return $this->makeProperty('sitesearch_keyword_parameters', $default = array(), FieldConfig::TYPE_ARRAY, function (FieldConfig $field) use ($settings) {
            $field->title = Collect::translate('SitesManager_SearchKeywordLabel');
            $field->uiControl = FieldConfig::UI_CONTROL_TEXT;
            $field->inlineHelp = Collect::translate('SitesManager_SearchKeywordParametersDesc');
            $field->condition = 'sitesearch && !use_default_site_search_params';
            $field->transform = function ($value) use ($settings) {
                if ($settings->unsetSiteSearchKeywords) {
                    return '';
                }
                return $value;
            };
        });
    }

    private function makeSiteSearchCategory(Plugin\Manager $pluginManager)
    {
        return $this->makeProperty('sitesearch_category_parameters', $default = array(), FieldConfig::TYPE_ARRAY, function (FieldConfig $field) use ($pluginManager) {
            $field->title = Collect::translate('SitesManager_SearchCategoryLabel');
            $field->uiControl = FieldConfig::UI_CONTROL_TEXT;
            $field->inlineHelp = Collect::translate('Goals_Optional')
                . '<br /><br />'
                . Collect::translate('SitesManager_SearchCategoryParametersDesc');

            $field->condition = 'sitesearch && !use_default_site_search_params';
        });
    }

    private function makeEcommerce()
    {
        return $this->makeProperty('ecommerce', $default = 0, FieldConfig::TYPE_INT, function (FieldConfig $field) {
            $field->title = Collect::translate('Goals_Ecommerce');
            $field->inlineHelp = Collect::translate('SitesManager_EcommerceHelp')
                . '<br />'
                . Collect::translate('SitesManager_CollectOffersEcommerceAnalytics',
                    array("<a href='https://matomo.org/docs/ecommerce-analytics/' target='_blank'>", '</a>'));
            $field->uiControl = FieldConfig::UI_CONTROL_SINGLE_SELECT;
            $field->availableValues = array(
                0 => Collect::translate('SitesManager_NotAnEcommerceSite'),
                1 => Collect::translate('SitesManager_EnableEcommerce')
            );
        });
    }

    public function checkAndReturnCommaSeparatedStringList($parameters)
    {
        if (empty($parameters)) {
            return array();
        }

        $parameters = array_map('trim', $parameters);
        $parameters = array_filter($parameters, 'strlen');
        $parameters = array_unique($parameters);
        return $parameters;
    }

}
