<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\VisitorInterest;

use Collect\ArchiveProcessor;
use Collect\FrontController;
use Collect\Metrics;
use Collect\Collect;
use Collect\Plugins\CoreVisualizations\Visualizations\Cloud;
use Collect\Plugins\CoreVisualizations\Visualizations\Graph;

class VisitorInterest extends \Collect\Plugin
{
    function postLoad()
    {
        Collect::addAction('Template.footerVisitsFrequency', array('Collect\Plugins\VisitorInterest\VisitorInterest', 'footerVisitsFrequency'));
    }

   public static function footerVisitsFrequency(&$out)
    {
        $out .= FrontController::getInstance()->fetchDispatch('VisitorInterest', 'index');
    }
}
