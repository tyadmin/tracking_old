<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\VisitorInterest\Columns;

use Collect\Columns\Dimension;
use Collect\Collect;

class VisitDuration extends Dimension
{

    protected $type = self::TYPE_DURATION_S;
    protected $nameSingular = 'VisitorInterest_ColumnVisitDuration';
}