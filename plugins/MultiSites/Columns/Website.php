<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\MultiSites\Columns;

use Collect\Columns\Dimension;
use Collect\Collect;

class Website extends Dimension
{
    public function getName()
    {
        return Collect::translate('General_Website');
    }
}