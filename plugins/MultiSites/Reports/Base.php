<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\MultiSites\Reports;

use Collect\Collect;
use Collect\Plugins\MultiSites\API;

abstract class Base extends \Collect\Plugin\Report
{
    protected function init()
    {
        $this->categoryId = 'General_MultiSitesSummary';

        $allMetricsInfo = API::getApiMetrics($enhanced = true);

        $metadataMetrics = array();
        $processedMetricsMetadata = array();

        foreach ($allMetricsInfo as $metricName => $metricSettings) {
            $metadataMetrics[$metricName] =
                Collect::translate($metricSettings[API::METRIC_TRANSLATION_KEY]);

            $processedMetricsMetadata[$metricSettings[API::METRIC_EVOLUTION_COL_NAME_KEY]] =
                Collect::translate($metricSettings[API::METRIC_TRANSLATION_KEY]) . " " . Collect::translate('MultiSites_Evolution');
        }

        $this->metrics = array_keys($metadataMetrics);
        $this->processedMetrics = array_keys($processedMetricsMetadata);
    }

}
