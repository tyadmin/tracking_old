<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\MultiSites\Reports;

use Collect\Collect;
use Collect\Plugins\MultiSites\Columns\Website;

class GetOne extends Base
{
    protected function init()
    {
        parent::init();
        $this->dimension     = new Website();
        $this->name          = Collect::translate('General_SingleWebsitesDashboard');
        $this->documentation = Collect::translate('MultiSites_SingleWebsitesDashboardDocumentation');
        $this->constantRowsCount = false;
        $this->order = 5;
    }

}
