/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
(function () {
    angular.module('collectApp').controller('MultiSitesDashboardController', MultiSitesDashboardController);

    MultiSitesDashboardController.$inject = ['$scope', 'collect', 'multisitesDashboardModel'];

    function MultiSitesDashboardController($scope, collect, multisitesDashboardModel){

        $scope.model = multisitesDashboardModel;

        $scope.evolutionSelector = 'visits_evolution';
        $scope.hasSuperUserAccess = collect.hasSuperUserAccess;
        $scope.date = collect.broadcast.getValueFromUrl('date');
        $scope.idSite = collect.broadcast.getValueFromUrl('idSite');
        $scope.url  = collect.collect_url;
        $scope.period = collect.period;
        $scope.areAdsForProfessionalServicesEnabled = collect.config && collect.config.are_ads_enabled;

        this.refresh = function (interval) {
            multisitesDashboardModel.refreshInterval = interval;
            multisitesDashboardModel.fetchAllSites();
        };
    }
})();
