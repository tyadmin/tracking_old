<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Dashboard\Categories;

use Collect\Category\Category;
use Collect\Collect;

class DashboardCategory extends Category
{
    protected $id = 'Dashboard_Dashboard';
    protected $order = 0;
    protected $icon = 'icon-reporting-dashboard';

    public function getHelp()
    {
        return '<p>' . Collect::translate('Dashboard_DashboardCategoryHelp', ['<strong>', '</strong>']) . '</p>';
    }
}
