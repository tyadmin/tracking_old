<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Dashboard;

use Collect\Db;
use Collect\Menu\MenuTop;
use Collect\Collect;
use Collect\Plugins\UsersManager\UserPreferences;
use Collect\Site;

/**
 */
class Menu extends \Collect\Plugin\Menu
{
    public function configureTopMenu(MenuTop $menu)
    {
        $userPreferences = new UserPreferences();
        $idSite = $userPreferences->getDefaultWebsiteId();
        $tooltip = Collect::translate('Dashboard_TopLinkTooltip', Site::getNameFor($idSite));

        $urlParams = $this->urlForModuleActionWithDefaultUserParams('CoreHome', 'index') ;
        $menu->addItem('Dashboard_Dashboard', null, $urlParams, 1, $tooltip);
    }
}

