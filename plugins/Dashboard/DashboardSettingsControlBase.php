<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link     https://matomo.org
 * @license  http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
namespace Collect\Plugins\Dashboard;

use Collect\View\UIControl;

/**
 * Generates the HTML for the dashboard manager control.
 */
abstract class DashboardSettingsControlBase extends UIControl
{
    const TEMPLATE = "@Dashboard/_dashboardSettings";

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->cssClass = "borderedControl collectTopControl dashboardSettings";
        $this->htmlAttributes = array('collect-expand-on-click' => '');
        $this->dashboardActions = array();
        $this->generalActions = array();
    }
}