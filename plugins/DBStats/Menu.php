<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\DBStats;

use Collect\Menu\MenuAdmin;
use Collect\Collect;

/**
 */
class Menu extends \Collect\Plugin\Menu
{
    public function configureAdminMenu(MenuAdmin $menu)
    {
        if (Collect::hasUserSuperUserAccess()) {
            $menu->addDiagnosticItem('DBStats_DatabaseUsage',
                                     $this->urlForAction('index'),
                                     $order = 6);
        }
    }
}
