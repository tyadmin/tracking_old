<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\DBStats\Reports;

use Collect\Collect;
use Collect\Plugin\ViewDataTable;
use Collect\Plugins\CoreVisualizations\Visualizations\Graph;
use Collect\Plugin\ReportsProvider;

/**
 * Shows a datatable that displays the amount of space each blob archive table
 * takes up in the MySQL database, for each year of blob data.
 */
class GetReportDataSummaryByYear extends Base
{
    protected function init()
    {
        $this->name = Collect::translate('DBStats_ReportDataByYear');
    }

    public function configureView(ViewDataTable $view)
    {
        $this->addBaseDisplayProperties($view);
        $this->addPresentationFilters($view);

        $view->config->title = $this->name;
        $view->config->addTranslation('label', Collect::translate('Intl_PeriodYear'));
    }

    public function getRelatedReports()
    {
        return array(
            ReportsProvider::factory('DBStats', 'getReportDataSummary'),
        );
    }

}
