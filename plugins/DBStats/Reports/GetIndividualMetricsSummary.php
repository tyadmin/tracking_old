<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\DBStats\Reports;

use Collect\Collect;
use Collect\Plugin\ViewDataTable;
use Collect\Plugins\CoreVisualizations\Visualizations\Graph;
use Collect\Plugins\CoreVisualizations\Visualizations\HtmlTable;

/**
 * Shows a datatable that displays how many occurrences there are of each individual
 * metric type stored in the MySQL database.
 *
 * Goal metrics, metrics of the format .*_[0-9]+ and 'done...' metrics are grouped together.
 */
class GetIndividualMetricsSummary extends Base
{

    protected function init()
    {
        $this->name = Collect::translate('General_Metrics');
    }

    public function configureView(ViewDataTable $view)
    {
        $this->addBaseDisplayProperties($view);
        $this->addPresentationFilters($view, $addTotalSizeColumn = false, $addPercentColumn = false,
            $sizeColumns = array('estimated_size'));

        $view->requestConfig->filter_sort_order = 'asc';
        $view->config->addTranslation('label', Collect::translate('General_Metric'));

        $this->setIndividualSummaryFooterMessage($view);
    }

}
