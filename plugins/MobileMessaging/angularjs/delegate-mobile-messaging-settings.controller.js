/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
(function () {
    angular.module('collectApp').controller('DelegateMobileMessagingSettingsController', DelegateMobileMessagingSettingsController);

    DelegateMobileMessagingSettingsController.$inject = ['collectApi', 'collect'];

    function DelegateMobileMessagingSettingsController(collectApi, collect) {

        var self = this;
        this.isLoading = false;

        this.save = function () {
            this.isLoading = true;

            collectApi.post(
                {method: 'MobileMessaging.setDelegatedManagement'},
                {delegatedManagement: (this.enabled == '1') ? 'true' : 'false'}
            ).then(function () {

                var UI = require('collect/UI');
                var notification = new UI.Notification();
                notification.show(_pk_translate('CoreAdminHome_SettingsSaveSuccess'), {
                    id: 'mobileMessagingSettings', context: 'success'
                });
                notification.scrollToNotification();
                
                collect.helper.redirect();
                self.isLoading = false;
            }, function () {
                self.isLoading = false;
            });
        };
    }
})();