<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\MobileMessaging;

use Collect\Menu\MenuAdmin;
use Collect\Collect;

class Menu extends \Collect\Plugin\Menu
{
    public function configureAdminMenu(MenuAdmin $menu)
    {
        $title = 'MobileMessaging_SettingsMenu';
        $url = $this->urlForAction('index');
        $order = 35;

        if (Collect::hasUserSuperUserAccess()) {
            $menu->addSystemItem($title, $url, $order);
        } else if (!Collect::isUserIsAnonymous()) {
            $menu->addPersonalItem($title, $url, $order);
        }
    }
}
