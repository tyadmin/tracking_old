<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Contents\Categories;

use Collect\Category\Subcategory;
use Collect\Collect;

class ContentsSubcategory extends Subcategory
{
    protected $categoryId = 'General_Actions';
    protected $id = 'Contents_Contents';
    protected $order = 45;

    public function getHelp()
    {
        return '<p>' . Collect::translate('Contents_ContentsSubcategoryHelp1') . '</p>'
            . '<p><a href="https://matomo.org/docs/content-tracking/" rel="noreferrer noopener" target="_blank">' . Collect::translate('Contents_ContentsSubcategoryHelp2') . '</a></p>';
    }
}
