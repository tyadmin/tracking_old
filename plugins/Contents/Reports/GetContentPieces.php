<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Contents\Reports;

use Collect\Collect;
use Collect\Plugins\Contents\Columns\ContentPiece;
use Collect\Plugins\Contents\Columns\Metrics\InteractionRate;

/**
 * This class defines a new report.
 *
 */
class GetContentPieces extends Base
{
    protected function init()
    {
        parent::init();

        $this->name          = Collect::translate('Contents_ContentPiece');
        $this->documentation = Collect::translate('Contents_ContentPieceReportDocumentation');
        $this->dimension     = new ContentPiece();
        $this->order         = 36;
        $this->actionToLoadSubTables = 'getContentPieces';

        $this->metrics = array('nb_impressions', 'nb_interactions');
        $this->processedMetrics = array(new InteractionRate());
    }
}
