<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Contents\Reports;

use Collect\Collect;
use Collect\Plugins\Contents\Columns\ContentName;
use Collect\Plugins\Contents\Columns\Metrics\InteractionRate;

/**
 * This class defines a new report.
 *
 */
class GetContentNames extends Base
{
    protected function init()
    {
        parent::init();

        $this->name          = Collect::translate('Contents_ContentName');
        $this->documentation = Collect::translate('Contents_ContentNameReportDocumentation');
        $this->dimension     = new ContentName();
        $this->order         = 35;
        $this->actionToLoadSubTables = 'getContentNames';

        $this->metrics = array('nb_impressions', 'nb_interactions');
        $this->processedMetrics = array(new InteractionRate());
    }
}
