/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

/**
 * Usage:
 * <div collect-segment-generator>
 */
(function () {
    angular.module('collectApp').directive('collectSegmentGenerator', collectSegmentGenerator);

    collectSegmentGenerator.$inject = ['$document', 'collect', '$filter', '$timeout'];

    function collectSegmentGenerator($document, collect, $filter, $timeout){
        var defaults = {
            segmentDefinition: '',
            addInitialCondition: false,
            visitSegmentsOnly: false,
            idsite: collect.idSite
        };

        return {
            restrict: 'A',
            scope: {
                segmentDefinition: '@',
                addInitialCondition: '=',
                visitSegmentsOnly: '=',
                idsite: '='
            },
            require: "?ngModel",
            templateUrl: 'plugins/SegmentEditor/angularjs/segment-generator/segmentgenerator.directive.html?cb=' + collect.cacheBuster,
            controller: 'SegmentGeneratorController',
            controllerAs: 'segmentGenerator',
            compile: function (element, attrs) {

                for (var index in defaults) {
                    if (attrs[index] === undefined) {
                        attrs[index] = defaults[index];
                    }
                }

                return function (scope, element, attrs, ngModel) {
                    if (ngModel) {
                        ngModel.$render = function() {
                            scope.segmentDefinition = ngModel.$viewValue;
                            if (scope.segmentDefinition) {
                                scope.segmentGenerator.setSegmentString(scope.segmentDefinition);
                            } else {
                                scope.segmentGenerator.setSegmentString('');
                            }
                        };
                    }

                    scope.$watch('segmentDefinition', function (newValue) {
                        if (ngModel) {
                            ngModel.$setViewValue(newValue);
                        }
                    });
                };
            }
        };
    }
})();