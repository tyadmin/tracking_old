<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
namespace Collect\Plugins\Goals\Columns\Metrics\GoalSpecific;

use Collect\DataTable\Row;
use Collect\Metrics;
use Collect\Collect;
use Collect\Plugins\Goals\Columns\Metrics\GoalSpecificProcessedMetric;
use Collect\Plugins\Goals\Goals;

/**
 * The number of ecommerce order items for conversions of a goal. Returns the 'items'
 * goal specific metric.
 */
class ItemsCount extends GoalSpecificProcessedMetric
{
    public function getName()
    {
        return Goals::makeGoalColumn($this->idGoal, 'items', false);
    }

    public function getTranslatedName()
    {
        return Collect::translate('General_PurchasedProducts');
    }

    public function getDocumentation()
    {
        return Collect::translate('Goals_ColumnPurchasedProductsDocumentation', $this->getGoalNameForDocs());
    }

    public function getDependentMetrics()
    {
        return array('goals');
    }

    public function compute(Row $row)
    {
        $mappingFromNameToIdGoal = Metrics::getMappingFromNameToIdGoal();

        $goalMetrics = $this->getGoalMetrics($row);
        return (int) $this->getMetric($goalMetrics, 'items', $mappingFromNameToIdGoal);
    }
}