<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
namespace Collect\Plugins\Goals\Columns\Metrics\GoalSpecific;

use Collect\Archive\DataTableFactory;
use Collect\DataTable;
use Collect\DataTable\Row;
use Collect\Metrics;
use Collect\Metrics\Formatter;
use Collect\Collect;
use Collect\Plugins\Goals\Columns\Metrics\GoalSpecificProcessedMetric;
use Collect\Plugins\Goals\Goals;

/**
 * Revenue for a specific goal.
 */
class Revenue extends GoalSpecificProcessedMetric
{
    public function getName()
    {
        return Goals::makeGoalColumn($this->idGoal, 'revenue', false);
    }

    public function getTranslatedName()
    {
        return Collect::translate('%s ' . Collect::translate('General_ColumnRevenue'), $this->getGoalName());
    }

    public function getDocumentation()
    {
        return Collect::translate('Goals_ColumnRevenueDocumentation', $this->getGoalNameForDocs());
    }

    public function getDependentMetrics()
    {
        return array('goals');
    }

    public function compute(Row $row)
    {
        $mappingFromNameToIdGoal = Metrics::getMappingFromNameToIdGoal();

        $goalMetrics = $this->getGoalMetrics($row);
        return (float) $this->getMetric($goalMetrics, 'revenue', $mappingFromNameToIdGoal);
    }

    public function format($value, Formatter $formatter)
    {
        return $formatter->getPrettyMoney($value, $this->idSite);
    }

    public function beforeFormat($report, DataTable $table)
    {
        $this->idSite = DataTableFactory::getSiteIdFromMetadata($table);
        return !empty($this->idSite); // skip formatting if there is no site to get currency info from
    }
}