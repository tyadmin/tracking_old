<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\Goals\Columns\Metrics;

use Collect\Archive\DataTableFactory;
use Collect\DataTable;
use Collect\DataTable\Row;
use Collect\Metrics\Formatter;
use Collect\Collect;
use Collect\Plugin\ProcessedMetric;
use Collect\Tracker\GoalManager;

/**
 * The average price for each ecommerce order or abandoned cart. Calculated as:
 *
 *     price / (orders or abandoned_carts)
 *
 * price, orders and abandoned_carts are calculated by the Goals archiver.
 */
class AveragePrice extends ProcessedMetric
{
    private $idSite;

    public function getName()
    {
        return 'avg_price';
    }

    public function getTranslatedName()
    {
        return Collect::translate('General_AveragePrice');
    }

    public function compute(Row $row)
    {
        $price = $this->getMetric($row, 'price');
        $orders = $this->getMetric($row, 'orders');
        $abandonedCarts = $this->getMetric($row, 'abandoned_carts');

        return Collect::getQuotientSafe($price, $orders === false ? $abandonedCarts : $orders, GoalManager::REVENUE_PRECISION);
    }

    public function getDependentMetrics()
    {
        return array('price', 'orders', 'abandoned_carts');
    }

    public function format($value, Formatter $formatter)
    {
        return $formatter->getPrettyMoney($value, $this->idSite);
    }

    public function beforeFormat($report, DataTable $table)
    {
        $this->idSite = DataTableFactory::getSiteIdFromMetadata($table);
        return !empty($this->idSite); // skip formatting if there is no site to get currency info from
    }
}