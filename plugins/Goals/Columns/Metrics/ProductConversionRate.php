<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\Goals\Columns\Metrics;

use Collect\DataTable\Row;
use Collect\Metrics\Formatter;
use Collect\Collect;
use Collect\Plugin\ProcessedMetric;
use Collect\Tracker\GoalManager;

/**
 * The conversion rate for ecommerce orders. Calculated as:
 *
 *     (orders or abandoned_carts) / nb_visits
 *
 * orders and abandoned_carts are calculated by the Goals archiver.
 */
class ProductConversionRate extends ProcessedMetric
{
    public function getName()
    {
        return 'conversion_rate';
    }

    public function getTranslatedName()
    {
        return Collect::translate('General_ProductConversionRate');
    }

    public function format($value, Formatter $formatter)
    {
        return $formatter->getPrettyPercentFromQuotient($value);
    }

    public function compute(Row $row)
    {
        $orders = $this->getMetric($row, 'orders');
        $abandonedCarts = $this->getMetric($row, 'abandoned_carts');
        $visits = $this->getMetric($row, 'nb_visits');

        return Collect::getQuotientSafe($orders === false ? $abandonedCarts : $orders, $visits, GoalManager::REVENUE_PRECISION + 2);
    }

    public function getDependentMetrics()
    {
        return array('orders', 'abandoned_carts', 'nb_visits');
    }
}