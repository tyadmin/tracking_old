<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Goals\Columns;

use Collect\Columns\Dimension;
use Collect\Collect;

class DaysToConversion extends Dimension
{
    protected $type = self::TYPE_NUMBER;
    protected $nameSingular = 'Goals_DaysToConv';
}