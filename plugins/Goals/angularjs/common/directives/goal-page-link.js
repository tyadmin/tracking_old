/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

/**
 *
 * Usage:
 * <div collect-goal-page-link="idGoal">
 */
(function () {
    angular.module('collectApp.directive').directive('collectGoalPageLink', collectGoalPageLink);

    collectGoalPageLink.$inject = ['$location', 'collect'];

    function collectGoalPageLink($location, collect){

        return {
            restrict: 'A',
            compile: function (element, attrs) {

                if (attrs.collectGoalPageLink && collect.helper.isAngularRenderingThePage()) {
                    var title = element.text();
                    element.html('<a></a>');
                    var link =  element.find('a');
                    link.text(title);
                    link.attr('href', 'javascript:void(0)');
                    link.attr('title', _pk_translate('Goals_ClickToViewThisGoal'));
                    link.bind('click', function () {
                        var $search = $location.search();
                        $search.category = 'Goals_Goals';
                        $search.subcategory = encodeURIComponent(attrs.collectGoalPageLink);
                        $location.search($search);
                    });
                }

                return function (scope, element, attrs) {

                };
            }
        };
    }
})();