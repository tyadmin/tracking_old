/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */


/**
 * Usage:
 * <div collect-manage-goals>
 */
(function () {
    angular.module('collectApp').directive('collectManageGoals', collectManageGoals);

    collectManageGoals.$inject = ['collect'];

    function collectManageGoals(collect){

        return {
            restrict: 'A',
            priority: 10,
            controller: 'ManageGoalsController',
            controllerAs: 'manageGoals',
            compile: function (element, attrs) {

                return function (scope, element, attrs, controller) {
                    if (attrs.showAddGoal) {
                        controller.createGoal();
                    } else if (attrs.showGoal) {
                        controller.editGoal(attrs.showGoal);
                    }

                };
            }
        };
    }
})();