<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Plugins\Goals;


use Collect\Collect;

class TranslationHelper
{

    public function translateGoalMetricCategory($category)
    {
        // Return either "Goals by %s" or "Goals %s", depending on the category
        if ($category === 'General_Visit') {
                return Collect::translate('Goals_GoalsAdjective', Collect::translate('Goals_CategoryText' . $category));
        }
        return Collect::translate('Goals_GoalsBy', Collect::translate('Goals_CategoryText' . $category));
    }

    public function translateEcommerceMetricCategory($category)
    {
        // Return either "Sales by %s" or "Sales %s", depending on the category
        if ($category === 'General_Visit') {
                return Collect::translate('Ecommerce_SalesAdjective', Collect::translate('Goals_CategoryText' . $category));
        }
        return Collect::translate('Ecommerce_SalesBy', Collect::translate('Goals_CategoryText' . $category));
    }

    public function getTranslationForCompleteDescription($match, $patternType, $pattern)
    {
        $description = $this->getTranslationForMatchAttribute($match);
        if($this->isPatternUsedForMatchAttribute($match)) {
            $description = sprintf(
                '%s %s',
                $description,
                $this->getTranslationForPattern(
                    $patternType,
                    $pattern
                )
            );
        }

        return $description;
    }

    protected function isPatternUsedForMatchAttribute($match)
    {
        return in_array(
            $match,
            array('url', 'title', 'event_category', 'event_action', 'event_name', 'file', 'external_website')
        );
    }

    protected function getTranslationForMatchAttribute($match)
    {
        switch ($match) {
            case 'manually':
                return Collect::translate('Goals_ManuallyTriggeredUsingJavascriptFunction');

            case 'url':
                return Collect::translate('Goals_VisitUrl');

            case 'title':
                return Collect::translate('Goals_VisitPageTitle');

            case 'event_category':
            case 'event_action':
            case 'event_name':
                return Collect::translate('Goals_SendEvent');

            case 'file':
                return Collect::translate('Goals_Download');

            case 'external_website':
                return Collect::translate('Goals_ClickOutlink');

            default:
                return '';
        }
    }

    protected function getTranslationForPattern($patternType, $pattern)
    {
        switch ($patternType) {
            case 'regex':
                return sprintf('%s %s',
                    Collect::translate('Goals_Pattern'),
                    Collect::translate('Goals_MatchesExpression', array($pattern))
                );

            case 'contains':
                return sprintf('%s %s',
                    Collect::translate('Goals_Pattern'),
                    Collect::translate('Goals_Contains', array($pattern))
                );

            case 'exact':
                return sprintf('%s %s',
                    Collect::translate('Goals_Pattern'),
                    Collect::translate('Goals_IsExactly', array($pattern))
                );

            default:
                return '';
        }
    }
}