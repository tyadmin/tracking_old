<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Goals\Categories;

use Collect\Category\Subcategory;
use Collect\Collect;

class ManageGoalsSubcategory extends Subcategory
{
    protected $categoryId = 'Goals_Goals';
    protected $id = 'Goals_ManageGoals';
    protected $order = 9999;

    public function getHelp()
    {
        return '<p>' . Collect::translate('Goals_ManageGoalsSubcategoryHelp1') . '</p>'
            . '<p><a href="https://matomo.org/docs/tracking-goals-web-analytics/" rel="noreferrer noopener" target="_blank">' . Collect::translate('Goals_ManageGoalsSubcategoryHelp2') . '</a></p>';
    }
}
