<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Goals\Reports;

use Collect\API\Request;
use Collect\Common;
use Collect\DataTable;
use Collect\Metrics\Formatter;
use Collect\NumberFormatter;
use Collect\Collect;
use Collect\Plugin;
use Collect\Plugin\ViewDataTable;
use Collect\Plugins\CoreVisualizations\Visualizations\JqplotGraph\Evolution;
use Collect\Plugins\CoreVisualizations\Visualizations\Sparklines;
use Collect\Plugins\Goals\API;
use Collect\Plugins\Goals\Goals;
use Collect\Plugins\Goals\Pages;
use Collect\Report\ReportWidgetFactory;
use Collect\Site;
use Collect\Tracker\GoalManager;
use Collect\Url;
use Collect\Widget\WidgetsList;

class Get extends Base
{
    protected function init()
    {
        parent::init();

        $this->name = Collect::translate('Goals_Goals');
        $this->processedMetrics = array('conversion_rate');
        $this->documentation = Collect::translate('Goals_OverviewReportDocumentation');
        $this->order = 1;
        $this->orderGoal = 50;
        $this->metrics = array('nb_conversions', 'nb_visits_converted', 'revenue');
        $this->parameters = null;
    }

    private function getGoals()
    {
        $idSite = $this->getIdSite();
        $goals = Request::processRequest('Goals.getGoals', ['idSite' => $idSite, 'filter_limit' => '-1'], $default = []);
        return $goals;
    }

    private function getGoal($goalId)
    {
        $goals = $this->getGoals();

        if (!empty($goals[$goalId])) {

            return $goals[$goalId];
        }
    }

    public function configureWidgets(WidgetsList $widgetsList, ReportWidgetFactory $factory)
    {
        $idSite  = Common::getRequestVar('idSite', 0, 'int');

        if (empty($idSite)) {
            return;
        }

        $goals   = $this->getGoals();
        $reports = Goals::getReportsWithGoalMetrics();

        $page = new Pages($factory, $reports);

        $widgetsList->addWidgetConfigs($page->createGoalsOverviewPage($goals));

        if ($this->isEcommerceEnabled($idSite)) {
            $widgetsList->addWidgetConfigs($page->createEcommerceOverviewPage());
            $widgetsList->addWidgetConfigs($page->createEcommerceSalesPage());
        }

        foreach ($goals as $goal) {
            $widgetsList->addWidgetConfigs($page->createGoalDetailPage($goal));
        }
    }

    private function getIdSite()
    {
        return Common::getRequestVar('idSite', null, 'int');
    }

    private function isEcommerceEnabled($idSite)
    {
        if (!Plugin\Manager::getInstance()->isPluginActivated('Ecommerce')) {
            return false;
        }

        $site = new Site($idSite);
        return $site->isEcommerceEnabled();
    }

    public function configureView(ViewDataTable $view)
    {
        $idGoal = Common::getRequestVar('idGoal', 0, 'string');

        $idSite = $this->getIdSite();

        if ($view->isViewDataTableId(Sparklines::ID)) {
            /** @var Sparklines $view */
            $isEcommerceEnabled = $this->isEcommerceEnabled($idSite);

            $onlySummary = Common::getRequestVar('only_summary', 0, 'int');

            if ($onlySummary && !empty($idGoal)) {
                if (is_numeric($idGoal)) {
                    $view->config->title_attributes = array('collect-goal-page-link' => $idGoal);
                }

                // in Goals overview summary we show proper title for a goal
                $goal = $this->getGoal($idGoal);
                if (!empty($goal['name'])) {
                    $view->config->title = Collect::translate('Goals_GoalX', "'" . $goal['name'] . "'");
                }
            } else {
                $view->config->title = '';
            }

            $numberFormatter = NumberFormatter::getInstance();
            $view->config->filters[] = function (DataTable $table) use ($numberFormatter, $idSite) {
                $firstRow = $table->getFirstRow();
                if ($firstRow) {
                    $revenue = $firstRow->getColumn('revenue');
                    $currencySymbol = Site::getCurrencySymbolFor($idSite);
                    $revenue = $numberFormatter->formatCurrency($revenue, $currencySymbol, GoalManager::REVENUE_PRECISION);
                    $firstRow->setColumn('revenue', $revenue);
                }
            };

            $view->config->addTranslations(array(
                'nb_visits' => Collect::translate('VisitsSummary_NbVisitsDescription'),
                'nb_conversions' => Collect::translate('Goals_ConversionsDescription'),
                'nb_visits_converted' => Collect::translate('General_NVisits'),
                'conversion_rate' => Collect::translate('Goals_OverallConversionRate'),
                'revenue' => Collect::translate('Goals_OverallRevenue'),
            ));

            $allowMultiple = Common::getRequestVar('allow_multiple', 0, 'int');

            if ($allowMultiple) {
                $view->config->addSparklineMetric(array('nb_conversions', 'nb_visits_converted'), $order = 10);
            } else {
                $view->config->addSparklineMetric(array('nb_conversions'), $order = 10);
            }

            $view->config->addSparklineMetric(array('conversion_rate'), $order = 20);

            if (empty($idGoal)) {
                // goals overview sparklines below evolution graph

                if ($isEcommerceEnabled) {
                    // this would be ideally done in Ecommerce plugin but then it is hard to keep same order
                    $view->config->addSparklineMetric(array('revenue'), $order = 30);
                }

            } else {
                if ($onlySummary) {
                    // in Goals Overview we list an overview for each goal....
                    $view->config->addTranslation('conversion_rate', Collect::translate('Goals_ConversionRate'));

                } elseif ($isEcommerceEnabled) {
                    // in Goals detail page...
                    $view->config->addSparklineMetric(array('revenue'), $order = 30);
                }
            }
        } else if ($view->isViewDataTableId(Evolution::ID)) {
            if (!empty($idSite) && Collect::isUserHasWriteAccess($idSite)) {
                $view->config->title_edit_entity_url = 'index.php' . Url::getCurrentQueryStringWithParametersModified(array(
                    'module' => 'Goals',
                    'action' => 'manage',
                    'forceView' => null,
                    'viewDataTable' => null,
                    'showtitle' => null,
                    'random' => null
                ));
            }

            $goal = $this->getGoal($idGoal);
            if (!empty($goal['name'])) {
                $view->config->title = Collect::translate('Goals_GoalX', "'" . Common::unsanitizeInputValue($goal['name']) . "'");
                if (!empty($goal['description'])) {
                    $view->config->description = Common::unsanitizeInputValue($goal['description']);
                }
            } else {
                $view->config->title = Collect::translate('General_EvolutionOverPeriod');
            }

            if (empty($view->config->columns_to_display)) {
                $view->config->columns_to_display = array('nb_conversions');
            }
        }
    }

    public function configureReportMetadata(&$availableReports, $infos)
    {
        if (!$this->isEnabled()) {
            return;
        }

        parent::configureReportMetadata($availableReports, $infos);

        $this->addReportMetadataForEachGoal($availableReports, $infos, function ($goal) {
            return Collect::translate('Goals_GoalX', $goal['name']);
        }, $isSummary = true);
    }
}
