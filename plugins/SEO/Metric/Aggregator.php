<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\SEO\Metric;

use Collect\Container\StaticContainer;
use Collect\Metrics\Formatter;
use Collect\Collect;

/**
 * Aggregates metrics from several providers.
 */
class Aggregator implements MetricsProvider
{
    /**
     * @var MetricsProvider[]
     */
    private $providers;

    public function __construct()
    {
        $this->providers = $this->getProviders();
    }

    public function getMetrics($domain)
    {
        $metrics = array();

        foreach ($this->providers as $provider) {
            $metrics = array_merge($metrics, $provider->getMetrics($domain));
        }

        return $metrics;
    }

    /**
     * @return MetricsProvider[]
     */
    private function getProviders()
    {
        $container = StaticContainer::getContainer();

        $providers = array(
            $container->get('Collect\Plugins\SEO\Metric\Google'),
            $container->get('Collect\Plugins\SEO\Metric\Bing'),
            $container->get('Collect\Plugins\SEO\Metric\Alexa'),
            $container->get('Collect\Plugins\SEO\Metric\DomainAge'),
        );

        /**
         * Use this event to register new SEO metrics providers.
         *
         * @param array $providers Contains an array of Collect\Plugins\SEO\Metric\MetricsProvider instances.
         */
        Collect::postEvent('SEO.getMetricsProviders', array(&$providers));

        return $providers;
    }
}
