<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\SEO\Widgets;

use Collect\Common;
use Collect\DataTable\Renderer;
use Collect\Widget\WidgetConfig;
use Collect\Site;
use Collect\Url;
use Collect\UrlHelper;
use Collect\Plugins\SEO\API;

class GetRank extends \Collect\Widget\Widget
{
    public static function getCategory()
    {
        return 'SEO';
    }

    public static function getName()
    {
        return 'SEO_SeoRankings';
    }

    public static function configure(WidgetConfig $config)
    {
        $config->setCategoryId(self::getCategory());
        $config->setName(self::getName());
    }

    public function render()
    {
        $idSite = Common::getRequestVar('idSite');
        $site = new Site($idSite);

        $url = urldecode(Common::getRequestVar('url', '', 'string'));

        if (!empty($url) && strpos($url, 'http://') !== 0 && strpos($url, 'https://') !== 0) {
            $url = 'http://' . $url;
        }

        if (empty($url) || !UrlHelper::isLookLikeUrl($url)) {
            $url = $site->getMainUrl();
        }

        $dataTable = API::getInstance()->getRank($url);

        /** @var \Collect\DataTable\Renderer\Json $renderer */
        $renderer = Renderer::factory('json');
        $renderer->setTable($dataTable);

        return $this->renderTemplate('getRank', array(
            'urlToRank' => Url::getHostFromUrl($url),
            'ranks' => json_decode($renderer->render(), true)
        ));
    }

}
