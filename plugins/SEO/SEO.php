<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\SEO;
use Collect\Plugins\SEO\Widgets\GetRank;
use Collect\SettingsCollect;
use Collect\Widget\WidgetsList;

/**
 */
class SEO extends \Collect\Plugin
{
    public function registerEvents()
    {
        return [
            'Widget.filterWidgets' => 'filterWidgets',
            'AssetManager.getJavaScriptFiles' => 'getJsFiles',
        ];
    }

    public function getJsFiles(&$jsFiles)
    {
        $jsFiles[] = "plugins/SEO/javascripts/rank.js";
    }

    /**
     * @param WidgetsList $list
     */
    public function filterWidgets($list)
    {
        if (!SettingsCollect::isInternetEnabled()) {
            $list->remove(GetRank::getCategory(), GetRank::getName());
        }
    }
}
