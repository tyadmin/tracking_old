<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Resolution\Reports;

use Collect\Collect;
use Collect\Plugin\ViewDataTable;
use Collect\Plugins\Resolution\Columns\Configuration;
use Collect\Plugin\ReportsProvider;

class GetConfiguration extends Base
{
    protected function init()
    {
        parent::init();
        $this->dimension     = new Configuration();
        $this->name          = Collect::translate('Resolution_Configurations');
        $this->documentation = Collect::translate('Resolution_WidgetGlobalVisitorsDocumentation', '<br />');
        $this->order = 7;

        $this->subcategoryId = 'DevicesDetection_Software';
    }

    public function configureView(ViewDataTable $view)
    {
        $this->getBasicResolutionDisplayProperties($view);

        $view->config->addTranslation('label', $this->dimension->getName());

        $view->requestConfig->filter_limit = 3;
    }

    public function getRelatedReports()
    {
        return array(
            ReportsProvider::factory('Resolution', 'getResolution'),
        );
    }
}
