<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Plugins\Resolution;

use Collect\Collect;

function getConfigurationLabel($str)
{
    if (strpos($str, ';') === false) {
        return $str;
    }
    $values = explode(";", $str);

    $os = \Collect\Plugins\DevicesDetection\getOsFullName($values[0]);
    $name = $values[1];
    $browser = \Collect\Plugins\DevicesDetection\getBrowserName($name);
    if ($browser === false) {
        $browser = Collect::translate('General_Unknown');
    }
    $resolution = $values[2];
    return $os . " / " . $browser . " / " . $resolution;
}
