<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Events\Categories;

use Collect\Category\Subcategory;
use Collect\Collect;

class EventsSubcategory extends Subcategory
{
    protected $categoryId = 'General_Actions';
    protected $id = 'Events_Events';
    protected $order = 40;

    public function getHelp()
    {
        return '<p>' . Collect::translate('Events_EventsSubcategoryHelp1') . '</p>'
            . '<p><a href="https://matomo.org/docs/event-tracking/" rel="noreferrer noopener" target="_blank">' . Collect::translate('Events_EventsSubcategoryHelp2') . '</a></p>';
    }
}
