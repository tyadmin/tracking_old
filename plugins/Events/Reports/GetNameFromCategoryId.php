<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Events\Reports;

use Collect\Collect;
use Collect\Plugins\Events\Columns\EventName;

/**
 * Report metadata class for the Events.getNameFromCategoryId class.
 */
class GetNameFromCategoryId extends Base
{
    protected function init()
    {
        $this->categoryId = 'Events_Events';
        $this->processedMetrics = false;

        $this->dimension     = new EventName();
        $this->name          = Collect::translate('Events_EventNames');
        $this->isSubtableReport = true;
    }
}