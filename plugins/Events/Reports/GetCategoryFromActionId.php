<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Events\Reports;

use Collect\Collect;
use Collect\Plugins\Events\Columns\EventCategory;

/**
 * Report metadata class for the Events.getCategoryFromActionId class.
 */
class GetCategoryFromActionId extends Base
{
    protected function init()
    {
        parent::init();

        $this->dimension     = new EventCategory();
        $this->name          = Collect::translate('Events_EventCategories');
        $this->isSubtableReport = true;
    }
}