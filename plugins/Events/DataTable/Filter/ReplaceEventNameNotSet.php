<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Events\DataTable\Filter;

use Collect\DataTable\BaseFilter;
use Collect\DataTable\Row;
use Collect\DataTable;
use Collect\Collect;
use Collect\Plugins\Events\Archiver;

class ReplaceEventNameNotSet extends BaseFilter
{
    /**
     * Constructor.
     *
     * @param DataTable $table The table to eventually filter.
     */
    public function __construct($table)
    {
        parent::__construct($table);
    }

    /**
     * @param DataTable $table
     */
    public function filter($table)
    {
        $row = $table->getRowFromLabel(Archiver::EVENT_NAME_NOT_SET);
        if ($row) {
            $row->setColumn('label', Collect::translate('General_NotDefined', Collect::translate('Events_EventName')));
        }
    }
}