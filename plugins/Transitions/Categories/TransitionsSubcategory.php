<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Transitions\Categories;

use Collect\Category\Subcategory;
use Collect\Collect;

class TransitionsSubcategory extends Subcategory
{
    protected $categoryId = 'General_Actions';
    protected $id = 'Transitions_Transitions';
    protected $order = 46;

    public function getHelp()
    {
        return '<p>' . Collect::translate('Transitions_TransitionsSubcategoryHelp1') . '</p>'
            . '<p><a href="https://matomo.org/docs/transitions/" rel="noreferrer noopener" target="_blank">' . Collect::translate('Transitions_TransitionsSubcategoryHelp2') . '</a></p>';
    }
}
