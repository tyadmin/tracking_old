<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Transitions\Widgets;

use Collect\Common;
use Collect\Collect;
use Collect\Plugins\Transitions\Controller;
use Collect\Widget\Widget;
use Collect\Widget\WidgetConfig;

class GetTransitions extends Widget
{
    public static function configure(WidgetConfig $config)
    {
        $config->setCategoryId('General_Actions');
        $config->setSubcategoryId('Transitions_Transitions');
        $config->setName('Transitions_Transitions');
        $config->setOrder(99);
        $idSite = self::getIdSite();
        if (!$idSite || !Collect::isUserHasViewAccess($idSite)) {
            $config->disable();
        }
    }

    private static function getIdSite()
    {
        return Common::getRequestVar('idSite', 0, 'int');
    }

    public function render()
    {
        Collect::checkUserHasViewAccess(self::getIdSite());

        $isWidgetized = Common::getRequestVar('widget', 0, 'int') === 1;

        return $this->renderTemplate('transitions', array(
            'isWidget' => $isWidgetized
        ));
    }

}