<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\DevicePlugins\Columns;

use Collect\Common;
use Collect\Collect;
use Collect\Tracker\Request;
use Collect\Tracker\Visitor;
use Collect\Tracker\Action;

class PluginSilverlight extends DevicePluginColumn
{
    protected $columnName = 'config_silverlight';
    protected $columnType = 'TINYINT(1) NULL';
    protected $type = self::TYPE_BOOL;

    public function getName()
    {
        return Collect::translate('General_Plugin') . ' (Silverlight)';
    }

    /**
     * @param Request $request
     * @param Visitor $visitor
     * @param Action|null $action
     * @return mixed
     */
    public function onNewVisit(Request $request, Visitor $visitor, $action)
    {
        return Common::getRequestVar('ag', 0, 'int', $request->getParams());
    }
}