<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\DevicePlugins\Columns;

use Collect\Columns\Dimension;
use Collect\Collect;

class Plugin extends Dimension
{
    protected $nameSingular = 'General_Plugin';
}