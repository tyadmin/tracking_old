<?php

return array(
    'diagnostics.optional' => DI\add(array(
        DI\get('Collect\Plugins\UserCountry\Diagnostic\GeolocationDiagnostic'),
    )),
);
