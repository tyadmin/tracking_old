<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\UserCountry;

use Collect\Menu\MenuAdmin;
use Collect\Collect;

class Menu extends \Collect\Plugin\Menu
{
    public function configureAdminMenu(MenuAdmin $menu)
    {
        if (UserCountry::isGeoLocationAdminEnabled() && Collect::hasUserSuperUserAccess()) {
            $menu->addSystemItem('UserCountry_Geolocation',
                                 $this->urlForAction('adminIndex'),
                                 $order = 30);
        }
    }
}
