<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\UserCountry\Categories;

use Collect\Category\Subcategory;
use Collect\Collect;

class LocationsSubcategory extends Subcategory
{
    protected $categoryId = 'General_Visitors';
    protected $id = 'UserCountry_SubmenuLocations';
    protected $order = 10;

    public function getHelp()
    {
        return '<p>' . Collect::translate('UserCountry_LocationsSubcategoryHelp') . '</p>';
    }
}
