<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\UserCountry\Columns;

use Collect\Columns\DimensionSegmentFactory;
use Collect\Common;
use Collect\Config;
use Collect\Container\StaticContainer;
use Collect\Intl\Data\Provider\RegionDataProvider;
use Collect\Metrics\Formatter;
use Matomo\Network\IP;
use Collect\Collect;
use Collect\Plugin\Manager;
use Collect\Plugin\Segment;
use Collect\Plugins\Provider\Provider as ProviderProvider;
use Collect\Plugins\UserCountry\LocationProvider;
use Collect\Segment\SegmentsList;
use Collect\Tracker\Visit;
use Collect\Tracker\Visitor;
use Collect\Tracker\Action;
use Collect\Tracker\Request;

require_once TJWXJC_INCLUDE_PATH . '/plugins/UserCountry/functions.php';

class Country extends Base
{
    protected $columnName = 'location_country';
    protected $columnType = 'CHAR(3) NULL';
    protected $type = self::TYPE_TEXT;

    protected $category =  'UserCountry_VisitLocation';
    protected $nameSingular = 'UserCountry_Country';
    protected $namePlural = 'UserCountryMap_Countries';
    protected $segmentName = 'countryCode';
    protected $acceptValues = 'ISO 3166-1 alpha-2 country codes (de, us, fr, in, es, etc.)';

    public function configureSegments(SegmentsList $segmentsList, DimensionSegmentFactory $dimensionSegmentFactory)
    {
        $segment = new Segment();
        $segment->setName('UserCountry_CountryCode');
        $segmentsList->addSegment($dimensionSegmentFactory->createSegment($segment));

        $segment = new Segment();
        $segment->setSegment('countryName');
        $segment->setName('UserCountry_Country');
        $segment->setAcceptedValues('Germany, France, Spain, ...');
        $segment->setNeedsMostFrequentValues(false);
        $regionDataProvider = StaticContainer::get('Collect\Intl\Data\Provider\RegionDataProvider');
        $countryList = $regionDataProvider->getCountryList();
        array_walk($countryList, function(&$item, $key) {
            $item = Collect::translate('Intl_Country_'.strtoupper($key), [], 'en');
        });

        $segment->setSqlFilterValue(function ($val) use ($countryList) {
            $result   = array_search($val, $countryList);
            if ($result === false) {
                $result = 'UNK';
            }
            return $result;
        });
        $segment->setSuggestedValuesCallback(function ($idSite, $maxValuesToReturn, $table) use ($countryList) {
            return $this->sortStaticListByUsage($countryList, $table, 'countryCode', $maxValuesToReturn);
        });
        $segmentsList->addSegment($dimensionSegmentFactory->createSegment($segment));
    }


    public function formatValue($value, $idSite, Formatter $formatter)
    {
        return \Collect\Plugins\UserCountry\countryTranslate($value);
    }

    /**
     * @param Request $request
     * @param Visitor $visitor
     * @param Action|null $action
     * @return mixed
     */
    public function onNewVisit(Request $request, Visitor $visitor, $action)
    {
        $value = $this->getUrlOverrideValueIfAllowed('country', $request);
        if ($value !== false) {
            $value = substr($value, 0, 3);
            return $value;
        }

        $userInfo = $this->getUserInfo($request, $visitor);
        $country  = $this->getLocationDetail($userInfo, LocationProvider::COUNTRY_CODE_KEY);

        if (!empty($country) && $country != Visit::UNKNOWN_CODE) {
            return strtolower($country);
        }

        $country = $this->getCountryUsingProviderExtensionIfValid($userInfo['ip']);

        if (!empty($country)) {
            return $country;
        }

        return Visit::UNKNOWN_CODE;
    }

    private function getCountryUsingProviderExtensionIfValid($ipAddress)
    {
        if (!Manager::getInstance()->isPluginInstalled('Provider')) {
            return false;
        }

        $hostname = $this->getHost($ipAddress);
        $hostnameExtension = ProviderProvider::getCleanHostname($hostname);

        $hostnameDomain = substr($hostnameExtension, 1 + strrpos($hostnameExtension, '.'));
        if ($hostnameDomain == 'uk') {
            $hostnameDomain = 'gb';
        }

        /** @var RegionDataProvider $regionDataProvider */
        $regionDataProvider = StaticContainer::get('Collect\Intl\Data\Provider\RegionDataProvider');

        if (array_key_exists($hostnameDomain, $regionDataProvider->getCountryList())) {
            return $hostnameDomain;
        }

        return false;
    }

    /**
     * Returns the hostname given the IP address string
     *
     * @param string $ipStr IP Address
     * @return string hostname (or human-readable IP address)
     */
    private function getHost($ipStr)
    {
        $ip = IP::fromStringIP($ipStr);

        $host = $ip->getHostname();
        $host = ($host === null ? $ipStr : $host);

        return trim(strtolower($host));
    }

    /**
     * @param Request $request
     * @param Visitor $visitor
     * @param Action|null $action
     * @return int
     */
    public function onExistingVisit(Request $request, Visitor $visitor, $action)
    {
        return $this->getUrlOverrideValueIfAllowed('country', $request);
    }

    /**
     * @param Request $request
     * @param Visitor $visitor
     * @param Action|null $action
     * @return mixed
     */
    public function onAnyGoalConversion(Request $request, Visitor $visitor, $action)
    {
        $country = $visitor->getVisitorColumn($this->columnName);

        if (isset($country) && false !== $country) {
            return $country;
        }

        $browserLanguage = $request->getBrowserLanguage();
        $enableLanguageToCountryGuess = Config::getInstance()->Tracker['enable_language_to_country_guess'];
        $locationIp = $visitor->getVisitorColumn('location_ip');

        $country = Common::getCountry($browserLanguage, $enableLanguageToCountryGuess, $locationIp);

        return $country;
    }
}