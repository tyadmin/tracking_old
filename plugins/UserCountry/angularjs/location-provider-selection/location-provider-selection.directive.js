/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

/**
 * Usage:
 * <div collect-location-provider-selection>
 */
(function () {
    angular.module('collectApp').directive('collectLocationProviderSelection', collectLocationProviderSelection);

    collectLocationProviderSelection.$inject = ['collect'];

    function collectLocationProviderSelection(collect){

        return {
            restrict: 'A',
            transclude: true,
            controller: 'LocationProviderSelectionController',
            controllerAs: 'locationSelector',
            template: '<div ng-transclude></div>',
            compile: function (element, attrs) {

                return function (scope, element, attrs, controller) {
                    controller.selectedProvider = attrs.collectLocationProviderSelection;
                };
            }
        };
    }
})();