<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\UserCountry\Reports;

use Collect\Collect;
use Collect\Plugin\ViewDataTable;
use Collect\Plugins\UserCountry\Columns\Continent;
use Collect\Report\ReportWidgetFactory;
use Collect\Widget\WidgetsList;

class GetContinent extends Base
{
    protected function init()
    {
        parent::init();
        $this->dimension      = new Continent();
        $this->name           = Collect::translate('UserCountry_Continent');
        $this->documentation  = Collect::translate('UserCountry_getContinentDocumentation');
        $this->metrics        = array('nb_visits', 'nb_uniq_visitors', 'nb_actions');
        $this->hasGoalMetrics = true;
        $this->order = 6;

        $this->subcategoryId = 'UserCountry_SubmenuLocations';
    }

    public function configureWidgets(WidgetsList $widgetsList, ReportWidgetFactory $factory)
    {
        $widgetsList->addWidgetConfig($factory->createContainerWidget('Continent'));

        $widgetsList->addToContainerWidget('Continent', $factory->createWidget());

        $widget = $factory->createWidget()->setAction('getDistinctCountries')->setName('');
        $widgetsList->addToContainerWidget('Continent', $widget);
    }

    public function configureView(ViewDataTable $view)
    {
        $view->config->show_exclude_low_population = false;
        $view->config->show_search = false;
        $view->config->show_offset_information = false;
        $view->config->show_pagination_control = false;
        $view->config->show_limit_control = false;
        $view->config->documentation = $this->documentation;
        $view->config->addTranslation('label', $this->dimension->getName());
    }

}
