<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\UserCountry\Reports;

use Collect\Collect;
use Collect\Plugin\ViewDataTable;
use Collect\Plugins\UserCountry\Columns\Country;
use Collect\Plugins\UserCountry\LocationProvider;

class GetCountry extends Base
{
    protected function init()
    {
        parent::init();
        $this->dimension      = new Country();
        $this->name           = Collect::translate('UserCountry_Country');
        $this->documentation  = Collect::translate('UserCountry_getCountryDocumentation');
        $this->metrics        = array('nb_visits', 'nb_uniq_visitors', 'nb_actions');
        $this->hasGoalMetrics = true;
        $this->order = 5;
        $this->subcategoryId = 'UserCountry_SubmenuLocations';
    }

    public function configureView(ViewDataTable $view)
    {
        $view->config->show_exclude_low_population = false;
        $view->config->addTranslation('label', $this->dimension->getName());
        $view->config->documentation = $this->documentation;

        $view->requestConfig->filter_limit = 5;

        if (LocationProvider::getCurrentProviderId() == LocationProvider\DefaultProvider::ID) {
            // if we're using the default location provider, add a note explaining how it works
            $footerMessage = Collect::translate("General_Note") . ': '
                . Collect::translate('UserCountry_DefaultLocationProviderExplanation',
                    array('<a rel="noreferrer noopener" target="_blank" href="https://matomo.org/docs/geo-locate/">', '</a>'));

            $view->config->show_footer_message = $footerMessage;
        }
    }

}
