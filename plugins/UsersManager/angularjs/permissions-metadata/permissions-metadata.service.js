/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

/**
 * Usage:
 * <collect-capabilities-edit>
 */
(function () {
    angular.module('collectApp').factory('permissionsMetadataService', PermissionsMetadataService);

    PermissionsMetadataService.$inject = ['collectApi', '$q'];

    function PermissionsMetadataService(collectApi, $q) {
        var allCapabilities;

        return {
            getAllCapabilities: function () {
                if (allCapabilities) {
                    return $q.when(allCapabilities);
                }

                return collectApi.fetch({
                    method: 'UsersManager.getAvailableCapabilities',
                }).then(function (capabilities) {
                    allCapabilities = capabilities;
                    return allCapabilities;
                });
            },
        };
    }
})();
