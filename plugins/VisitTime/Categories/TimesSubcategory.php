<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\VisitTime\Categories;

use Collect\Category\Subcategory;
use Collect\Collect;

class TimesSubcategory extends Subcategory
{
    protected $categoryId = 'General_Visitors';
    protected $id = 'VisitTime_SubmenuTimes';
    protected $order = 35;

    public function getHelp()
    {
        return '<p>' . Collect::translate('VisitTime_TimesSubcategoryHelp') . '</p>';
    }
}
