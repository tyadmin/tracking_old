<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\VisitTime\Columns;

use Collect\Columns\Dimension;
use Collect\Collect;

class DayOfTheWeek extends Dimension
{
    protected $nameSingular = 'VisitTime_DayOfWeek';
}