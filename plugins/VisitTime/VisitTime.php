<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\VisitTime;

// empty plugin definition, otherwise plugin won't be installed during test run
class VisitTime extends \Collect\Plugin
{
}
