<?php

use Collect\Tests\Framework\Mock\ProfessionalServices\Advertising;
use Collect\Plugins\ProfessionalServices\tests\Framework\Mock\Promo;

return array(
    'Collect\ProfessionalServices\Advertising' => function () {
        return new Advertising();
    },
    'Collect\Plugins\ProfessionalServices\Promo' => function () {
        return new Promo();
    }
);
