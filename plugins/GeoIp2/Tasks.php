<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\GeoIp2;

use Collect\Plugins\UserCountry\UserCountry;
use Collect\SettingsCollect;

class Tasks extends \Collect\Plugin\Tasks
{
    public function schedule()
    {
        // add the auto updater task if GeoIP admin is enabled
        if (UserCountry::isGeoLocationAdminEnabled() && SettingsCollect::isInternetEnabled() === true) {
            $this->scheduleTask(new GeoIP2AutoUpdater());
        }
    }
}