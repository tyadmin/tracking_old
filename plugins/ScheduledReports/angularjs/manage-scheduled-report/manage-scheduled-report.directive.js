/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

var getReportParametersFunctions = Object();
var updateReportParametersFunctions = Object();
var resetReportParametersFunctions = Object();

/**
 * Usage:
 * <div collect-manage-scheduled-report>
 */
(function () {
    angular.module('collectApp').directive('collectManageScheduledReport', collectManageScheduledReport);

    collectManageScheduledReport.$inject = ['collect'];

    function collectManageScheduledReport(collect){

        return {
            restrict: 'A',
            priority: 10,
            controller: 'ManageScheduledReportController',
            controllerAs: 'manageScheduledReport'
        };
    }
})();