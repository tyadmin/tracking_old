<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Plugins\ScheduledReports\ReportEmailGenerator;

use Collect\Mail;
use Collect\Plugins\ScheduledReports\GeneratedReport;
use Collect\Plugins\ScheduledReports\ReportEmailGenerator;

class HtmlReportEmailGenerator extends ReportEmailGenerator
{
    protected function configureEmail(Mail $mail, GeneratedReport $report)
    {
        // Needed when using images as attachment with cid
        $mail->setBodyHtml($report->getContents());
    }
}