<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\ExamplePlugin;

use Collect\Menu\MenuAdmin;
use Collect\Menu\MenuTop;

/**
 * This class allows you to add, remove or rename menu items.
 * To configure a menu (such as Admin Menu, Top Menu, User Menu...) simply call the corresponding methods as
 */
class Menu extends \Collect\Plugin\Menu
{

    public function configureTopMenu(MenuTop $menu)
    {
        // $menu->addItem('ExamplePlugin_MyTopItem', null, $this->urlForDefaultAction(), $orderId = 30);
    }

    public function configureAdminMenu(MenuAdmin $menu)
    {
        // reuse an existing category. Execute the showList() method within the controller when menu item was clicked
        // $menu->addManageItem('ExamplePlugin_MyUserItem', $this->urlForAction('showList'), $orderId = 30);
        // $menu->addPlatformItem('ExamplePlugin_MyUserItem', $this->urlForDefaultAction(), $orderId = 30);

        // or create a custom category
        // $menu->addItem('CoreAdminHome_MenuManage', 'ExamplePlugin_MyUserItem', $this->urlForDefaultAction(), $orderId = 30);
    }
}
