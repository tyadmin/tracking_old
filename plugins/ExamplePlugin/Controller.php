<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\ExamplePlugin;

use Collect\View;

/**
 * A controller lets you for example create a page that can be added to a menu. For more information read our guide
 * http://developer.collect.org/guides/mvc-in-collect or have a look at the our API references for controller and view:
 * http://developer.collect.org/api-reference/Collect/Plugin/Controller and
 * http://developer.collect.org/api-reference/Collect/View
 */
class Controller extends \Collect\Plugin\Controller
{
    public function index()
    {
        // Render the Twig template templates/index.twig and assign the view variable answerToLife to the view.
        return $this->renderTemplate('index', array(
            'answerToLife' => 42
        ));
    }
}
