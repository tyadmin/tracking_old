/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

/**
 * Usage:
 * <div collect-component>
 */
(function () {
    angular.module('collectApp').directive('collectComponent', collectComponent);

    collectComponent.$inject = ['collect'];

    function collectComponent(collect){
        var defaults = {
            // showAllSitesItem: 'true'
        };

        return {
            restrict: 'A',
            scope: {
               // showAllSitesItem: '<'
            },
            templateUrl: 'plugins/ExamplePlugin/angularjs/directive-component/component.directive.html?cb=' + collect.cacheBuster,
            controller: 'ComponentController',
            controllerAs: 'componentAs',
            compile: function (element, attrs) {

                for (var index in defaults) {
                    if (defaults.hasOwnProperty(index) && attrs[index] === undefined) {
                        attrs[index] = defaults[index];
                    }
                }

                return function (scope, element, attrs) {

                };
            }
        };
    }
})();