<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Live\Widgets;

use Collect\Common;
use Collect\Collect;
use Collect\Plugins\Live\Live;
use Collect\Widget\WidgetConfig;

class GetVisitorProfilePopup extends \Collect\Widget\Widget
{

    public static function configure(WidgetConfig $config)
    {
        $config->setCategoryId('General_Visitors');
        $config->setName('Live_VisitorProfile');
        $config->setOrder(25);

        if (Collect::isUserIsAnonymous()) {
            $config->disable();
        }

        $idSite = Common::getRequestVar('idSite', 0, 'int');

        if (empty($idSite)) {
            return;
        }

        if (!Live::isVisitorProfileEnabled($idSite)) {
            $config->disable();
        }
    }

    public function render()
    {

    }

}
