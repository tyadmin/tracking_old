<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Live\Reports;

use Collect\Config;
use Collect\Metrics\Formatter;
use Collect\Collect;
use Collect\Plugin\Report;
use Collect\Plugins\Live\Controller;
use Collect\API\Request;
use Collect\Report\ReportWidgetFactory;
use Collect\View;
use Collect\Widget\WidgetsList;

class GetSimpleLastVisitCount extends Base
{
    protected function init()
    {
        parent::init();
        $this->categoryId = 'General_Visitors';
        $this->order = 3;
    }

    public function configureWidgets(WidgetsList $widgetsList, ReportWidgetFactory $factory)
    {
        $widget = $factory->createWidget()->setName('Live_RealTimeVisitorCount')->setOrder(15);
        $widgetsList->addWidgetConfig($widget);
    }

    public function render()
    {
        $lastMinutes = Config::getInstance()->General[Controller::SIMPLE_VISIT_COUNT_WIDGET_LAST_MINUTES_CONFIG_KEY];

        $params    = array('lastMinutes' => $lastMinutes, 'showColumns' => array('visits', 'visitors', 'actions'));
        $lastNData = Request::processRequest('Live.getCounters', $params);

        $formatter = new Formatter();

        $view = new View('@Live/getSimpleLastVisitCount');
        $view->lastMinutes = $lastMinutes;
        $view->visitors    = $formatter->getPrettyNumber($lastNData[0]['visitors']);
        $view->visits      = $formatter->getPrettyNumber($lastNData[0]['visits']);
        $view->actions     = $formatter->getPrettyNumber($lastNData[0]['actions']);
        $view->refreshAfterXSecs = Config::getInstance()->General['live_widget_refresh_after_seconds'];
        $view->translations = array(
            'one_visitor' => Collect::translate('Live_NbVisitor'),
            'visitors'    => Collect::translate('Live_NbVisitors'),
            'one_visit'   => Collect::translate('General_OneVisit'),
            'visits'      => Collect::translate('General_NVisits'),
            'one_action'  => Collect::translate('General_OneAction'),
            'actions'     => Collect::translate('VisitsSummary_NbActionsDescription'),
            'one_minute'  => Collect::translate('Intl_OneMinute'),
            'minutes'     => Collect::translate('Intl_NMinutes')
        );

        return $view->render();
    }
}