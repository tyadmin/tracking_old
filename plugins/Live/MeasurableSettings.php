<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\Live;

use Collect\Collect;
use Collect\Settings\FieldConfig;
use Collect\Settings\Measurable\MeasurableSetting;

class MeasurableSettings extends \Collect\Settings\Measurable\MeasurableSettings
{
    /** @var MeasurableSetting|null */
    public $disableVisitorLog;

    /** @var MeasurableSetting|null */
    public $disableVisitorProfile;

    protected function init()
    {
        $this->disableVisitorLog     = $this->makeVisitorLogSetting();
        $this->disableVisitorProfile = $this->makeVisitorProfileSetting();

        $systemSettings = new SystemSettings();

        $this->disableVisitorLog->setIsWritableByCurrentUser(!$systemSettings->disableVisitorLog->getValue());
        $this->disableVisitorProfile->setIsWritableByCurrentUser(!$systemSettings->disableVisitorProfile->getValue());
    }

    private function makeVisitorLogSetting(): MeasurableSetting
    {
        $defaultValue = false;
        $type = FieldConfig::TYPE_BOOL;

        return $this->makeSetting('disable_visitor_log', $defaultValue, $type, function (FieldConfig $field) {
            $field->title = Collect::translate('Live_DisableVisitsLogAndProfile');
            $field->inlineHelp = Collect::translate('Live_DisableVisitsLogAndProfileDescription');
            $field->uiControl = FieldConfig::UI_CONTROL_CHECKBOX;
        });
    }

    private function makeVisitorProfileSetting(): MeasurableSetting
    {
        $defaultValue = false;
        $type = FieldConfig::TYPE_BOOL;

        return $this->makeSetting('disable_visitor_profile', $defaultValue, $type, function (FieldConfig $field) {
            $field->title = Collect::translate('Live_DisableVisitorProfile');
            $field->inlineHelp = Collect::translate('Live_DisableVisitorProfileDescription');
            $field->uiControl = FieldConfig::UI_CONTROL_CHECKBOX;
            $field->condition = 'disable_visitor_log==0';
        });
    }
}