<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Live\Categories;

use Collect\Category\Subcategory;
use Collect\Collect;

class RealTimeVisitorsSubcategory extends Subcategory
{
    protected $categoryId = 'General_Visitors';
    protected $id = 'General_RealTime';
    protected $order = 7;

    public function getHelp()
    {
        $result = '<p>' . Collect::translate('Live_RealTimeHelp1') . '</p>';
        $result .= '<p>' . Collect::translate('Live_RealTimeHelp2') . '</p>';
        return $result;
    }
}
