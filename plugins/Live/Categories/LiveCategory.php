<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Live\Categories;

use Collect\Category\Category;

class LiveCategory extends Category
{
    protected $id = 'Live!';
    protected $order = 2;
}
