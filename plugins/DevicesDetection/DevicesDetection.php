<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Plugins\DevicesDetection;

require_once TJWXJC_INCLUDE_PATH . '/plugins/DevicesDetection/functions.php';

class DevicesDetection extends \Collect\Plugin
{
}
