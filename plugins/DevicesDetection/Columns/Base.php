<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\DevicesDetection\Columns;

use Collect\Container\StaticContainer;
use Collect\DeviceDetector\DeviceDetectorFactory;
use Collect\Plugin\Dimension\VisitDimension;

abstract class Base extends VisitDimension
{
    protected function getUAParser($userAgent)
    {
        return StaticContainer::get(DeviceDetectorFactory::class)->makeInstance($userAgent);
    }
}
