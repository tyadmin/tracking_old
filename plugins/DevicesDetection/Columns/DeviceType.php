<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\DevicesDetection\Columns;

use Collect\Metrics\Formatter;
use Collect\Tracker\Request;
use Exception;
use Collect\Tracker\Visitor;
use Collect\Tracker\Action;
use DeviceDetector\Parser\Device\AbstractDeviceParser as DeviceParser;

class DeviceType extends Base
{
    protected $columnName = 'config_device_type';
    protected $columnType = 'TINYINT( 100 ) NULL DEFAULT NULL';
    protected $segmentName = 'deviceType';
    protected $type = self::TYPE_ENUM;
    protected $nameSingular = 'DevicesDetection_DeviceType';
    protected $namePlural = 'DevicesDetection_DeviceTypes';

    public function __construct()
    {
        $deviceTypes    = DeviceParser::getAvailableDeviceTypeNames();
        $deviceTypeList = implode(", ", $deviceTypes);

        $this->acceptValues = $deviceTypeList;
    }

    public function formatValue($value, $idSite, Formatter $formatter)
    {
        return \Collect\Plugins\DevicesDetection\getDeviceTypeLabel($value);
    }

    public function getEnumColumnValues()
    {
        $values = DeviceParser::getAvailableDeviceTypes();
        return array_flip($values);
    }

    /**
     * @param Request $request
     * @param Visitor $visitor
     * @param Action|null $action
     * @return mixed
     */
    public function onNewVisit(Request $request, Visitor $visitor, $action)
    {
        $userAgent = $request->getUserAgent();
        $parser    = $this->getUAParser($userAgent);

        return $parser->getDevice();
    }

    /**
     * @param Request $request
     * @param Visitor $visitor
     * @param Action|null $action
     * @return mixed
     */
    public function onAnyGoalConversion(Request $request, Visitor $visitor, $action)
    {
        return $visitor->getVisitorColumn($this->columnName);
    }
}