<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\DevicesDetection\Columns;

use DeviceDetector\Parser\OperatingSystem;
use Collect\Columns\DimensionSegmentFactory;
use Collect\Common;
use Collect\Metrics\Formatter;
use Collect\Collect;
use Collect\Plugin\Segment;
use Collect\Segment\SegmentsList;
use Collect\Tracker\Request;
use Collect\Tracker\Settings;
use Collect\Tracker\Visitor;
use Collect\Tracker\Action;

class Os extends Base
{
    protected $columnName = 'config_os';
    protected $columnType = 'CHAR(3) NULL';
    protected $segmentName = 'operatingSystemCode';
    protected $nameSingular = 'DevicesDetection_ColumnOperatingSystem';
    protected $namePlural = 'DevicesDetection_OperatingSystems';
    protected $acceptValues = 'WIN, LIN, MAX, AND, IOS etc.';
    protected $type = self::TYPE_TEXT;

    public function configureSegments(SegmentsList $segmentsList, DimensionSegmentFactory $dimensionSegmentFactory)
    {
        $segment = new Segment();
        $segment->setName('DevicesDetection_OperatingSystemCode');
        $segmentsList->addSegment($dimensionSegmentFactory->createSegment($segment));

        $segment = new Segment();
        $segment->setSegment('operatingSystemName');
        $segment->setName('DevicesDetection_ColumnOperatingSystem');
        $segment->setAcceptedValues('Windows, Linux, Mac, Android, iOS etc.');
        $segment->setNeedsMostFrequentValues(false);
        $segment->setSqlFilterValue(function ($val) {
            $oss = OperatingSystem::getAvailableOperatingSystems();
            $oss = array_map(function($val) {
                return mb_strtolower($val);
            }, $oss);
            $result   = array_search(mb_strtolower($val), $oss);

            if ($result === false) {
                $result = 'UNK';
            }

            return $result;
        });
        $segment->setSuggestedValuesCallback(function ($idSite, $maxValuesToReturn, $table) {
            return $this->sortStaticListByUsage(OperatingSystem::getAvailableOperatingSystems(), $table,
                'operatingSystemCode', $maxValuesToReturn);
        });
        $segmentsList->addSegment($dimensionSegmentFactory->createSegment($segment));
    }

    public function formatValue($value, $idSite, Formatter $formatter)
    {
        return \Collect\Plugins\DevicesDetection\getOSFamilyFullName($value);
    }

    public function getName()
    {
        return Collect::translate('DevicesDetection_OperatingSystemFamily');
    }

    /**
     * @param Request $request
     * @param Visitor $visitor
     * @param Action|null $action
     * @return mixed
     */
    public function onNewVisit(Request $request, Visitor $visitor, $action)
    {
        $userAgent = $request->getUserAgent();
        $parser    = $this->getUAParser($userAgent);

        if ($parser->isBot()) {
            $os = Settings::OS_BOT;
        } else {
            $os = $parser->getOS();
            $os = $os['short_name'] ?? 'UNK';
        }

        return $os;
    }
}
