<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\DevicesDetection\Reports;

use Collect\Collect;
use Collect\Plugin\ViewDataTable;
use Collect\Plugins\DevicesDetection\Columns\DeviceBrand;

class GetBrand extends Base
{
    protected function init()
    {
        parent::init();
        $this->dimension     = new DeviceBrand();
        $this->name          = Collect::translate('DevicesDetection_DeviceBrand');
        $this->documentation = Collect::translate('DevicesDetection_DeviceBrandReportDocumentation');
        $this->order = 4;
        $this->hasGoalMetrics = true;
        $this->subcategoryId = 'DevicesDetection_Devices';
    }

    public function configureView(ViewDataTable $view)
    {
        $view->config->show_search = true;
        $view->config->show_exclude_low_population = false;
        $view->config->addTranslation('label', Collect::translate("DevicesDetection_dataTableLabelBrands"));
    }

}
