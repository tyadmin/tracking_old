<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\DevicesDetection\Reports;

use Collect\Metrics;

abstract class Base extends \Collect\Plugin\Report
{
    protected function init()
    {
        $this->categoryId = 'General_Visitors';
    }
}
