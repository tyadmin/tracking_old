<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
namespace Collect\Plugins\PrivacyManager\Diagnostic;

use Collect\Plugins\Diagnostics\Diagnostic\Diagnostic;
use Collect\Plugins\Diagnostics\Diagnostic\DiagnosticResult;
use Collect\Plugins\PrivacyManager\Config;
use Collect\SettingsCollect;
use Collect\Translation\Translator;

/**
 * Information about Matomo itself
 */
class PrivacyInformational implements Diagnostic
{
    /**
     * @var Translator
     */
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function execute()
    {
        $results = [];

        if (SettingsCollect::isMatomoInstalled()) {
            $config = new Config();

            $results[] = DiagnosticResult::informationalResult('Anonymize Referrer', $config->anonymizeReferrer);
            $results[] = DiagnosticResult::informationalResult('Do Not Track enabled', $config->doNotTrackEnabled);
        }

        return $results;
    }


}
