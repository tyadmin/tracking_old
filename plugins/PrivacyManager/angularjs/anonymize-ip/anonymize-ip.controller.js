/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
(function () {
    angular.module('collectApp').controller('AnonymizeIpController', AnonymizeIpController);

    AnonymizeIpController.$inject = ['collectApi'];

    function AnonymizeIpController(collectApi) {
        // remember to keep controller very simple. Create a service/factory (model) if needed

        var self = this;
        this.isLoading = false;

        this.save = function () {
            this.isLoading = true;

            collectApi.post({module: 'API', method: 'PrivacyManager.setAnonymizeIpSettings'}, {
                anonymizeIPEnable: this.enabled ? '1' : '0',
                anonymizeUserId: this.anonymizeUserId ? '1' : '0',
                anonymizeOrderId: this.anonymizeOrderId ? '1' : '0',
                forceCookielessTracking: this.forceCookielessTracking ? '1' : '0',
                anonymizeReferrer: this.anonymizeReferrer ? this.anonymizeReferrer : '',
                maskLength: this.maskLength,
                useAnonymizedIpForVisitEnrichment: parseInt(this.useAnonymizedIpForVisitEnrichment, 10) ? '1' : '0'
            }).then(function (success) {
                self.isLoading = false;

                var UI = require('collect/UI');
                var notification = new UI.Notification();
                notification.show(_pk_translate('CoreAdminHome_SettingsSaveSuccess'), {context: 'success', id:'privacyManagerSettings'});
                notification.scrollToNotification();

            }, function () {
                self.isLoading = false;
            });
        };
    }
})();