/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

/**
 * Usage:
 * <div matomo-anonymize-log-data>
 */
(function () {
    angular.module('collectApp').directive('matomoAnonymizeLogData', anonymizeLogData);

    anonymizeLogData.$inject = ['collect'];

    function anonymizeLogData(collect){
        return {
            restrict: 'A',
            scope: {},
            templateUrl: 'plugins/PrivacyManager/angularjs/anonymize-log-data/anonymize-log-data.directive.html?cb=' + collect.cacheBuster,
            controller: 'AnonymizeLogDataController',
            controllerAs: 'anonymizeLogData',
            compile: function (element, attrs) {

            }
        };
    }
})();