<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Marketplace\Widgets;

use Collect\Common;
use Collect\Collect;
use Collect\Plugins\Marketplace\Api\Client;
use Collect\Plugins\Marketplace\Input\PurchaseType;
use Collect\Plugins\Marketplace\Input\Sort;
use Collect\Widget\Widget;
use Collect\Widget\WidgetConfig;

class Marketplace extends Widget
{
    public static function configure(WidgetConfig $config)
    {
        $config->setCategoryId('Marketplace_Marketplace');
        $config->setSubcategoryId('Marketplace_Browse');
        $config->setName(Collect::translate('Marketplace_Marketplace'));
        $config->setModule('Marketplace');
        $config->setAction('overview');
        $config->setParameters(array('embed' => '1'));
        $config->setIsNotWidgetizable();
        $config->setOrder(19);
        $config->setIsEnabled(!Collect::isUserIsAnonymous());
    }


}