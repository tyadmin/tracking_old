/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
(function () {
    angular.module('collectApp').controller('CollectMarketplaceController', CollectMarketplaceController);

    CollectMarketplaceController.$inject = ['collect'];

    function CollectMarketplaceController(collect) {
        // remember to keep controller very simple. Create a service/factory (model) if needed

        this.changePluginSort = function () {
            collect.broadcast.propagateNewPage('query=&sort=' + this.pluginSort);
        };

        this.changePluginType = function () {
            collect.broadcast.propagateNewPage('query=&show=' + this.pluginType);
        };
    }
})();
