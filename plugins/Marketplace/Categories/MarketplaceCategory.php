<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Marketplace\Categories;

use Collect\Category\Category;
use Collect\Collect;

class MarketplaceCategory extends Category
{
    protected $id = 'Marketplace_Marketplace';
    protected $order = 200;
    protected $icon = ' icon-open-source';

    public function getDisplayName()
    {
        return Collect::translate('Marketplace_Marketplace');
    }
}
