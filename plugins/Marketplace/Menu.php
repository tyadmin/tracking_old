<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Marketplace;

use Collect\Menu\MenuAdmin;
use Collect\Collect;

/**
 */
class Menu extends \Collect\Plugin\Menu
{

    public function configureAdminMenu(MenuAdmin $menu)
    {
        if (!Collect::isUserIsAnonymous()) {
            $menu->addPlatformItem('Marketplace_Marketplace',
                $this->urlForAction('overview', array('activated' => '', 'mode' => 'admin', 'type' => '', 'show' => '')),
                $order = 5);
        }
    }

}
