<?php

use Psr\Container\ContainerInterface;
use Collect\Plugins\Marketplace\Api\Service;
use Collect\Plugins\Marketplace\LicenseKey;

return array(
    'MarketplaceEndpoint' => function (ContainerInterface $c) {
        $domain = 'http://plugins.matomo.org';
        $updater = $c->get('Collect\Plugins\CoreUpdater\Updater');

        if ($updater->isUpdatingOverHttps()) {
            $domain = str_replace('http://', 'https://', $domain);
        }

        return $domain;
    },
    'Collect\Plugins\Marketplace\Api\Service' => function (ContainerInterface $c) {
        /** @var \Collect\Plugins\Marketplace\Api\Service $previous */

        $domain = $c->get('MarketplaceEndpoint');

        $service = new Service($domain);

        $key = new LicenseKey();
        $accessToken = $key->get();

        $service->authenticate($accessToken);

        return $service;
    }
);
