/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

import { IDirective } from 'angular';
import PluginUpload from './PluginUpload';

export default function collectPluginUpload(): IDirective {
  return {
    restrict: 'A',
    link: function expandOnClickLink() {
      PluginUpload.mounted();
    },
  };
}

window.angular.module('collectApp').directive('collectPluginUpload', collectPluginUpload);
