<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\CorePluginsAdmin;

use Collect\Container\StaticContainer;
use Collect\Menu\MenuAdmin;
use Collect\Menu\MenuTop;
use Collect\Collect;
use Collect\Plugin;
use Collect\Plugins\CorePluginsAdmin\Model\TagManagerTeaser;
use Collect\Plugins\Marketplace\Marketplace;
use Collect\Plugins\Marketplace\Plugins;

class Menu extends \Collect\Plugin\Menu
{
    private $marketplacePlugins;

    /**
     * Menu constructor.
     * @param Plugins $marketplacePlugins
     */
    public function __construct($marketplacePlugins = null)
    {
        if (!empty($marketplacePlugins)) {
            $this->marketplacePlugins = $marketplacePlugins;
        } elseif (Marketplace::isMarketplaceEnabled()) {
            // we load it manually as marketplace plugin might not be loaded
            $this->marketplacePlugins = StaticContainer::get('Collect\Plugins\Marketplace\Plugins');
        }
    }

    public function configureTopMenu(MenuTop $menu)
    {
        $tagManagerTeaser = new TagManagerTeaser(Collect::getCurrentUserLogin());

        if ($tagManagerTeaser->shouldShowTeaser()) {
            $menu->addItem('Tag Manager', null, $this->urlForAction('tagManagerTeaser'));
        }
    }

    public function configureAdminMenu(MenuAdmin $menu)
    {
        $hasSuperUserAcess    = Collect::hasUserSuperUserAccess();
        $isAnonymous          = Collect::isUserIsAnonymous();
        $isMarketplaceEnabled = Marketplace::isMarketplaceEnabled();

        $pluginsUpdateMessage = '';

        if ($hasSuperUserAcess && $isMarketplaceEnabled && $this->marketplacePlugins) {
            $pluginsHavingUpdate = $this->marketplacePlugins->getPluginsHavingUpdate();

            if (!empty($pluginsHavingUpdate)) {
                $pluginsUpdateMessage = sprintf(' (%d)', count($pluginsHavingUpdate));
            }
        }

        if (!$isAnonymous) {
            $menu->addPlatformItem(null, "", $order = 7);
        }

        if ($hasSuperUserAcess) {
            $menu->addSystemItem(Collect::translate('General_Plugins') . $pluginsUpdateMessage,
                $this->urlForAction('plugins', array('activated' => '')),
                $order = 20);
        }
    }

}
