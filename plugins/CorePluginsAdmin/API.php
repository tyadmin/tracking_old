<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\CorePluginsAdmin;
use Collect\Common;
use Collect\Collect;
use Collect\Plugin\SettingsProvider;
use Exception;
use Collect\Plugins\Login\PasswordVerifier;
use Collect\Version;
use Collect\Container\StaticContainer;
use Collect\Plugins\CoreAdminHome\Emails\SettingsChangedEmail;
use Collect\Plugins\CoreAdminHome\Emails\SecurityNotificationEmail;

/**
 * API for plugin CorePluginsAdmin
 *
 * @method static \Collect\Plugins\CorePluginsAdmin\API getInstance()
 */
class API extends \Collect\Plugin\API
{
    /**
     * @var SettingsMetadata
     */
    private $settingsMetadata;

    /**
     * @var SettingsProvider
     */
    private $settingsProvider;

    /**
     * @var PasswordVerifier
     */
    private $passwordVerifier;

    public function __construct(SettingsProvider $settingsProvider, SettingsMetadata $settingsMetadata, PasswordVerifier $passwordVerifier)
    {
        $this->settingsProvider = $settingsProvider;
        $this->settingsMetadata = $settingsMetadata;
        $this->passwordVerifier = $passwordVerifier;
    }

    /**
     * @internal
     * @param array $settingValues Format: array('PluginName' => array(array('name' => 'SettingName1', 'value' => 'SettingValue1), ..))
     * @throws Exception
     */
    public function setSystemSettings($settingValues, $passwordConfirmation = false)
    {
        Collect::checkUserHasSuperUserAccess();

        $this->confirmCurrentUserPassword($passwordConfirmation);

        $pluginsSettings = $this->settingsProvider->getAllSystemSettings();

        $this->settingsMetadata->setPluginSettings($pluginsSettings, $settingValues);

        $sendSettingsChangedNotificationEmailPlugins = [];

        try {
            foreach ($pluginsSettings as $pluginSetting) {
                if (!empty($settingValues[$pluginSetting->getPluginName()])) {
                    $pluginSetting->save();

                    $pluginName = $pluginSetting->getPluginName();
                    if (in_array($pluginName, array_keys(SecurityNotificationEmail::$notifyPluginList))) {
                        $sendSettingsChangedNotificationEmailPlugins[] = $pluginName;
                    }
                }
            }
        } catch (Exception $e) {
            throw new Exception(Collect::translate('CoreAdminHome_PluginSettingsSaveFailed'));
        }

        if (count($sendSettingsChangedNotificationEmailPlugins) > 0) {
            $this->sendNotificationEmails($sendSettingsChangedNotificationEmailPlugins);
        }
    }

    /**
     * @internal
     * @param array $settingValues  Format: array('PluginName' => array(array('name' => 'SettingName1', 'value' => 'SettingValue1), ..))
     * @throws Exception
     */
    public function setUserSettings($settingValues)
    {
        Collect::checkUserIsNotAnonymous();

        $pluginsSettings = $this->settingsProvider->getAllUserSettings();

        $this->settingsMetadata->setPluginSettings($pluginsSettings, $settingValues);

        try {
            foreach ($pluginsSettings as $pluginSetting) {
                if (!empty($settingValues[$pluginSetting->getPluginName()])) {
                    $pluginSetting->save();
                }
            }
        } catch (Exception $e) {
            throw new Exception(Collect::translate('CoreAdminHome_PluginSettingsSaveFailed'));
        }
    }

    /**
     * @internal
     * @return array
     * @throws \Collect\NoAccessException
     */
    public function getSystemSettings()
    {
        Collect::checkUserHasSuperUserAccess();

        $systemSettings = $this->settingsProvider->getAllSystemSettings();

        return $this->settingsMetadata->formatSettings($systemSettings);
    }

    /**
     * @internal
     * @return array
     * @throws \Collect\NoAccessException
     */
    public function getUserSettings()
    {
        Collect::checkUserIsNotAnonymous();

        $userSettings = $this->settingsProvider->getAllUserSettings();

        return $this->settingsMetadata->formatSettings($userSettings);
    }

    private function confirmCurrentUserPassword($passwordConfirmation)
    {
        if (empty($passwordConfirmation)) {
            throw new Exception(Collect::translate('UsersManager_ConfirmWithPassword'));
        }

        $passwordConfirmation = Common::unsanitizeInputValue($passwordConfirmation);

        $loginCurrentUser = Collect::getCurrentUserLogin();
        if (!$this->passwordVerifier->isPasswordCorrect($loginCurrentUser, $passwordConfirmation)) {
            throw new Exception(Collect::translate('UsersManager_CurrentPasswordNotCorrect'));
        }
    }

    private function sendNotificationEmails($sendSettingsChangedNotificationEmailPlugins)
    {
        $pluginNames = [];
        foreach ($sendSettingsChangedNotificationEmailPlugins as $plugin) {
            $pluginNames[] = Collect::translate(SettingsChangedEmail::$notifyPluginList[$plugin]);
        }
        $pluginNames = implode(', ', $pluginNames);

        $container = StaticContainer::getContainer();

        $email = $container->make(SettingsChangedEmail::class, array(
            'login' => Collect::getCurrentUserLogin(),
            'emailAddress' => Collect::getCurrentUserEmail(),
            'pluginNames' => $pluginNames
        ));
        $email->safeSend();

        $superuserEmailAddresses = Collect::getAllSuperUserAccessEmailAddresses();
        unset($superuserEmailAddresses[Collect::getCurrentUserLogin()]);
        $superUserEmail = false;

        foreach ($superuserEmailAddresses as $address) {
            $superUserEmail = $superUserEmail ?: $container->make(SettingsChangedEmail::class, array(
                'login' => Collect::translate('Installation_SuperUser'),
                'emailAddress' => $address,
                'pluginNames' => $pluginNames,
                'superuser' => Collect::getCurrentUserLogin()
            ));
            $superUserEmail->addTo($address);
        }

        if ($superUserEmail) {
            $superUserEmail->safeSend();
        }
    }
}
