<?php

return array(
    'observers.global' => DI\add(array(
        array('Request.dispatchCoreAndPluginUpdatesScreen', \DI\value(function () {
            $pluginName = 'TagManager';
            $unloadTagManager = \Collect\Container\StaticContainer::get('test.vars.unloadTagManager');
            $tagManagerTeaser = new \Collect\Plugins\CorePluginsAdmin\Model\TagManagerTeaser(\Collect\Collect::getCurrentUserLogin());
            if ($unloadTagManager) {
                $pluginManager = \Collect\Plugin\Manager::getInstance();
                if ($pluginManager->isPluginActivated($pluginName)
                    && $pluginManager->isPluginLoaded($pluginName)) {
                    $pluginManager->unloadPlugin($pluginName);
                }
                $tagManagerTeaser->reset();
            }
        })),
    ))
);
