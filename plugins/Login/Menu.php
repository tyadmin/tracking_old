<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\Login;

use Collect\Menu\MenuAdmin;
use Collect\Collect;

class Menu extends \Collect\Plugin\Menu
{

    public function configureAdminMenu(MenuAdmin $menu)
    {
        if (Collect::hasUserSuperUserAccess()) {
            $systemSettings = new SystemSettings();
            if ($systemSettings->enableBruteForceDetection->getValue()) {
                $menu->addDiagnosticItem('Login_BruteForceLog', $this->urlForAction('bruteForceLog'), $orderId = 30);
            }
        }
    }
}
