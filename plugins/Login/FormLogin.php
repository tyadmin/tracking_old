<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Login;

use HTML_QuickForm2_DataSource_Array;
use Collect\Collect;
use Collect\QuickForm2;

/**
 *
 */
class FormLogin extends QuickForm2
{
    function __construct($id = 'login_form', $method = 'post', $attributes = null, $trackSubmit = false)
    {
        $attributes = array_merge($attributes ?: [], [ 'action' => '?module=' . Collect::getLoginPluginName() ]);
        parent::__construct($id, $method, $attributes, $trackSubmit);
    }

    function init()
    {
        $this->addElement('text', 'form_login')
            ->addRule('required', Collect::translate('General_Required', Collect::translate('Login_LoginOrEmail')));

        $this->addElement('password', 'form_password')
            ->addRule('required', Collect::translate('General_Required', Collect::translate('General_Password')));

        $this->addElement('hidden', 'form_redirect');

        $this->addElement('hidden', 'form_nonce');

        $this->addElement('checkbox', 'form_rememberme');

        $this->addElement('submit', 'submit');

        // default values
        $this->addDataSource(new HTML_QuickForm2_DataSource_Array(array(
                                                                       'form_rememberme' => 0,
                                                                  )));
    }
}
