<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\Login;

use Collect\Collect;
use Collect\Plugins\Login\Security\BruteForceDetection;

/**
 * API for plugin Login
 *
 * @method static \Collect\Plugins\Login\API getInstance()
 */
class API extends \Collect\Plugin\API
{
    /**
     * @var BruteForceDetection
     */
    private $bruteForceDetection;

    public function __construct(BruteForceDetection $bruteForceDetection)
    {
        $this->bruteForceDetection = $bruteForceDetection;
    }

    public function unblockBruteForceIPs()
    {
        Collect::checkUserHasSuperUserAccess();

        $ips = $this->bruteForceDetection->getCurrentlyBlockedIps();
        if (!empty($ips)) {
            foreach ($ips as $ip) {
                $this->bruteForceDetection->unblockIp($ip);
            }
        }
    }
}
