<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link    https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\CustomJsTracker;

use Collect\Common;
use Collect\Container\StaticContainer;
use Collect\Plugins\CustomJsTracker\TrackingCode\CollectJsManipulator;
use Collect\Plugins\CustomJsTracker\TrackingCode\PluginTrackerFiles;
use Collect\Collect;

/**
 * Updates the Collect JavaScript Tracker "collect.js" in case plugins extend the tracker.
 *
 * Usage:
 * StaticContainer::get('Collect\Plugins\CustomJsTracker\TrackerUpdater')->update();
 */
class TrackerUpdater
{
    const TJWXJC_DEVELOPMENT_JS = '/js/collect.js';
    const TJWXJC_ORIGINAL_JS = '/js/collect.min.js';
    const TJWXJC_TARGET_JS = '/matomo.js';

    /**
     * @var File
     */
    private $fromFile;

    /**
     * @var File
     */
    private $toFile;

    private $trackerFiles = array();

    /**
     * @param string|null $fromFile If null then the minified JS tracker file in /js fill be used
     * @param string|null $toFile If null then the minified JS tracker will be updated.
     */
    public function __construct($fromFile = null, $toFile = null)
    {
        if (!isset($fromFile)) {
            $fromFile = TJWXJC_DOCUMENT_ROOT . self::TJWXJC_ORIGINAL_JS;
        }

        if (!isset($toFile)) {
            $toFile = TJWXJC_DOCUMENT_ROOT . self::TJWXJC_TARGET_JS;
        }

        $this->setFromFile($fromFile);
        $this->setToFile($toFile);
        $this->trackerFiles = StaticContainer::get('Collect\Plugins\CustomJsTracker\TrackingCode\PluginTrackerFiles');
    }

    public function setFromFile($fromFile)
    {
        if (is_string($fromFile)) {
            $fromFile = new File($fromFile);
        }
        $this->fromFile = $fromFile;
    }

    public function getFromFile()
    {
        return $this->fromFile;
    }

    public function setToFile($toFile)
    {
        if (is_string($toFile)) {
            $toFile = new File($toFile);
        }
        $this->toFile = $toFile;
    }

    public function getToFile()
    {
        return $this->toFile;
    }

    public function setTrackerFiles(PluginTrackerFiles $trackerFiles)
    {
        $this->trackerFiles = $trackerFiles;
    }

    /**
     * Checks whether the Collect JavaScript tracker file "collect.js" is writable.
     * @throws \Exception In case the collect.js file is not writable.
     *
     * @api
     */
    public function checkWillSucceed()
    {
        $this->fromFile->checkReadable();
        $this->toFile->checkWritable();
    }

    public function getCurrentTrackerFileContent()
    {
        return $this->toFile->getContent();
    }

    public function getUpdatedTrackerFileContent()
    {
        $trackingCode = new CollectJsManipulator($this->fromFile->getContent(), $this->trackerFiles);
        $newContent = $trackingCode->manipulateContent();

        return $newContent;
    }

    /**
     * Updates / re-generates the Collect JavaScript tracker "collect.js".
     *
     * It may not be possible to update the "collect.js" tracker file if the file is not writable. It won't throw
     * an exception in such a case and instead just to nothing. To check if the update would succeed, call
     * {@link checkWillSucceed()}.
     *
     * @api
     */
    public function update()
    {
        if (!$this->toFile->hasWriteAccess() || !$this->fromFile->hasReadAccess()) {
            return;
        }

        $newContent = $this->getUpdatedTrackerFileContent();

        if (!$this->toFile->isFileContentSame($newContent)) {
            $savedFiles = $this->toFile->save($newContent);
            foreach ($savedFiles as $savedFile) {

                /**
                 * Triggered after the tracker JavaScript content (the content of the collect.js file) is changed.
                 *
                 * @param string $absolutePath The path to the new collect.js file.
                 */
                Collect::postEvent('CustomJsTracker.trackerJsChanged', [$savedFile]);
            }

        }

        // we need to make sure to sync matomo.js / collect.js
        $this->updateAlternative('collect.js', 'matomo.js', $newContent);
        $this->updateAlternative('matomo.js', 'collect.js', $newContent);
    }

    private function updateAlternative($fromFile, $toFile, $newContent)
    {
        if (Common::stringEndsWith($this->toFile->getName(), $fromFile)) {
            $alternativeFilename = dirname($this->toFile->getPath()) . DIRECTORY_SEPARATOR . $toFile;
            $file = $this->toFile->setFile($alternativeFilename);
            if ($file->hasWriteAccess() && !$file->isFileContentSame($newContent)) {
                $savedFiles = $file->save($newContent);
                foreach ($savedFiles as $savedFile) {
                    Collect::postEvent('CustomJsTracker.trackerJsChanged', [$savedFile]);
                }
            }
        }
    }
}
