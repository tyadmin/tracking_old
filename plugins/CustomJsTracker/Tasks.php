<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\CustomJsTracker;

use Collect\Container\StaticContainer;

class Tasks extends \Collect\Plugin\Tasks
{
    public function schedule()
    {
        $this->hourly('updateTracker');
    }

    public function updateTracker()
    {
        $updater = StaticContainer::get('Collect\Plugins\CustomJsTracker\TrackerUpdater');
        $updater->update();
    }
}
