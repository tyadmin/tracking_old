<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\CustomJsTracker;

use Collect\Container\StaticContainer;
use Collect\Collect;
use Collect\Plugins\CustomJsTracker\Exception\AccessDeniedException;

/**
 * API for plugin CustomJsTracker
 *
 * @method static \Collect\Plugins\CustomJsTracker\API getInstance()
 */
class API extends \Collect\Plugin\API
{
    /**
     * Detects whether plugin trackers will be automatically added to collect.js or not. If not, the plugin tracker files
     * need to be loaded manually.
     * @return bool
     */
    public function doesIncludePluginTrackersAutomatically()
    {
        Collect::checkUserHasSomeAdminAccess();

        try {
            $updater = StaticContainer::get('Collect\Plugins\CustomJsTracker\TrackerUpdater');
            $updater->checkWillSucceed();
            return true;
        } catch (AccessDeniedException $e) {
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

}
