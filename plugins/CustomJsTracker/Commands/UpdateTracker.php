<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\CustomJsTracker\Commands;

use Collect\Container\StaticContainer;
use Collect\Plugin\ConsoleCommand;
use Collect\Plugins\CustomJsTracker\TrackerUpdater;
use Collect\Plugins\CustomJsTracker\TrackingCode\PluginTrackerFiles;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateTracker extends ConsoleCommand
{
    protected function configure()
    {
        $this->setName('custom-collect-js:update');
        $this->setAliases(array('custom-matomo-js:update'));
        $this->addOption('source-file', null, InputOption::VALUE_REQUIRED, 'Absolute path to source CollectJS file.', $this->getPathOriginalCollectJs());
        $this->addOption('target-file', null, InputOption::VALUE_REQUIRED, 'Absolute path to target file. Useful if your /matomo.js is not writable and you want to replace the file manually', TJWXJC_DOCUMENT_ROOT . TrackerUpdater::TJWXJC_TARGET_JS);
        $this->addOption('ignore-minified', null, InputOption::VALUE_NONE, 'Ignore minified tracker files, useful during development so the original source file can be debugged');
        $this->setDescription('Update the Javascript Tracker with plugin tracker additions');
    }

    private function getPathOriginalCollectJs()
    {
        return TJWXJC_DOCUMENT_ROOT . TrackerUpdater::TJWXJC_ORIGINAL_JS;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sourceFile = $input->getOption('source-file');
        $targetFile = $input->getOption('target-file');
        $ignoreMinified = (bool)$input->getOption('ignore-minified');

        $this->updateTracker($sourceFile, $targetFile, $ignoreMinified);

        $output->writeln('<info>The Javascript Tracker has been updated</info>');
    }

    public function updateTracker($sourceFile, $targetFile, $ignoreMinified)
    {
        $pluginTrackerFiles = StaticContainer::get('Collect\Plugins\CustomJsTracker\TrackingCode\PluginTrackerFiles');

        if ($ignoreMinified) {
            if (empty($sourceFile) || $sourceFile === $this->getPathOriginalCollectJs()) {
                // no custom source file was requested
                $sourceFile = TJWXJC_DOCUMENT_ROOT . TrackerUpdater::TJWXJC_DEVELOPMENT_JS;
            }
            $pluginTrackerFiles->ignoreMinified();
        }

        $updater = StaticContainer::getContainer()->make('Collect\Plugins\CustomJsTracker\TrackerUpdater', array(
            'fromFile' => $sourceFile, 'toFile' => $targetFile
        ));
        $updater->setTrackerFiles($pluginTrackerFiles);
        $updater->checkWillSucceed();
        $updater->update();
    }
}
