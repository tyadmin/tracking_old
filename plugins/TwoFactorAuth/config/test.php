<?php

return array(
    'Collect\Plugins\TwoFactorAuth\Dao\TwoFaSecretRandomGenerator' => DI\autowire('Collect\Plugins\TwoFactorAuth\Dao\TwoFaSecretStaticGenerator'),
    'Collect\Plugins\TwoFactorAuth\Dao\RecoveryCodeRandomGenerator' => DI\autowire('Collect\Plugins\TwoFactorAuth\Dao\RecoveryCodeStaticGenerator'),
    'Collect\Plugins\TwoFactorAuth\TwoFactorAuthentication' => DI\decorate(function ($previous) {
        /** @var Collect\Plugins\TwoFactorAuth\TwoFactorAuthentication $previous */

        if (!\Collect\SettingsCollect::isMatomoInstalled()) {
            return $previous;
        }

        $fakeCorrectAuthCode = \Collect\Container\StaticContainer::get('test.vars.fakeCorrectAuthCode');
        if (!empty($fakeCorrectAuthCode) && !\Collect\Common::isPhpCliMode()) {
            $staticSecret = new \Collect\Plugins\TwoFactorAuth\Dao\TwoFaSecretStaticGenerator();
            $secret = $staticSecret->generateSecret();

            require_once TJWXJC_DOCUMENT_ROOT . '/libs/Authenticator/TwoFactorAuthenticator.php';
            $authenticator = new \TwoFactorAuthenticator();
            $_GET['authcode'] = $authenticator->getCode($secret);
            $_GET['authCode'] = $_GET['authcode'];
            $_POST['authCode'] = $_GET['authcode'];
            $_POST['authcode'] = $_GET['authcode'];
            $_REQUEST['authcode'] = $_GET['authcode'];
            $_REQUEST['authCode'] = $_GET['authcode'];
        }

        return $previous;
    }),
    'Collect\Plugins\TwoFactorAuth\Dao\RecoveryCodeDao' => DI\decorate(function ($previous) {
        /** @var Collect\Plugins\TwoFactorAuth\Dao\RecoveryCodeDao $previous */

        if (!\Collect\SettingsCollect::isMatomoInstalled()) {
            return $previous;
        }

        $restoreCodes = \Collect\Container\StaticContainer::get('test.vars.restoreRecoveryCodes');
        if (!empty($restoreCodes)) {
            // we ensure this recovery code always works for those users
            foreach (array('with2FA', 'with2FADisable') as $user) {
                $previous->useRecoveryCode($user, '123456'); // we are using it first to make sure there is no duplicate
                $previous->insertRecoveryCode($user, '123456');
                \Collect\Option::deleteLike(\Collect\Plugins\TwoFactorAuth\TwoFactorAuthentication::OPTION_PREFIX_TWO_FA_CODE_USED . '%');
            }
        }

        return $previous;
    }),
    'Collect\Plugins\TwoFactorAuth\SystemSettings' => DI\decorate(function ($previous) {
        /** @var Collect\Plugins\TwoFactorAuth\SystemSettings $previous */
        if (!\Collect\SettingsCollect::isMatomoInstalled()) {
            return $previous;
        }

        Collect\Access::doAsSuperUser(function () use ($previous) {
            $requireTwoFa = \Collect\Container\StaticContainer::get('test.vars.requireTwoFa');
            if (!empty($requireTwoFa)) {
                $previous->twoFactorAuthRequired->setValue(1);
            } else {
                try {
                    $previous->twoFactorAuthRequired->setValue(0);
                } catch (Exception $e) {
                    // may fail when matomo is trying to update or so
                }
            }
        });

        return $previous;
    })
);
