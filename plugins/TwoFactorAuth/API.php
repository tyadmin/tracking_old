<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\TwoFactorAuth;

use Collect\Collect;
use Collect\Plugins\Login\PasswordVerifier;

class API extends \Collect\Plugin\API
{
    /**
     * @var TwoFactorAuthentication
     */
    private $twoFa;

    /**
     * @var PasswordVerifier
     */
    private $passwordVerifier;

    public function __construct(TwoFactorAuthentication $twoFa, PasswordVerifier $passwordVerifier)
    {
        $this->twoFa = $twoFa;
        $this->passwordVerifier = $passwordVerifier;
    }

    public function resetTwoFactorAuth($userLogin, $passwordConfirmation)
    {
        Collect::checkUserHasSuperUserAccess();

        if (!$this->passwordVerifier->isPasswordCorrect(Collect::getCurrentUserLogin(), $passwordConfirmation)) {
            throw new \Exception(Collect::translate('UsersManager_CurrentPasswordNotCorrect'));
        }

        $this->twoFa->disable2FAforUser($userLogin);
    }
}
