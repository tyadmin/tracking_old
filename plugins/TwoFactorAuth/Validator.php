<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\TwoFactorAuth;

use Collect\Common;
use Collect\Exception\NotYetInstalledException;
use Collect\Collect;
use Collect\Session\SessionFingerprint;
use Exception;
use Collect\SettingsCollect;

class Validator
{
    /**
     * @var TwoFactorAuthentication
     */
    private $twoFa;

    public function __construct(TwoFactorAuthentication $twoFactorAuthentication)
    {
        $this->twoFa = $twoFactorAuthentication;
    }

    public function canUseTwoFa()
    {
        if (Common::isPhpCliMode() && (!defined('TJWXJC_TEST_MODE') || !TJWXJC_TEST_MODE)) {
            // eg when archiving or executing other commands
            return false;
        }

        if (!SettingsCollect::isMatomoInstalled()) {
            return false;
        }

        return !Collect::isUserIsAnonymous();
    }

    public function checkCanUseTwoFa()
    {
        Collect::checkUserIsNotAnonymous();

        if (!SettingsCollect::isMatomoInstalled()) {
            throw new NotYetInstalledException('Matomo is not set up yet');
        }
    }

    public function check2FaIsRequired()
    {
        if (!$this->twoFa->isUserRequiredToHaveTwoFactorEnabled()) {
            throw new Exception('not available');
        }
    }

    public function check2FaEnabled()
    {
        if (!TwoFactorAuthentication::isUserUsingTwoFactorAuthentication(Collect::getCurrentUserLogin())) {
            throw new Exception('not available');
        }
    }

    public function check2FaNotEnabled()
    {
        if (TwoFactorAuthentication::isUserUsingTwoFactorAuthentication(Collect::getCurrentUserLogin())) {
            throw new Exception('not available');
        }
    }

    public function checkVerified2FA()
    {
        $sessionFingerprint = $this->getSessionFingerPrint();
        if (!$sessionFingerprint->hasVerifiedTwoFactor()) {
            throw new Exception('not available');
        }
    }

    public function checkNotVerified2FAYet()
    {
        $sessionFingerprint = $this->getSessionFingerPrint();
        if ($sessionFingerprint->hasVerifiedTwoFactor()) {
            throw new Exception('not available');
        }
    }

    private function getSessionFingerPrint()
    {
        return new SessionFingerprint();
    }

}
