<?php

use Collect\Plugins\Diagnostics\Diagnostic\CronArchivingLastRunCheck;
use Collect\Plugins\Diagnostics\Diagnostic\RequiredPrivateDirectories;
use Collect\Plugins\Diagnostics\Diagnostic\RecommendedPrivateDirectories;

return array(
    // Diagnostics for everything that is required for Collect to run
    'diagnostics.required' => array(
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\PhpVersionCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\DbAdapterCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\DbReaderCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\PhpExtensionsCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\PhpFunctionsCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\PhpSettingsCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\WriteAccessCheck'),
    ),
    // Diagnostics for recommended features
    'diagnostics.optional' => array(
        DI\get(RequiredPrivateDirectories::class),
        DI\get(RecommendedPrivateDirectories::class),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\FileIntegrityCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\PHPBinaryCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\TrackerCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\MemoryLimitCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\TimezoneCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\HttpClientCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\PageSpeedCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\GdExtensionCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\RecommendedExtensionsCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\RecommendedFunctionsCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\NfsDiskCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\CronArchivingCheck'),
        DI\get(CronArchivingLastRunCheck::class),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\DatabaseAbilitiesCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\DbOverSSLCheck'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\DbMaxPacket'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\ForceSSLCheck'),
    ),
    'diagnostics.informational' => array(
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\MatomoInformational'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\PhpInformational'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\DatabaseInformational'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\ConfigInformational'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\ServerInformational'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\ReportInformational'),
        DI\get('Collect\Plugins\Diagnostics\Diagnostic\UserInformational'),
        DI\get(\Collect\Plugins\Diagnostics\Diagnostic\ArchiveInvalidationsInformational::class),
    ),
    // Allows other plugins to disable diagnostics that were previously registered
    'diagnostics.disabled' => array(),

    'Collect\Plugins\Diagnostics\DiagnosticService' => DI\autowire()
        ->constructor(DI\get('diagnostics.required'), DI\get('diagnostics.optional'), DI\get('diagnostics.informational'), DI\get('diagnostics.disabled')),

    'Collect\Plugins\Diagnostics\Diagnostic\MemoryLimitCheck' => DI\autowire()
        ->constructorParameter('minimumMemoryLimit', DI\get('ini.General.minimum_memory_limit')),

    'Collect\Plugins\Diagnostics\Diagnostic\WriteAccessCheck' => DI\autowire()
        ->constructorParameter('tmpPath', DI\get('path.tmp')),
);
