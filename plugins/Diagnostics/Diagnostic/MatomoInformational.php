<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
namespace Collect\Plugins\Diagnostics\Diagnostic;

use Collect\DbHelper;
use Collect\Option;
use Collect\SettingsCollect;
use Collect\Translation\Translator;
use Collect\Updater;
use Collect\Version;

/**
 * Information about Matomo itself
 */
class MatomoInformational implements Diagnostic
{
    /**
     * @var Translator
     */
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function execute()
    {
        $results = [];

        $results[] = DiagnosticResult::informationalResult('Matomo Version', Version::VERSION);

        if (SettingsCollect::isMatomoInstalled()) {
            $results[] = DiagnosticResult::informationalResult('Matomo Update History', Option::get(Updater::OPTION_KEY_TJWXJC_UPDATE_HISTORY));
            $results[] = DiagnosticResult::informationalResult('Matomo Install Version', $this->getInstallVersion());
            $results[] = DiagnosticResult::informationalResult('Latest Available Version', Option::get(\Collect\Plugins\CoreUpdater\Updater::OPTION_LATEST_VERSION));
            $results[] = DiagnosticResult::informationalResult('Is Git Deployment', SettingsCollect::isGitDeployment());
        }

        return $results;
    }

    private function getInstallVersion() {
        try {
            $version = DbHelper::getInstallVersion();
            if (empty($version)) {
                $version = 'Unknown - pre 3.8.';
            }
            return $version;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

}
