<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
namespace Collect\Plugins\Diagnostics\Diagnostic;

use Collect\Common;
use Collect\Config;
use Collect\Db;
use Collect\Collect;
use Collect\Translation\Translator;

/**
 * Check if Collect can use LOAD DATA INFILE.
 */
class DbReaderCheck implements Diagnostic
{
    /**
     * @var Translator
     */
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function execute()
    {
        $isCollectInstalling = !Config::getInstance()->existsLocalConfig();
        if ($isCollectInstalling) {
            // Skip the diagnostic if Collect is being installed
            return array();
        }

        if (!Db::hasReaderConfigured()) {
            // only show an entry when reader is actually configured
            return array();
        }

        $label = $this->translator->translate('Diagnostics_DatabaseReaderConnection');

        try {
            Db::getReader();
            return array(DiagnosticResult::singleResult($label, DiagnosticResult::STATUS_OK, ''));
        } catch (\Exception $e) {

        }

        $comment = Collect::translate('Installation_CannotConnectToDb');
        return array(DiagnosticResult::singleResult($label, DiagnosticResult::STATUS_WARNING, $comment));
    }
}
