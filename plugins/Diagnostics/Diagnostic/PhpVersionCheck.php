<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
namespace Collect\Plugins\Diagnostics\Diagnostic;

use Collect\Translation\Translator;

/**
 * Check the PHP version.
 */
class PhpVersionCheck implements Diagnostic
{
    /**
     * @var Translator
     */
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function execute()
    {
        global $collect_minimumPHPVersion;

        $actualVersion = PHP_VERSION;

        $label = sprintf(
            '%s >= %s',
            $this->translator->translate('Installation_SystemCheckPhp'),
            $collect_minimumPHPVersion
        );

        if ($this->isPhpVersionValid($collect_minimumPHPVersion, $actualVersion)) {
            $status = DiagnosticResult::STATUS_OK;
            $comment = $actualVersion;
        } else {
            $status = DiagnosticResult::STATUS_ERROR;
            $comment = sprintf(
                '%s: %s',
                $this->translator->translate('General_Error'),
                $this->translator->translate('General_Required', array($label))
            );
        }

        return array(DiagnosticResult::singleResult($label, $status, $comment));
    }

    private function isPhpVersionValid($requiredVersion, $actualVersion)
    {
        return version_compare($requiredVersion, $actualVersion) <= 0;
    }
}
