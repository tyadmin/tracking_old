<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\Diagnostics;

use Collect\Menu\MenuAdmin;
use Collect\Collect;

/**
 * This class allows you to add, remove or rename menu items.
 * To configure a menu (such as Admin Menu, Reporting Menu, User Menu...) simply call the corresponding methods as
 */
class Menu extends \Collect\Plugin\Menu
{
    public function configureAdminMenu(MenuAdmin $menu)
    {
        if (Collect::hasUserSuperUserAccess()) {
            $menu->addDiagnosticItem('Diagnostics_ConfigFileTitle', $this->urlForAction('configfile'), $orderId = 30);
        }
    }

}
