<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Ecommerce\Categories;

use Collect\Category\Subcategory;
use Collect\Collect;

class SalesSubcategory extends Subcategory
{
    protected $categoryId = 'Goals_Ecommerce';
    protected $id = 'Ecommerce_Sales';
    protected $order = 15;

    public function getHelp()
    {
        return '<p>' . Collect::translate('Ecommerce_SalesSubcategoryHelp1') . '</p>'
            . '<p>' . Collect::translate('Ecommerce_SalesSubcategoryHelp2') . '</p>';
    }
}
