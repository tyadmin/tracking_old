<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Ecommerce\Widgets;

use Collect\Common;
use Collect\Plugin\Manager;
use Collect\Plugins\Live\Live;
use Collect\Plugins\Live\MeasurableSettings;
use Collect\Widget\WidgetConfig;
use Collect\Site;

class GetEcommerceLog extends \Collect\Widget\Widget
{
    public static function configure(WidgetConfig $config)
    {
        $config->setCategoryId('Goals_Ecommerce');
        $config->setSubcategoryId('Goals_EcommerceLog');
        $config->setName('Goals_EcommerceLog');

        $idSite = Common::getRequestVar('idSite', 0, 'int');
        if (empty($idSite)) {
            $config->disable();
            return;
        }

        $site  = new Site($idSite);
        $config->setIsEnabled($site->isEcommerceEnabled());

        if (!Manager::getInstance()->isPluginActivated('Live') || !Live::isVisitorLogEnabled($idSite)) {
            $config->disable();
        }
    }

}
