<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Ecommerce\Reports;

use Collect\Collect;
use Collect\Plugins\Ecommerce\Columns\ProductSku;

class GetItemsSku extends BaseItem
{

    protected function init()
    {
        parent::init();

        $this->name      = Collect::translate('Goals_ProductSKU');
        $this->dimension = new ProductSku();
        $this->order     = 31;

        $this->subcategoryId = 'Goals_Products';
    }

}
