<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Ecommerce\Reports;

use Collect\Collect;
use Collect\Plugins\Ecommerce\Columns\ProductCategory;

class GetItemsCategory extends BaseItem
{
    protected function init()
    {
        parent::init();

        $this->name      = Collect::translate('Goals_ProductCategory');
        $this->dimension = new ProductCategory();
        $this->order     = 32;

        $this->subcategoryId = 'Goals_Products';
    }
}
