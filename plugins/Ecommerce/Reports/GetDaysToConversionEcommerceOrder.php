<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Ecommerce\Reports;

use Collect\Metrics;
use Collect\Collect;
use Collect\Plugins\Goals\Columns\DaysToConversion;

class GetDaysToConversionEcommerceOrder extends Base
{
    protected function init()
    {
        parent::init();

        $this->action = 'getDaysToConversion';
        $this->name = Collect::translate('General_EcommerceOrders') . ' - ' . Collect::translate('Goals_DaysToConv');
        $this->dimension = new DaysToConversion();
        $this->constantRowsCount = true;
        $this->processedMetrics = false;
        $this->metrics = array('nb_conversions');
        $this->order = 12;

        $this->parameters = array('idGoal' => Collect::LABEL_ID_GOAL_IS_ECOMMERCE_ORDER);
    }

}
