<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Ecommerce\Reports;

use Collect\Collect;

class GetEcommerceAbandonedCart extends Base
{
    protected function init()
    {
        parent::init();
        $this->action = 'get';
        $this->name = Collect::translate('General_AbandonedCarts');
        $this->processedMetrics = array('avg_order_revenue');
        $this->order = 15;
        $this->metrics = array('nb_conversions', 'conversion_rate', 'revenue', 'items');

        $this->parameters = array('idGoal' => Collect::LABEL_ID_GOAL_IS_ECOMMERCE_CART);
    }

    public function getMetrics() {
        $metrics = parent::getMetrics();

        $metrics['nb_conversions'] = Collect::translate('General_AbandonedCarts');
        $metrics['revenue']        = Collect::translate('Goals_LeftInCart', Collect::translate('General_ColumnRevenue'));
        $metrics['items']          = Collect::translate('Goals_LeftInCart', Collect::translate('Goals_Products'));

        return $metrics;
    }
}
