<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Ecommerce\Reports;

use Collect\Common;
use Collect\DataTable;
use Collect\Metrics\Formatter;
use Collect\Collect;
use Collect\Plugin\ViewDataTable;
use Collect\Plugins\CoreVisualizations\Visualizations\JqplotGraph\Evolution;
use Collect\Plugins\Goals\Columns\Metrics\AveragePrice;
use Collect\Plugins\Goals\Columns\Metrics\AverageQuantity;
use Collect\Plugins\Goals\Columns\Metrics\ProductConversionRate;
use Collect\Plugins\Goals\Conversions;
use Collect\Plugins\Goals\Model;
use Collect\Report\ReportWidgetFactory;
use Collect\Widget\WidgetsList;

abstract class BaseItem extends Base
{
    protected $defaultSortColumn = 'revenue';

    protected function init()
    {
        parent::init();
        $this->processedMetrics = array(
            new AveragePrice(),
            new AverageQuantity(),
            new ProductConversionRate()
        );
        $this->metrics = array(
            'revenue', 'quantity', 'orders', 'nb_visits'
        );
    }

    public function getMetrics()
    {
        $metrics = parent::getMetrics();
        $metrics['revenue'] = Collect::translate('General_ProductRevenue');
        $metrics['orders']  = Collect::translate('General_UniquePurchases');
        return $metrics;
    }

    public function getMetricsDocumentation()
    {
        // we do not check whether it is abandon carts if not set re performance improvements
        if ($this->isAbandonedCart($fetchIfNotSet = false)) {
            return array(
                'revenue'         => Collect::translate('Goals_ColumnRevenueDocumentation',
                                            Collect::translate('Goals_DocumentationRevenueGeneratedByProductSales')),
                'quantity'        => Collect::translate('Goals_ColumnQuantityDocumentation', $this->name),
                'orders'          => Collect::translate('Goals_ColumnOrdersDocumentation', $this->name),
                'avg_price'       => Collect::translate('Goals_ColumnAveragePriceDocumentation', $this->name),
                'avg_quantity'    => Collect::translate('Goals_ColumnAverageQuantityDocumentation', $this->name),
                'nb_visits'       => Collect::translate('Goals_ColumnVisitsProductDocumentation', $this->name),
                'conversion_rate' => Collect::translate('Goals_ColumnConversionRateProductDocumentation', $this->name),
            );
        }

        return array();
    }

    public function configureWidgets(WidgetsList $widgetsList, ReportWidgetFactory $factory)
    {
        $widgetsList->addToContainerWidget('Products', $factory->createWidget());
    }

    public function configureView(ViewDataTable $view)
    {
        $idSite = Common::getRequestVar('idSite');

        $view->config->show_ecommerce = true;
        $view->config->show_table     = false;
        $view->config->show_all_views_icons      = false;
        $view->config->show_exclude_low_population = false;
        $view->config->show_table_all_columns      = false;

        if (!($view instanceof Evolution)) {
            $moneyColumns = array('revenue');
            $formatter    = array(new Formatter(), 'getPrettyMoney');
            $view->config->filters[] = array('ColumnCallbackReplace', array($moneyColumns, $formatter, array($idSite)));
        }

        $view->requestConfig->filter_limit       = 10;
        $view->requestConfig->filter_sort_column = 'revenue';
        $view->requestConfig->filter_sort_order  = 'desc';

        $view->config->custom_parameters['isFooterExpandedInDashboard'] = true;

        // set columns/translations which differ based on viewDataTable TODO: shouldn't have to do this check...
        // amount of reports should be dynamic, but metadata should be static
        $columns = array_merge($this->getMetrics(), $this->getProcessedMetrics());
        $columnsOrdered = array('label', 'revenue', 'quantity', 'orders', 'avg_price', 'avg_quantity',
                                'nb_visits', 'conversion_rate');

        // handle old case where viewDataTable is set to ecommerceOrder/ecommerceAbandonedCart. in this case, we
        // set abandonedCarts accordingly and remove the ecommerceOrder/ecommerceAbandonedCart as viewDataTable.
        $viewDataTable = Common::getRequestVar('viewDataTable', '');
        if ($viewDataTable == 'ecommerceOrder') {
            $view->config->custom_parameters['viewDataTable'] = 'table';
            $abandonedCart = false;
        } else if ($viewDataTable == 'ecommerceAbandonedCart') {
            $view->config->custom_parameters['viewDataTable'] = 'table';
            $abandonedCart = true;
        } else {
            $abandonedCart = $this->isAbandonedCart($fetchIfNotSet = true);
        }

        if ($abandonedCart) {
            $columns['abandoned_carts'] = Collect::translate('General_AbandonedCarts');
            $columns['revenue'] = Collect::translate('Goals_LeftInCart', $columns['revenue']);
            $columns['quantity'] = Collect::translate('Goals_LeftInCart', $columns['quantity']);
            $columns['avg_quantity'] = Collect::translate('Goals_LeftInCart', $columns['avg_quantity']);
            unset($columns['orders']);
            unset($columns['conversion_rate']);

            $columnsOrdered = array('label', 'revenue', 'quantity', 'avg_price', 'avg_quantity', 'nb_visits',
                                    'abandoned_carts');

            $view->config->custom_parameters['abandonedCarts'] = '1';
        } else {
            $view->config->custom_parameters['abandonedCarts'] = '0';
        }

        $view->requestConfig->request_parameters_to_modify['abandonedCarts'] = $view->config->custom_parameters['abandonedCarts'];

        $translations = array_merge(array('label' => $this->name), $columns);

        $view->config->addTranslations($translations);
        $view->config->columns_to_display = $columnsOrdered;
    }

    private function isAbandonedCart($fetchIfNotSet)
    {
        $abandonedCarts = Common::getRequestVar('abandonedCarts', '', 'string');

        if ($abandonedCarts === '') {
            if ($fetchIfNotSet) {

                $idSite = Common::getRequestVar('idSite', 0, 'int');
                $period = Common::getRequestVar('period', '', 'string');
                $date   = Common::getRequestVar('date', '', 'string');

                $conversion = new Conversions();
                $conversions = $conversion->getConversionForGoal(Collect::LABEL_ID_GOAL_IS_ECOMMERCE_ORDER, $idSite, $period, $date);
                $cartNbConversions = $conversion->getConversionForGoal(Collect::LABEL_ID_GOAL_IS_ECOMMERCE_CART, $idSite, $period, $date);
                $preloadAbandonedCart = $cartNbConversions !== false && $conversions == 0;

                if ($preloadAbandonedCart) {
                    $abandonedCarts = '1';
                } else {
                    $abandonedCarts = '0';
                }
            } else {
                $abandonedCarts = '0';
            }
        }

        return $abandonedCarts == '1';
    }
}
