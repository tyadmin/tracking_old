<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link    https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

namespace Collect\Plugins\Ecommerce\ProfileSummary;

use Collect\Common;
use Collect\Collect;
use Collect\Plugins\Live\ProfileSummary\ProfileSummaryAbstract;
use Collect\View;

/**
 * Class EcommerceSummary
 */
class EcommerceSummary extends ProfileSummaryAbstract
{
    /**
     * @inheritdoc
     */
    public function getName()
    {
        return Collect::translate('Goals_Ecommerce');
    }

    /**
     * @inheritdoc
     */
    public function render()
    {
        if (empty($this->profile['totalEcommerceRevenue'])) {
            return '';
        }

        $view              = new View('@Ecommerce/_profileSummary.twig');
        $view->idSite      = Common::getRequestVar('idSite', null, 'int');
        $view->visitorData = $this->profile;
        return $view->render();
    }

    /**
     * @inheritdoc
     */
    public function getOrder()
    {
        return 20;
    }
}