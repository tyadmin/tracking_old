<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Ecommerce\Columns;

use Collect\Columns\Discriminator;
use Collect\Columns\Join\ActionNameJoin;
use Collect\Common;
use Collect\Plugin\Dimension\ActionDimension;
use Collect\Plugin\Manager;
use Collect\Plugins\CustomVariables\Tracker\CustomVariablesRequestProcessor;
use Collect\Tracker\Action;
use Collect\Tracker\Request;

class ProductViewName extends ActionDimension
{
    protected $type = self::TYPE_TEXT;
    protected $nameSingular = 'Ecommerce_ViewedProductName';
    protected $columnName = 'idaction_product_name';
    protected $segmentName = 'productViewName';
    protected $columnType = 'INT(10) UNSIGNED NULL';
    protected $category = 'Goals_Ecommerce';
    protected $sqlFilter = '\\Collect\\Tracker\\TableLogAction::getIdActionFromSegment';

    public function getDbColumnJoin()
    {
        return new ActionNameJoin();
    }

    public function getDbDiscriminator()
    {
        return new Discriminator('log_action', 'type', Action::TYPE_ECOMMERCE_ITEM_NAME);
    }

    public function onLookupAction(Request $request, Action $action)
    {
        if ($request->hasParam('_pkn')) {
            return Common::unsanitizeInputValue($request->getParam('_pkn'));
        }

        // fall back to custom variables (might happen if old logs are replayed)
        if (Manager::getInstance()->isPluginActivated('CustomVariables')) {
            $customVariables = CustomVariablesRequestProcessor::getCustomVariablesInPageScope($request);
            if (isset($customVariables['custom_var_k4']) && $customVariables['custom_var_k4'] === '_pkn') {
                return $customVariables['custom_var_v4'] ?? false;
            }
        }

        return parent::onLookupAction($request, $action);
    }

    public function getActionId()
    {
        return Action::TYPE_ECOMMERCE_ITEM_NAME;
    }
}