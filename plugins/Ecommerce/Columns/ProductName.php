<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Ecommerce\Columns;

use Collect\Columns\Dimension;
use Collect\Columns\Discriminator;
use Collect\Columns\Join\ActionNameJoin;
use Collect\Tracker\Action;

class ProductName extends Dimension
{
    protected $type = self::TYPE_TEXT;
    protected $dbTableName = 'log_conversion_item';
    protected $columnName = 'idaction_name';
    protected $nameSingular = 'Goals_ProductName';
    protected $namePlural = 'Goals_ProductNames';
    protected $category = 'Goals_Ecommerce';
    protected $sqlFilter = '\\Collect\\Tracker\\TableLogAction::getIdActionFromSegment';
    protected $segmentName = 'productName';

    public function getDbColumnJoin()
    {
        return new ActionNameJoin();
    }

    public function getDbDiscriminator()
    {
        return new Discriminator('log_action', 'type', Action::TYPE_ECOMMERCE_ITEM_NAME);
    }

}