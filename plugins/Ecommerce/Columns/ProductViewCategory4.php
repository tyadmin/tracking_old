<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Ecommerce\Columns;

use Collect\Columns\DimensionSegmentFactory;
use Collect\Segment\SegmentsList;

class ProductViewCategory4 extends ProductViewCategory
{
    protected $columnName = 'idaction_product_cat4';
    protected $categoryNumber = 4;

    public function configureSegments(SegmentsList $segmentsList, DimensionSegmentFactory $dimensionSegmentFactory)
    {
        // handled in category 1
    }
}