<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\PagePerformance\Categories;

use Collect\Category\Subcategory;
use Collect\Collect;

class PerformanceSubcategory extends Subcategory
{
    protected $categoryId = 'General_Actions';
    protected $id = 'PagePerformance_Performance';
    protected $order = 47;

    public function getHelp()
    {
        return '<p>' . Collect::translate('PagePerformance_PerformanceSubcategoryHelp1') . '</p>'
            . '<p>' . Collect::translate('PagePerformance_PerformanceSubcategoryHelp2') . '</p>';
    }
}
