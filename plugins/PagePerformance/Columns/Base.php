<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\PagePerformance\Columns;

use Collect\Exception\InvalidRequestParameterException;
use Collect\Plugin\Dimension\ActionDimension;
use Collect\Tracker\Action;
use Collect\Tracker\ActionPageview;
use Collect\Tracker\Request;
use Collect\Tracker\Visitor;

abstract class Base extends ActionDimension
{
    protected $type = self::TYPE_DURATION_MS;

    abstract public function getRequestParam();

    public function onNewAction(Request $request, Visitor $visitor, Action $action)
    {
        if (!($action instanceof ActionPageview)) {
            return false;
        }

        $time = $request->getParam($this->getRequestParam());

        if ($time === -1) {
            return false;
        }

        if ($time < 0) {
            throw new InvalidRequestParameterException(sprintf('Value for %1$s can\'t be negative.', $this->getRequestParam()));
        }

        // ignore values that are too high to be stored in column (unsigned mediumint)
        // refs https://github.com/matomo-org/matomo/issues/17035
        if ($time > 16777215) {
            return false;
        }

        return $time;
    }
}
