<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\UserLanguage;

use Collect\Collect;
use Collect\FrontController;

/**
 *
 */
class UserLanguage extends \Collect\Plugin
{
    public function postLoad()
    {
        Collect::addAction('Template.footerUserCountry', array('Collect\Plugins\UserLanguage\UserLanguage', 'footerUserCountry'));
    }

    public static function footerUserCountry(&$out)
    {
        $out .= '<h2 collect-enriched-headline>' . Collect::translate('UserLanguage_BrowserLanguage') . '</h2>';
        $out .= FrontController::getInstance()->fetchDispatch('UserLanguage', 'getLanguage');
    }
}
