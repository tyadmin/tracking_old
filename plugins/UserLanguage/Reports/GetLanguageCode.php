<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\UserLanguage\Reports;

use Collect\Collect;
use Collect\Plugins\UserLanguage\Columns\Language;
use Collect\Plugin\ReportsProvider;

class GetLanguageCode extends GetLanguage
{
    protected function init()
    {
        parent::init();
        $this->dimension     = new Language();
        $this->name          = Collect::translate('UserLanguage_LanguageCode');
        $this->documentation = Collect::translate('UserLanguage_getLanguageCodeDocumentation');
        $this->order = 11;
    }

    public function getRelatedReports()
    {
        return array(
            ReportsProvider::factory('UserLanguage', 'getLanguage'),
        );
    }

}
