<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Widgetize;

use Collect\Menu\MenuAdmin;
use Collect\Collect;

class Menu extends \Collect\Plugin\Menu
{
    public function configureAdminMenu(MenuAdmin $menu)
    {
        $tooltip   = Collect::translate('Widgetize_TopLinkTooltip');
        $urlParams = $this->urlForAction('index', array('segment' => false));

        $menu->addPlatformItem('General_Widgets', $urlParams, 6, $tooltip);
    }

}
