(function () {
    angular.module('collectApp.config', []);

    if ('undefined' === (typeof collect) || !collect) {
        return;
    }

    var collectAppConfig = angular.module('collectApp.config');
    // we probably want this later as a separate config file, till then it serves as a "bridge"
    for (var index in collect.config) {
        collectAppConfig.constant(index.toUpperCase(), collect.config[index]);
    }
})();