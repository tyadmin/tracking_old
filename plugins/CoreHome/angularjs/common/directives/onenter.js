/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

/**
 * Allows you to define any expression to be executed in case the user presses enter
 *
 * Example
 * <div collect-onenter="save()">
 * <div collect-onenter="showList=false">
 */
(function () {
    angular.module('collectApp.directive').directive('collectOnenter', collectOnenter);

  /**
   * @deprecated
   */
  function collectOnenter() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                    scope.$apply(function(){
                        scope.$eval(attrs.collectOnenter, {'event': event});
                    });

                    event.preventDefault();
                }
            });
        };
    }
})();
