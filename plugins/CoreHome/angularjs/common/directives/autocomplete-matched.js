/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

/**
 * If the given text or resolved expression matches any text within the element, the matching text will be wrapped
 * with a class.
 *
 * Example:
 * <div collect-autocomplete-matched="'text'">My text</div> ==> <div>My <span class="autocompleteMatched">text</span></div>
 *
 * <div collect-autocomplete-matched="searchTerm">{{ name }}</div>
 * <input type="text" ng-model="searchTerm">
 *
 * @deprecated
 */
(function () {
    angular.module('collectApp.directive').directive('collectAutocompleteMatched', collectAutocompleteMatched);

    collectAutocompleteMatched.$inject = ['collect', '$sanitize'];

    /**
     * @deprecated
     */
    function collectAutocompleteMatched(collect, $sanitize) {

        return {
            priority: 10, // makes sure to render after other directives, otherwise the content might be overwritten again 
            link: function (scope, element, attrs) {
                var searchTerm;

                scope.$watch(attrs.collectAutocompleteMatched, function (value) {
                    searchTerm = value;
                    updateText();
                });

                function updateText() {
                    if (!searchTerm || !element) {
                        return;
                    }

                    var content = collect.helper.htmlEntities(element.text());
                    var startTerm = content.toLowerCase().indexOf(searchTerm.toLowerCase());

                    if (-1 !== startTerm) {
                        var word = content.substr(startTerm, searchTerm.length);
                        var escapedword = $sanitize(collect.helper.htmlEntities(word));
                        content = content.replace(word, '<span class="autocompleteMatched">' + escapedword + '</span>');
                        element.html(content);
                    }
                }
            }
        };
    }
})();
