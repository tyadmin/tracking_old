/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
(function () {
    angular.module('collectApp.filter').filter('escape', escape);

    function escape() {

        return function(value) {
            return collectHelper.escape(collectHelper.htmlEntities(value));
        };
    }
})();