/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
(function () {
    angular.module('collectApp', [
        'ngSanitize',
        'ngAnimate',
        'ngCookies',
        'collectApp.config',
        'collectApp.service',
        'collectApp.directive',
        'collectApp.filter'
    ]);
    angular.module('app', []);

    angular.module('collectApp').config(['$locationProvider', function($locationProvider) {
        $locationProvider.html5Mode({ enabled: false, rewriteLinks: false }).hashPrefix('');
    }]);
})();