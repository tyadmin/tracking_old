<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\CoreHome;

use Exception;
use Collect\API\Request;
use Collect\Common;
use Collect\DataTable\Renderer\Json;
use Collect\Date;
use Collect\FrontController;
use Collect\Notification\Manager as NotificationManager;
use Collect\Collect;
use Collect\Plugin\Report;
use Collect\Plugins\Marketplace\Marketplace;
use Collect\SettingsCollect;
use Collect\Widget\Widget;
use Collect\Plugins\CoreHome\DataTableRowAction\MultiRowEvolution;
use Collect\Plugins\CoreHome\DataTableRowAction\RowEvolution;
use Collect\Plugins\Dashboard\DashboardManagerControl;
use Collect\Plugins\UsersManager\API;
use Collect\Site;
use Collect\Translation\Translator;
use Collect\UpdateCheck;
use Collect\Url;
use Collect\View;
use Collect\ViewDataTable\Manager as ViewDataTableManager;
use Collect\Widget\WidgetConfig;

class Controller extends \Collect\Plugin\Controller
{
    /**
     * @var Translator
     */
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;

        parent::__construct();
    }

    public function getDefaultAction()
    {
        return 'redirectToCoreHomeIndex';
    }

    public function renderReportWidget(Report $report)
    {
        Collect::checkUserHasSomeViewAccess();
        $this->checkSitePermission();

        $report->checkIsEnabled();

        return $report->render();
    }

    /**
     * This is only used for exported widgets
     * @return string
     * @throws Exception
     * @throws \Collect\NoAccessException
     */
    public function renderWidgetContainer()
    {
        Collect::checkUserHasSomeViewAccess();
        $this->checkSitePermission();

        $view = new View('@CoreHome/widgetContainer');
        $view->isWidgetized = (bool) Common::getRequestVar('widget', 0, 'int');
        $view->containerId  = Common::getRequestVar('containerId', null, 'string');

        return $view->render();
    }

    /**
     * @param Widget $widget
     * @return mixed
     * @throws Exception
     */
    public function renderWidget($widget)
    {
        Collect::checkUserHasSomeViewAccess();

        $config = new WidgetConfig();
        $widget::configure($config);

        $content = $widget->render();

        if ($config->getName() && Common::getRequestVar('showtitle', '', 'string') === '1') {
            if (strpos($content, '<h2') !== false
                || strpos($content, ' content-title=') !== false
                || strpos($content, ' collect-enriched-headline') !== false
                || strpos($content, '<h1') !== false ) {
                // already includes title
                return $content;
            }

            if (strpos($content, 'collect-content-block') === false
                && strpos($content, 'class="card"') === false
                && strpos($content, "class='card'") === false
                && strpos($content, 'class="card-content"') === false
                && strpos($content, "class='card-content'") === false) {
                $view = new View('@CoreHome/_singleWidget');
                $view->title = $config->getName();
                $view->content = $content;
                return $view->render();
            }
        }

        return $content;
    }

    function redirectToCoreHomeIndex()
    {
        $defaultReport = API::getInstance()->getUserPreference(
            API::PREFERENCE_DEFAULT_REPORT,
            Collect::getCurrentUserLogin()
        );
        $module = 'CoreHome';
        $action = 'index';

        // User preference: default report to load is the All Websites dashboard
        if ($defaultReport == 'MultiSites'
            && \Collect\Plugin\Manager::getInstance()->isPluginActivated('MultiSites')
        ) {
            $module = 'MultiSites';
        }

        if ($defaultReport == Collect::getLoginPluginName()) {
            $module = Collect::getLoginPluginName();
        }

        parent::redirectToIndex($module, $action, $this->idSite);
    }

    public function showInContext()
    {
        $controllerName = Common::getRequestVar('moduleToLoad');
        $actionName     = Common::getRequestVar('actionToLoad', 'index');

        if($controllerName == 'API') {
            throw new Exception("Showing API requests in context is not supported for security reasons. Please change query parameter 'moduleToLoad'.");
        }
        if ($actionName == 'showInContext') {
            throw new Exception("Preventing infinite recursion...");
        }

        $view = $this->getDefaultIndexView();
        $view->content = FrontController::getInstance()->fetchDispatch($controllerName, $actionName);
        return $view->render();
    }

    public function markNotificationAsRead()
    {
        Collect::checkUserHasSomeViewAccess();
        $this->checkTokenInUrl();

        $notificationId = Common::getRequestVar('notificationId');
        NotificationManager::cancel($notificationId);

        Json::sendHeaderJSON();
        return json_encode(true);
    }

    protected function getDefaultIndexView()
    {
        if (SettingsCollect::isInternetEnabled() && Marketplace::isMarketplaceEnabled()) {
            $this->securityPolicy->addPolicy('img-src', '*.matomo.org');
            $this->securityPolicy->addPolicy('default-src', '*.matomo.org');
        }

        $view = new View('@CoreHome/getDefaultIndexView');
        $this->setGeneralVariablesView($view);
        $view->showMenu = true;
        $view->dashboardSettingsControl = new DashboardManagerControl();
        $view->content = '';
        return $view;
    }

    protected function setDateTodayIfWebsiteCreatedToday()
    {
        $date = Common::getRequestVar('date', false);
        if ($date == 'today'
            || Common::getRequestVar('period', false) == 'range'
        ) {
            return;
        }

        if ($this->site) {
            $datetimeCreationDate      = $this->site->getCreationDate()->getDatetime();
            $creationDateLocalTimezone = Date::factory($datetimeCreationDate, $this->site->getTimezone())->toString('Y-m-d');
            $todayLocalTimezone        = Date::factory('now', $this->site->getTimezone())->toString('Y-m-d');

            if ($creationDateLocalTimezone == $todayLocalTimezone) {
                Collect::redirectToModule('CoreHome', 'index',
                    array('date'   => 'today',
                          'idSite' => $this->idSite,
                          'period' => Common::getRequestVar('period'))
                );
            }
        }
    }

    public function index()
    {
        $this->setDateTodayIfWebsiteCreatedToday();
        $view = $this->getDefaultIndexView();
        return $view->render();
    }

    //  --------------------------------------------------------
    //  ROW EVOLUTION
    //  The following methods render the popover that shows the
    //  evolution of a single or multiple rows in a data table
    //  --------------------------------------------------------

    /** Render the entire row evolution popover for a single row */
    public function getRowEvolutionPopover()
    {
        $rowEvolution = $this->makeRowEvolution($isMulti = false);
        $view = new View('@CoreHome/getRowEvolutionPopover');
        return $rowEvolution->renderPopover($this, $view);
    }

    /** Render the entire row evolution popover for multiple rows */
    public function getMultiRowEvolutionPopover()
    {
        $rowEvolution = $this->makeRowEvolution($isMulti = true);
        $view = new View('@CoreHome/getMultiRowEvolutionPopover');
        return $rowEvolution->renderPopover($this, $view);
    }

    /** Generic method to get an evolution graph or a sparkline for the row evolution popover */
    public function getRowEvolutionGraph($fetch = false, $rowEvolution = null)
    {
        if (empty($rowEvolution)) {
            $label = Common::getRequestVar('label', '', 'string');
            $isMultiRowEvolution = strpos($label, ',') !== false;

            $rowEvolution = $this->makeRowEvolution($isMultiRowEvolution, $graphType = 'graphEvolution');
            $rowEvolution->useAvailableMetrics();
        }

        $view = $rowEvolution->getRowEvolutionGraph();
        return $this->renderView($view);
    }

    /** Utility function. Creates a RowEvolution instance. */
    private function makeRowEvolution($isMultiRowEvolution, $graphType = null)
    {
        if ($isMultiRowEvolution) {
            return new MultiRowEvolution($this->idSite, $this->date, $graphType);
        } else {
            return new RowEvolution($this->idSite, $this->date, $graphType);
        }
    }

    /**
     * Forces a check for updates and re-renders the header message.
     *
     * This will check collect.org at most once per 10s.
     */
    public function checkForUpdates()
    {
        Collect::checkUserHasSomeAdminAccess();
        $this->checkTokenInUrl();

        // perform check (but only once every 10s)
        UpdateCheck::check($force = false, UpdateCheck::UI_CLICK_CHECK_INTERVAL);

        $view = new View('@CoreHome/checkForUpdates');
        $this->setGeneralVariablesView($view);
        return $view->render();
    }

    /**
     * Redirects the user to a paypal so they can donate to Collect.
     */
    public function redirectToPaypal()
    {
        $parameters = Request::getRequestArrayFromString($request = null);
        foreach ($parameters as $name => $param) {
            if ($name == 'idSite'
                || $name == 'module'
                || $name == 'action'
            ) {
                unset($parameters[$name]);
            }
        }
        $paypalParameters = [
            "cmd" => "_s-xclick"
        ];
        if (empty($parameters["onetime"]) || $parameters["onetime"] != "true") {
            $paypalParameters["hosted_button_id"] = "DVKLY73RS7JTE";
            $paypalParameters["currency_code"] = "USD";
            $paypalParameters["on0"] = "Collect Supporter";
            if (!empty($parameters["os0"])) {
                $paypalParameters["os0"] = $parameters["os0"];
            }
        } else {
            $paypalParameters["hosted_button_id"] = "RPL23NJURMTFA";
        }

        $url = "https://www.paypal.com/cgi-bin/webscr?" . Url::getQueryStringFromParameters($paypalParameters);

        Url::redirectToUrl($url);
        exit;
    }

    public function saveViewDataTableParameters()
    {
        Collect::checkUserIsNotAnonymous();
        $this->checkTokenInUrl();

        $reportId   = Common::getRequestVar('report_id', null, 'string');
        $parameters = (array) Common::getRequestVar('parameters', null, 'json');
        $login      = Collect::getCurrentUserLogin();
        $containerId = Common::getRequestVar('containerId', '', 'string');

        ViewDataTableManager::saveViewDataTableParameters($login, $reportId, $parameters, $containerId);
    }

}
