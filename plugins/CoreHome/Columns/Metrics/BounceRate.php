<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
namespace Collect\Plugins\CoreHome\Columns\Metrics;

use Collect\DataTable\Row;
use Collect\Metrics\Formatter;
use Collect\Collect;
use Collect\Plugin\ProcessedMetric;

/**
 * The percentage of visits that leave the site without visiting another page. Calculated
 * as:
 *
 *     bounce_count / nb_visits
 *
 * bounce_count & nb_visits are calculated by an Archiver.
 */
class BounceRate extends ProcessedMetric
{
    public function getName()
    {
        return 'bounce_rate';
    }

    public function getTranslatedName()
    {
        return Collect::translate('General_ColumnBounceRate');
    }

    public function getDependentMetrics()
    {
        return array('bounce_count', 'nb_visits');
    }

    public function format($value, Formatter $formatter)
    {
        return $formatter->getPrettyPercentFromQuotient($value);
    }

    public function compute(Row $row)
    {
        $bounceCount = $this->getMetric($row, 'bounce_count');
        $visits = $this->getMetric($row, 'nb_visits');

        return Collect::getQuotientSafe($bounceCount, $visits, $precision = 2);
    }
}