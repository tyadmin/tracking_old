<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\CoreHome\Columns;

use Collect\Columns\DimensionMetricFactory;
use Collect\Columns\MetricsList;
use Collect\Metrics\Formatter;
use Collect\Collect;
use Collect\Plugin\ArchivedMetric;
use Collect\Plugin\Dimension\VisitDimension;
use Collect\Segment\SegmentsList;
use Collect\Plugins\Live\SystemSettings;
use Collect\Columns\DimensionSegmentFactory;

/**
 * Dimension for the log_visit.idvisitor column. This column is added in the CREATE TABLE
 * statement, so this dimension exists only to configure a segment.
 */
class VisitorId extends VisitDimension
{
    protected $columnName = 'idvisitor';
    protected $metricId = 'visitors';
    protected $nameSingular = 'General_VisitorID';
    protected $namePlural = 'General_Visitors';
    protected $segmentName = 'visitorId';
    protected $acceptValues = '34c31e04394bdc63 - any 16 Hexadecimal chars ID, which can be fetched using the Tracking API function getVisitorId()';
    protected $allowAnonymous = false;
    protected $sqlFilterValue = array('Collect\Common', 'convertVisitorIdToBin');
    protected $type = self::TYPE_BINARY;

    public function configureMetrics(MetricsList $metricsList, DimensionMetricFactory $dimensionMetricFactory)
    {
        $metric = $dimensionMetricFactory->createMetric(ArchivedMetric::AGGREGATION_UNIQUE);
        $metric->setTranslatedName(Collect::translate('General_ColumnNbUniqVisitors'));
        $metric->setName('nb_uniq_visitors');
        $metricsList->addMetric($metric);
    }

    public function configureSegments(SegmentsList $segmentsList, DimensionSegmentFactory $dimensionSegmentFactory)
    {
        try {
            $systemSettings        = new SystemSettings();
            $visitorProfileEnabled = $systemSettings->disableVisitorProfile->getValue() === false
                && $systemSettings->disableVisitorLog->getValue() === false;
        } catch (\Zend_Db_Exception $e) {
            // when running tests the db might not yet be set up when fetching available segments
            if (!defined('TJWXJC_TEST_MODE') || !TJWXJC_TEST_MODE) {
                throw $e;
            }
            $visitorProfileEnabled = true;
        }

        if ($visitorProfileEnabled) {
            parent::configureSegments($segmentsList, $dimensionSegmentFactory);
        }
    }
}
