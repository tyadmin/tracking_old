<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\CoreHome\Columns;

use Collect\Columns\DimensionMetricFactory;
use Collect\Columns\MetricsList;
use Collect\Collect;
use Collect\Plugin\ArchivedMetric;
use Collect\Plugin\Dimension\ActionDimension;

class LinkVisitActionId extends ActionDimension
{
    protected $columnName = 'idlink_va';
    protected $acceptValues = 'Any integer.';
    protected $category = 'General_Actions';
    protected $nameSingular = 'General_Actions';
    protected $metricId = 'hits';
    protected $type = self::TYPE_NUMBER;

    public function configureMetrics(MetricsList $metricsList, DimensionMetricFactory $dimensionMetricFactory)
    {
        $metric = $dimensionMetricFactory->createMetric(ArchivedMetric::AGGREGATION_UNIQUE);
        $metric->setTranslatedName(Collect::translate('General_ColumnHits'));
        $metric->setName('hits');
        $metricsList->addMetric($metric);
    }
}
