<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\CoreHome\Columns;

use Collect\Columns\DimensionMetricFactory;
use Collect\Columns\MetricsList;
use Collect\Plugin\Dimension\VisitDimension;
use Collect\Metrics\Formatter;

require_once TJWXJC_INCLUDE_PATH . '/plugins/VisitTime/functions.php';

class VisitLastActionMonth extends VisitDimension
{
    protected $columnName = 'visit_last_action_time';
    protected $type = self::TYPE_DATETIME;
    protected $segmentName = 'visitEndServerMonth';
    protected $nameSingular = 'VisitTime_ColumnVisitEndServerMonth';
    protected $sqlSegment = 'MONTH(log_visit.visit_last_action_time)';
    protected $acceptValues = '1, 2, 3, ..., 11, 12';

    public function __construct()
    {
        $this->suggestedValuesCallback = function ($idSite, $maxValuesToReturn) {
            return range(1, min(12, $maxValuesToReturn));
        };
    }

    public function configureMetrics(MetricsList $metricsList, DimensionMetricFactory $dimensionMetricFactory)
    {
        // no metrics for this dimension
    }

    public function formatValue($value, $idSite, Formatter $formatter)
    {
        return \Collect\Plugins\VisitTime\translateMonth($value);
    }
}