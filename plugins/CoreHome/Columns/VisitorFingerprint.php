<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link http://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\CoreHome\Columns;

use Collect\Columns\DimensionMetricFactory;
use Collect\Columns\MetricsList;
use Collect\Collect;
use Collect\Plugin\ArchivedMetric;
use Collect\Plugin\Dimension\VisitDimension;

/**
 * Dimension for the log_visit.config_id column. This column is added in the CREATE TABLE
 * statement, so this dimension exists only to configure a segment.
 */
class VisitorFingerprint extends VisitDimension
{
    protected $columnName = 'config_id';
    protected $metricId = 'visitors_fingerprints';
    protected $nameSingular = 'General_VisitorFingerprint';
    protected $namePlural = 'General_Visitors';
    protected $segmentName = 'fingerprint';
    protected $acceptValues = '1eceaa833348b187 - any 16 Hexadecimal chars ID, which can be fetched from API.getLastVisitsDetails';
    protected $allowAnonymous = false;
    protected $sqlFilterValue = array('Collect\Common', 'convertVisitorIdToBin');

    public function configureMetrics(MetricsList $metricsList, DimensionMetricFactory $dimensionMetricFactory)
    {
        $metric = $dimensionMetricFactory->createMetric(ArchivedMetric::AGGREGATION_UNIQUE);
        $metric->setTranslatedName(Collect::translate('General_VisitorFingerprint'));
        $metricsList->addMetric($metric);
    }
}