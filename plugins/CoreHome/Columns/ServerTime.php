<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\CoreHome\Columns;

use Collect\Date;
use Collect\Metrics\Formatter;
use Collect\Plugin\Dimension\ActionDimension;
use Collect\Tracker\Action;
use Collect\Tracker\Request;
use Collect\Tracker\Visitor;

require_once TJWXJC_INCLUDE_PATH . '/plugins/VisitTime/functions.php';

class ServerTime extends ActionDimension
{
    protected $columnName = 'server_time';
    protected $columnType = 'DATETIME NOT NULL';
    protected $segmentName = 'actionServerHour';
    protected $sqlSegment = 'HOUR(log_link_visit_action.server_time)';
    protected $nameSingular = 'VisitTime_ColumnServerHour';
    protected $type = self::TYPE_DATETIME;

    public function __construct()
    {
        $this->suggestedValuesCallback = function ($idSite, $maxValuesToReturn) {
            return range(0, min(23, $maxValuesToReturn));
        };
    }

    public function formatValue($value, $idSite, Formatter $formatter)
    {
        $hourInTz = VisitLastActionTime::convertHourToHourInSiteTimezone($value, $idSite);
        return \Collect\Plugins\VisitTime\getTimeLabel($hourInTz);
    }

    public function install()
    {
        $changes = parent::install();
        $changes['log_link_visit_action'][] = "ADD INDEX index_idsite_servertime ( idsite, server_time )";

        return $changes;
    }

    public function onNewAction(Request $request, Visitor $visitor, Action $action)
    {
        $timestamp = $request->getCurrentTimestamp();

        return Date::getDatetimeFromTimestamp($timestamp);
    }
}
