/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

import { IDirective, IScope } from 'angular';
import DropdownButton from './DropdownButton';

export default function collectDropdownButton(): IDirective {
  return {
    restrict: 'C',
    link: function collectDropdownButtonLink(scope: IScope, element: JQuery) {
      DropdownButton.mounted(element[0]);
    },
  };
}

window.angular.module('collectApp').directive('dropdownButton', collectDropdownButton);
