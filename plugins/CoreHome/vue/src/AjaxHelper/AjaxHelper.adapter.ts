import AjaxHelper from './AjaxHelper';

declare global {
  interface Window {
    ajaxHelper: typeof AjaxHelper;
  }
}

window.ajaxHelper = AjaxHelper;

function ajaxQueue() {
  return window.globalAjaxQueue;
}

window.angular.module('collectApp.service').service('globalAjaxQueue', ajaxQueue);
