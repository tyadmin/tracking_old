/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */
import MatomoUrl from './MatomoUrl';

function collectUrl() {
  const model = {
    getSearchParam: MatomoUrl.getSearchParam.bind(MatomoUrl),
  };

  return model;
}

window.angular.module('collectApp.service').service('collectUrl', collectUrl);

// make sure $location is initialized early
window.angular.module('collectApp.service').run(['$location', () => null]);
