/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

import Periods from '../Periods/Periods';

let originalTitle: string;

const { collect, broadcast, collectHelper } = window;

collect.helper = collectHelper;
collect.broadcast = broadcast;

collect.updateDateInTitle = function updateDateInTitle(date: string, period: string) {
  if (!$('.top_controls #periodString').length) {
    return;
  }

  // Cache server-rendered page title
  originalTitle = originalTitle || document.title;

  if (originalTitle.indexOf(collect.siteName) === 0) {
    const dateString = ` - ${Periods.parse(period, date).getPrettyString()} `;
    document.title = `${collect.siteName}${dateString}${originalTitle.substr(collect.siteName.length)}`;
  }
};

collect.hasUserCapability = function hasUserCapability(capability: string) {
  return window.angular.isArray(collect.userCapabilities)
    && collect.userCapabilities.indexOf(capability) !== -1;
};

collect.on = function addMatomoEventListener(eventName: string, listener: WrappedEventListener) {
  function listenerWrapper(evt: Event) {
    listener(...(evt as CustomEvent<any[]>).detail); // eslint-disable-line
  }

  listener.wrapper = listenerWrapper;

  window.addEventListener(eventName, listenerWrapper);
};

collect.off = function removeMatomoEventListener(eventName: string, listener: WrappedEventListener) {
  if (listener.wrapper) {
    window.removeEventListener(eventName, listener.wrapper);
  }
};

collect.postEventNoEmit = function postEventNoEmit(
  eventName: string,
  ...args: any[] // eslint-disable-line
): void {
  const event = new CustomEvent(eventName, { detail: args });
  window.dispatchEvent(event);
};

collect.postEvent = function postMatomoEvent(
  eventName: string,
  ...args: any[] // eslint-disable-line
): void {
  collect.postEventNoEmit(eventName, ...args);

  // required until angularjs is removed
  window.angular.element(() => {
    const $rootScope = collect.helper.getAngularDependency('$rootScope') as any; // eslint-disable-line
    $rootScope.$oldEmit(eventName, ...args);
  });
};

const Matomo = collect;
export default Matomo;
