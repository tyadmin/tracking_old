/*!
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

import { IDirective, ITimeoutService } from 'angular';
import SideNav from './SideNav';

export default function collectSideNav($timeout: ITimeoutService): IDirective {
  return {
    restrict: 'A',
    priority: 10,
    link: function linkCollectSideNav(scope, element, attr) {
      const binding = {
        instance: null,
        value: {
          activator: $(attr.collectSideNav)[0],
        },
        oldValue: null,
        modifiers: {},
        dir: {},
      };

      $timeout(() => {
        SideNav.mounted(element[0], binding);
      });
    },
  };
}

collectSideNav.$inject = ['$timeout'];

window.angular.module('collectApp.directive').directive('collectSideNav', collectSideNav);
