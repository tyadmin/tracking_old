<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\CoreHome\Widgets;

use Collect\Common;
use Collect\Collect;
use Collect\Widget\Widget;
use Collect\Widget\WidgetConfig;
use Collect\Translation\Translator;
use Collect\View;

class GetDonateForm extends Widget
{
    /**
     * @var Translator
     */
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public static function configure(WidgetConfig $config)
    {
        $config->setCategoryId('About Matomo');
        $config->setName('CoreHome_SupportCollect');
        $config->setOrder(5);
    }

    public function render()
    {
        $footerMessage = null;
        if (Common::getRequestVar('widget', false)
            && Collect::hasUserSuperUserAccess()) {
            $footerMessage = $this->translator->translate('CoreHome_OnlyForSuperUserAccess');
        }

        return $this->renderTemplate('getDonateForm', array(
            'footerMessage' => $footerMessage
        ));
    }
}