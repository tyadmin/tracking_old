<?php


namespace Collect\Plugins\CoreHome\Widgets;


use Collect\Collect;
use Collect\Plugins\SitesManager\SitesManager;
use Collect\Plugins\UsersManager\UsersManager;
use Collect\Widget\Widget;
use Collect\Widget\WidgetConfig;

class QuickLinks extends Widget
{
    public static function configure(WidgetConfig $config)
    {
        $config->setCategoryId('About Matomo');
        $config->setName('CoreHome_QuickLinks');
        $config->setOrder(16);
        $config->setIsEnabled(Collect::hasUserSuperUserAccess());
    }

    public function render()
    {
        $hasUsersAdmin = UsersManager::isUsersAdminEnabled();
        $hasSitesAdmin = SitesManager::isSitesAdminEnabled();

        return $this->renderTemplate('quickLinks', array(
            'hasUsersAdmin' => $hasUsersAdmin,
            'hasSitesAdmin' => $hasSitesAdmin,
        ));
    }

}