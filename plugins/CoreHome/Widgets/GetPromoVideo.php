<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\CoreHome\Widgets;

use Collect\Widget\Widget;
use Collect\Widget\WidgetConfig;
use Collect\Translation\Translator;
use Collect\View;

class GetPromoVideo extends Widget
{
    /**
     * @var Translator
     */
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public static function configure(WidgetConfig $config)
    {
        $config->setCategoryId('About Matomo');
        $config->setName('Installation_Welcome');
        $config->setOrder(10);
    }

    public function render()
    {
        $view = new View('@CoreHome/getPromoVideo');
        $view->shareText     = $this->translator->translate('CoreHome_ShareCollectShort');
        $view->shareTextLong = $this->translator->translate('CoreHome_ShareCollectLong');
        $view->promoVideoUrl = 'https://matomo.org/docs/videos/';

        return $view->render();
    }
}