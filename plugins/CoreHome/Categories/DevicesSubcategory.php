<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\CoreHome\Categories;

use Collect\Category\Subcategory;
use Collect\Collect;

class DevicesSubcategory extends Subcategory
{
    protected $categoryId = 'General_Visitors';
    protected $id = 'DevicesDetection_Devices';
    protected $order = 15;

    public function getHelp()
    {
        return '<p>' . Collect::translate('CoreHome_DevicesSubcategoryHelp') . '</p>';
    }
}
