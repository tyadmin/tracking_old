<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\CoreHome\Categories;

use Collect\Category\Category;
use Collect\Collect;

class ActionsCategory extends Category
{
    protected $id = 'General_Actions';
    protected $order = 10;
    protected $icon = 'icon-reporting-actions';

    public function getDisplayName()
    {
        return Collect::translate('Actions_Behaviour');
    }
}
