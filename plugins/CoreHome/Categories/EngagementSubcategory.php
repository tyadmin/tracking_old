<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\CoreHome\Categories;

use Collect\Category\Subcategory;
use Collect\Collect;

class EngagementSubcategory extends Subcategory
{
    protected $categoryId = 'General_Actions';
    protected $id = 'VisitorInterest_Engagement';
    protected $order = 46;

    public function getHelp()
    {
        return '<p>' . Collect::translate('CoreHome_EngagementSubcategoryHelp1') . '</p>'
            . '<p>' . Collect::translate('CoreHome_EngagementSubcategoryHelp2') . '</p>';
    }
}
