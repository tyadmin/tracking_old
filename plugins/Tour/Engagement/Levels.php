<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Tour\Engagement;

use Collect\API\Request;
use Collect\Collect;

class Levels
{
    /**
     * @var array
     */
    private $challenges = array();

    private function getChallenges()
    {
        if (empty($this->challenges)) {
            $this->challenges = Request::processRequest('Tour.getChallenges', [], []);
        }

        return $this->challenges;
    }

    public function getNumChallengesCompleted()
    {
        $challenges = $this->getChallenges();

        $completed = 0;
        foreach ($challenges as $challenge) {
            if ($challenge['isSkipped'] || $challenge['isCompleted']) {
                $completed++;
            }
        }
        return $completed;
    }

    public function getCurrentLevel()
    {
        $completed = $this->getNumChallengesCompleted();

        $current = 0;
        foreach ($this->getLevels() as $threshold => $level) {
            if ($completed >= $threshold) {
                $current++;
            }
        }
        return $current;
    }

    public function getCurrentLevelName()
    {
        $completed = $this->getNumChallengesCompleted();

        $current = '';
        foreach ($this->getLevels() as $threshold => $level) {
            if ($completed >= $threshold) {
                $current = $level;
            }
        }
        return $current;
    }

    public function getNextLevelName()
    {
        $completed = $this->getNumChallengesCompleted();

        foreach ($this->getLevels() as $threshold => $level) {
            if ($completed < $threshold) {
               return $level;
            }
        }
    }

    public function getNumLevels()
    {
        $levels = $this->getLevels();
        return count($levels);
    }

    public function getNumChallengesNeededToNextLevel()
    {
        $completed = $this->getNumChallengesCompleted();

        foreach ($this->getLevels() as $threshold => $level) {
            if ($completed < $threshold) {
                return $threshold - $completed;
            }
        }
    }

    public function getCurrentDescription()
    {
        $login = Collect::getCurrentUserLogin();
        $numChallengesCompleted = $this->getNumChallengesCompleted();
        $numChallengesTotal = $this->getNumChallengesTotal();

        if ($numChallengesCompleted <= ($numChallengesTotal / 4)) {
            return Collect::translate('Tour_Part1Title', $login);
        }

        if ($numChallengesCompleted <= ($numChallengesTotal / 2)) {
            return Collect::translate('Tour_Part2Title', $login);
        }

        if ($numChallengesCompleted <= ($numChallengesTotal / 1.333)) {
            return Collect::translate('Tour_Part3Title', $login);
        }

        return Collect::translate('Tour_Part4Title', $login);
    }

    private function getNumChallengesTotal()
    {
        $challenges = $this->getChallenges();
        return count($challenges);
    }

    public function getLevels()
    {
        $numChallengesTotal = $this->getNumChallengesTotal();

        $levels = array(
            0 => Collect::translate('Tour_MatomoBeginner'),
            5 => Collect::translate('Tour_MatomoIntermediate'),
        );

        if ($numChallengesTotal > 10) {
            // the number of challenges varies from Matomo to Matomo depending on activated plugins and activated
            // features. Therefore we may remove some levels if there aren't too many challenges available.
            $levels[10] = Collect::translate('Tour_MatomoTalent');
        }

        if ($numChallengesTotal > 15) {
            $levels[15] = Collect::translate('Tour_MatomoProfessional');
        }

        $levels[$numChallengesTotal] = Collect::translate('Tour_MatomoExpert');

        return $levels;
    }


}