<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Tour\Engagement;

use Collect\Collect;

class ChallengeFlattenActions extends Challenge
{
    public function getName()
    {
        return Collect::translate('Tour_FlattenActions');
    }

    public function getDescription()
    {
        return Collect::translate('Tour_FlattenActionsDescription');
    }

    public function getId()
    {
        return 'flatten_actions';
    }

    public function getUrl()
    {
        return 'https://matomo.org/docs/collect-tour/#flattening-reports';
    }


}