<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Tour\Engagement;

use Collect\Collect;

class ChallengeViewVisitsLog extends Challenge
{
    public function getName()
    {
        return Collect::translate('Tour_ViewX', Collect::translate('Live_VisitsLog'));
    }

    public function getDescription()
    {
        return Collect::translate('Tour_ViewVisitsLogDescription');
    }

    public function getId()
    {
        return 'view_visits_log';
    }

    public function getUrl()
    {
        return 'https://matomo.org/docs/real-time/#visits-log';
    }


}