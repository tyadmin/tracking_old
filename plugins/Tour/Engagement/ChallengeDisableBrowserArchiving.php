<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Tour\Engagement;

use Collect\ArchiveProcessor\Rules;
use Collect\Collect;

class ChallengeDisableBrowserArchiving extends Challenge
{
    public function getName()
    {
        return Collect::translate('Tour_DisableBrowserArchiving');
    }

    public function getId()
    {
        return 'disable_browser_archiving';
    }

    public function isCompleted()
    {
        return !Rules::isBrowserTriggerEnabled();
    }

    public function getUrl()
    {
        return 'https://matomo.org/docs/setup-auto-archiving/';
    }


}