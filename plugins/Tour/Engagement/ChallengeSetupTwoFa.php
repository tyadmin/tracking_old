<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Tour\Engagement;

use Collect\Collect;
use Collect\Plugins\TwoFactorAuth\TwoFactorAuthentication;

class ChallengeSetupTwoFa extends Challenge
{
    public function getName()
    {
        return Collect::translate('Tour_SetupX', Collect::translate('TwoFactorAuth_TwoFactorAuthentication'));
    }

    public function getDescription()
    {
        return Collect::translate('TwoFactorAuth_TwoFactorAuthenticationIntro', array('', ''));
    }

    public function getId()
    {
        return 'setup_twofa';
    }

    public function isCompleted()
    {
        return TwoFactorAuthentication::isUserUsingTwoFactorAuthentication(Collect::getCurrentUserLogin());
    }

    public function getUrl()
    {
        return 'https://matomo.org/faq/general/faq_27245';
    }


}