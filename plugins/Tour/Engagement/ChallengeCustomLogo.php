<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Tour\Engagement;

use Collect\Collect;
use Collect\Plugins\CoreAdminHome\CustomLogo;
use Collect\Plugins\Tour\Dao\DataFinder;
use Collect\Url;

class ChallengeCustomLogo extends Challenge
{
    /**
     * @var DataFinder
     */
    private $finder;

    /**
     * @var null|bool
     */
    private $completed = null;

    public function __construct(DataFinder $dataFinder)
    {
        $this->finder = $dataFinder;
    }

    public function getName()
    {
        return Collect::translate('Tour_UploadLogo');
    }

    public function getDescription()
    {
        return Collect::translate('CoreAdminHome_CustomLogoHelpText');
    }

    public function getId()
    {
        return 'custom_logo';
    }

    public function isCompleted()
    {
        if (!isset($this->completed)) {
            $logo = new CustomLogo();
            $this->completed = $logo->isEnabled();
        }
        return $this->completed;
    }

    public function getUrl()
    {
        return 'index.php' . Url::getCurrentQueryStringWithParametersModified(array('module' => 'CoreAdminHome', 'action' => 'generalSettings', 'widget' => false)) . '#/#useCustomLogo';
    }

}
