<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Tour\Engagement;

use Collect\Collect;
use Collect\Url;

class ChallengeCreatedGoal extends Challenge
{

    public function getName()
    {
        return Collect::translate('Tour_DefineGoal');
    }

    public function getDescription()
    {
        return Collect::translate('Tour_DefineGoalDescription');
    }

    public function getId()
    {
        return 'define_goal';
    }

    public function getUrl()
    {
        return 'index.php' . Url::getCurrentQueryStringWithParametersModified(array('module' => 'Goals', 'action' => 'manage', 'widget' => false));
    }


}