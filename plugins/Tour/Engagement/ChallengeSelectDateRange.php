<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Tour\Engagement;

use Collect\Collect;

class ChallengeSelectDateRange extends Challenge
{
    public function getName()
    {
        return Collect::translate('Tour_SelectDateRange');
    }

    public function getDescription()
    {
        return Collect::translate('Tour_SelectDateRangeDescription');
    }

    public function getId()
    {
        return 'select_date_range';
    }

    public function getUrl()
    {
        return 'https://matomo.org/docs/collect-tour/#select-a-date-range';
    }


}