<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Tour\Engagement;

use Collect\Collect;

class ChallengeViewVisitorProfile extends Challenge
{
    public function getName()
    {
        return Collect::translate('Tour_ViewX', Collect::translate('Live_VisitorProfile'));
    }

    public function getDescription()
    {
        return Collect::translate('Tour_ViewVisitorProfileDescription');
    }

    public function getId()
    {
        return 'view_visitor_profile';
    }

    public function getUrl()
    {
        return 'https://matomo.org/docs/user-profile/';
    }


}