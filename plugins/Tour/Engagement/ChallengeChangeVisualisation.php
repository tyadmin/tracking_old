<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Tour\Engagement;

use Collect\Collect;

class ChallengeChangeVisualisation extends Challenge
{
    public function getName()
    {
        return Collect::translate('Tour_ChangeVisualisation');
    }

    public function getDescription()
    {
        return Collect::translate('Tour_ChangeVisualisationDescription');
    }

    public function getId()
    {
        return 'change_visualisations';
    }

    public function getUrl()
    {
        return 'https://matomo.org/docs/collect-tour/#a-look-at-a-collect-report';
    }


}