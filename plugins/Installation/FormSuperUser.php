<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Installation;

use HTML_QuickForm2_DataSource_Array;
use HTML_QuickForm2_Factory;
use HTML_QuickForm2_Rule;
use Collect\Container\StaticContainer;
use Collect\Collect;
use Collect\Plugins\UsersManager\UsersManager;
use Collect\QuickForm2;

/**
 *
 */
class FormSuperUser extends QuickForm2
{
    function __construct($id = 'generalsetupform', $method = 'post', $attributes = null, $trackSubmit = false)
    {
        parent::__construct($id, $method, $attributes = array('autocomplete' => 'off'), $trackSubmit);
    }

    function init()
    {
        HTML_QuickForm2_Factory::registerRule('checkLogin', 'Collect\Plugins\Installation\Rule_isValidLoginString');
        HTML_QuickForm2_Factory::registerRule('checkEmail', 'Collect\Plugins\Installation\Rule_isValidEmailString');

        $login = $this->addElement('text', 'login')
            ->setLabel(Collect::translate('Installation_SuperUserLogin'));
        $login->addRule('required', Collect::translate('General_Required', Collect::translate('Installation_SuperUserLogin')));
        $login->addRule('checkLogin');

        $password = $this->addElement('password', 'password')
            ->setLabel(Collect::translate('Installation_Password'));
        $password->addRule('required', Collect::translate('General_Required', Collect::translate('Installation_Password')));
        $pwMinLen = UsersManager::PASSWORD_MIN_LENGTH;
        $pwLenInvalidMessage = Collect::translate('UsersManager_ExceptionInvalidPassword', array($pwMinLen));
        $password->addRule('length', $pwLenInvalidMessage, array('min' => $pwMinLen));

        $passwordBis = $this->addElement('password', 'password_bis')
            ->setLabel(Collect::translate('Installation_PasswordRepeat'));
        $passwordBis->addRule('required', Collect::translate('General_Required', Collect::translate('Installation_PasswordRepeat')));
        $passwordBis->addRule('eq', Collect::translate('Installation_PasswordDoNotMatch'), ['operand' => $password]);

        $email = $this->addElement('text', 'email')
            ->setLabel(Collect::translate('Installation_Email'));
        $email->addRule('required', Collect::translate('General_Required', Collect::translate('Installation_Email')));
        $email->addRule('checkEmail', Collect::translate('UsersManager_ExceptionInvalidEmail'));

        $this->addElement('checkbox', 'subscribe_newsletter_collectorg', null,
            array(
                'content' => '&nbsp;&nbsp;' . Collect::translate('Installation_CollectOrgNewsletter'),
            ));

        $professionalServicesNewsletter = Collect::translate('Installation_ProfessionalServicesNewsletter',
            array("<a href='https://matomo.org/support/?pk_medium=App_Newsletter_link&pk_source=Matomo_App&pk_campaign=App_Installation' style='color:#444;' rel='noreferrer noopener' target='_blank'>", "</a>")
        );

        $privacyNoticeLink = '<a href="https://matomo.org/privacy-policy/" target="_blank" rel="noreferrer noopener">';
        $privacyNotice = '<div class="form-help email-privacy-notice">' . Collect::translate('Installation_EmailPrivacyNotice', [$privacyNoticeLink, '</a>'])
            . '</div>';

        $this->addElement('checkbox', 'subscribe_newsletter_professionalservices', null,
            array(
                'content' => $privacyNotice . '&nbsp;&nbsp;' . $professionalServicesNewsletter
            ));

        $this->addElement('submit', 'submit', array('value' => Collect::translate('General_Next') . ' »', 'class' => 'btn'));

        // default values
        $this->addDataSource(new HTML_QuickForm2_DataSource_Array(array(
            'subscribe_newsletter_collectorg' => 0,
            'subscribe_newsletter_professionalservices' => 0,
        )));
    }
}

/**
 * Login id validation rule
 *
 */
class Rule_isValidLoginString extends HTML_QuickForm2_Rule
{
    function validateOwner()
    {
        try {
            $login = $this->owner->getValue();
            if (!empty($login)) {
                Collect::checkValidLoginString($login);
            }
        } catch (\Exception $e) {
            $this->setMessage($e->getMessage());
            return false;
        }
        return true;
    }
}

/**
 * Email address validation rule
 *
 */
class Rule_isValidEmailString extends HTML_QuickForm2_Rule
{
    function validateOwner()
    {
        return Collect::isValidEmailString($this->owner->getValue());
    }
}
