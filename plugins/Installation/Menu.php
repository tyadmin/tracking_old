<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Installation;

use Collect\Menu\MenuAdmin;
use Collect\Collect;
use Collect\Plugin\Manager;

class Menu extends \Collect\Plugin\Menu
{
    public function configureAdminMenu(MenuAdmin $menu)
    {
        if (Collect::hasUserSuperUserAccess() && Manager::getInstance()->isPluginActivated('Diagnostics')) {
            $menu->addDiagnosticItem('Installation_SystemCheck',
                                   $this->urlForAction('systemCheckPage'),
                                   $order = 1);
        }
    }
}
