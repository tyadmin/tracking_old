<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Collect\Plugins\Installation;

use Collect\API\Request;
use Collect\Common;
use Collect\Config;
use Collect\Exception\NotYetInstalledException;
use Collect\FrontController;
use Collect\Collect;
use Collect\Plugins\Installation\Exception\DatabaseConnectionFailedException;
use Collect\SettingsCollect;
use Collect\View as CollectView;

/**
 *
 */
class Installation extends \Collect\Plugin
{
    protected $installationControllerName = '\\Collect\\Plugins\\Installation\\Controller';

    /**
     * @see \Collect\Plugin::registerEvents
     */
    public function registerEvents()
    {
        $hooks = array(
            'Config.NoConfigurationFile'      => 'dispatch',
            'Config.badConfigurationFile'     => 'dispatch',
            'Db.cannotConnectToDb'            => 'displayDbConnectionMessage',
            'Request.dispatch'                => 'dispatchIfNotInstalledYet',
            'AssetManager.getStylesheetFiles' => 'getStylesheetFiles',
        );
        return $hooks;
    }

    public function displayDbConnectionMessage($exception = null)
    {
        Common::sendResponseCode(500);

        $errorMessage = $exception->getMessage();

        if (Request::isApiRequest(null)) {
            $ex = new DatabaseConnectionFailedException($errorMessage);
            throw $ex;
        }

        $view = new CollectView("@Installation/cannotConnectToDb");
        $view->exceptionMessage = $errorMessage;

        $ex = new DatabaseConnectionFailedException($view->render());
        $ex->setIsHtmlMessage();

        throw $ex;
    }

    public function dispatchIfNotInstalledYet(&$module, &$action, &$parameters)
    {
        $general = Config::getInstance()->General;

        if (!SettingsCollect::isMatomoInstalled() && !$general['enable_installer']) {
            throw new NotYetInstalledException('Matomo is not set up yet');
        }

        if (empty($general['installation_in_progress'])) {
            return;
        }

        if ($module == 'Installation') {
            return;
        }

        $module = 'Installation';

        if (!$this->isAllowedAction($action)) {
            $action = 'welcome';
        }

        $parameters = array();
    }

    public function setControllerToLoad($newControllerName)
    {
        $this->installationControllerName = $newControllerName;
    }

    protected function getInstallationController()
    {
        return new $this->installationControllerName();
    }

    /**
     * @param \Exception|null $exception
     */
    public function dispatch($exception = null)
    {
        if ($exception) {
            $message = $exception->getMessage();
        } else {
            $message = '';
        }

        $action = Common::getRequestVar('action', 'welcome', 'string');

        if ($this->isAllowedAction($action) && (!defined('TJWXJC_ENABLE_DISPATCH') || TJWXJC_ENABLE_DISPATCH)) {
            echo FrontController::getInstance()->dispatch('Installation', $action, array($message));
        } elseif (defined('TJWXJC_ENABLE_DISPATCH') && !TJWXJC_ENABLE_DISPATCH) {
            if ($exception && $exception instanceof \Exception) {
                throw $exception;
            }
            return;
        } else {
            Collect::exitWithErrorMessage($this->getMessageToInviteUserToInstallCollect($message));
        }

        exit;
    }

    /**
     * Adds CSS files to list of CSS files for asset manager.
     */
    public function getStylesheetFiles(&$stylesheets)
    {
        $stylesheets[] = "plugins/Installation/stylesheets/systemCheckPage.less";
    }

    private function isAllowedAction($action)
    {
        $controller = $this->getInstallationController();
        $isActionAllowed = in_array($action, array('saveLanguage', 'getInstallationCss', 'getInstallationJs', 'reuseTables'));

        return in_array($action, array_keys($controller->getInstallationSteps()))
                || $isActionAllowed;
    }

    /**
     * @param $message
     * @return string
     */
    private function getMessageToInviteUserToInstallCollect($message)
    {
        $messageWhenCollectSeemsNotInstalled =
            $message .
            "\n<br/>" .
            Collect::translate('Installation_NoConfigFileFound') .
            "<br/><b>» " .
            Collect::translate('Installation_YouMayInstallCollectNow', array("<a href='index.php'>", "</a></b>")) .
            "<br/><small>" .
            Collect::translate('Installation_IfCollectInstalledBeforeTablesCanBeKept') .
            "</small>";
        return $messageWhenCollectSeemsNotInstalled;
    }
}
