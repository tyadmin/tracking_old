<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

namespace Collect\Plugins\TestRunner;

class Controller extends \Collect\Plugin\Controller
{
    public function check()
    {
        return 'OK';
    }
}
