<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 */

use Collect\SettingsServer;
use Collect\Tracker\RequestSet;
use Collect\Tracker;
use Collect\Tracker\Handler;
use Collect\API\CORSHandler;

@ignore_user_abort(true);

// Note: if you wish to debug the Tracking API please see this documentation:

if (!defined('TJWXJC_DOCUMENT_ROOT')) {
    define('TJWXJC_DOCUMENT_ROOT', dirname(__FILE__) == '/' ? '' : dirname(__FILE__));
}
if (file_exists(TJWXJC_DOCUMENT_ROOT . '/bootstrap.php')) {
    require_once TJWXJC_DOCUMENT_ROOT . '/bootstrap.php';
}
if (!defined('TJWXJC_INCLUDE_PATH')) {
    define('TJWXJC_INCLUDE_PATH', TJWXJC_DOCUMENT_ROOT);
}

require_once TJWXJC_INCLUDE_PATH . '/core/bootstrap.php';

require_once TJWXJC_INCLUDE_PATH . '/core/Plugin/Controller.php';
require_once TJWXJC_INCLUDE_PATH . '/core/Exception/NotYetInstalledException.php';
require_once TJWXJC_INCLUDE_PATH . '/core/Plugin/ControllerAdmin.php';
require_once TJWXJC_INCLUDE_PATH . '/core/Singleton.php';
require_once TJWXJC_INCLUDE_PATH . '/core/Plugin/Manager.php';
require_once TJWXJC_INCLUDE_PATH . '/core/Plugin.php';
require_once TJWXJC_INCLUDE_PATH . '/core/Common.php';
require_once TJWXJC_INCLUDE_PATH . '/core/Collect.php';
require_once TJWXJC_INCLUDE_PATH . '/core/IP.php';
require_once TJWXJC_INCLUDE_PATH . '/core/UrlHelper.php';
require_once TJWXJC_INCLUDE_PATH . '/core/Url.php';
require_once TJWXJC_INCLUDE_PATH . '/core/SettingsCollect.php';
require_once TJWXJC_INCLUDE_PATH . '/core/SettingsServer.php';
require_once TJWXJC_INCLUDE_PATH . '/core/Tracker.php';
require_once TJWXJC_INCLUDE_PATH . '/core/Config.php';
require_once TJWXJC_INCLUDE_PATH . '/core/Tracker/Cache.php';
require_once TJWXJC_INCLUDE_PATH . '/core/Tracker/Request.php';
require_once TJWXJC_INCLUDE_PATH . '/core/Cookie.php';
require_once TJWXJC_INCLUDE_PATH . '/core/API/CORSHandler.php';

SettingsServer::setIsTrackerApiRequest();

$environment = new \Collect\Application\Environment('tracker');
try {
    $environment->init();
} catch(\Collect\Exception\NotYetInstalledException $e) {
    die($e->getMessage());
}

Tracker::loadTrackerEnvironment();

$corsHandler = new CORSHandler();
$corsHandler->handle();

$tracker    = new Tracker();
$requestSet = new RequestSet();

ob_start();

try {
    $handler  = Handler\Factory::make();
    $response = $tracker->main($handler, $requestSet);

    if (!is_null($response)) {
        echo $response;
    }

} catch (Exception $e) {
    echo "Error:" . $e->getMessage();
    exit(1);
}

if (ob_get_level() > 1) {
    ob_end_flush();
}
