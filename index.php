<?php
/**
 * 入口文件
 *
 */

if (!defined('TJWXJC_DOCUMENT_ROOT')) {
    define('TJWXJC_DOCUMENT_ROOT', dirname(__FILE__) == '/' ? '' : dirname(__FILE__));
}
if (file_exists(TJWXJC_DOCUMENT_ROOT . '/bootstrap.php')) {
    require_once TJWXJC_DOCUMENT_ROOT . '/bootstrap.php';
}
if (!defined('TJWXJC_INCLUDE_PATH')) {
    define('TJWXJC_INCLUDE_PATH', TJWXJC_DOCUMENT_ROOT);
}

require_once TJWXJC_INCLUDE_PATH . '/core/bootstrap.php';

if (!defined('TJWXJC_PRINT_ERROR_BACKTRACE')) {
    define('TJWXJC_PRINT_ERROR_BACKTRACE', false);
}

require_once TJWXJC_INCLUDE_PATH . '/core/dispatch.php';
